# -*- coding: utf-8 -*-

import os, sys, time, random, subprocess
from functools import partial

from qgis.gui import *
from qgis.core import *
from qgis.utils import iface
from PyQt5.QtCore import *
from PyQt5 import QtWidgets, QtGui

from .rdi_param import rdiParam


def makeUniqueIdent():
    t = hex(int(time.time()*1000))[2:]
    r = hex(random.randint(1000000,10000000))[2:]
    return str(t) + str(r)
    
def compress(txt):
    return zlib.compress(txt)
    
def niceDuration(d):
    s = ''
    q = 0
    l = [{'val':86400,'aff':'j'},
        {'val':3600,'aff':'h'},
        {'val':60,'aff':'mn'},
        {'val':1,'aff':'s'}]
    for e in l:
        if d>=e['val']:
            q = int(d/e['val'])
            s = s + str(q) + e['aff']
            d = d - q*e['val']
    if s=="":
        s = "<1s"
    return s

def quote(txt):
    return f'"{txt}"'
    
def flashMsg(*args, **kwargs):
    '''
    Usage :
      flash_msg('Mon information', 'mon message d information')
      flash_msg('Mon erreur', 'mon message d erreur', category='error')
      flash_msg('mon message', category= 'info', duration=10)
    '''
    levels = { 
        'info' : Qgis.Info,
        'warning' : Qgis.Warning,   
        'error' : Qgis.Critical,
        'succes': Qgis.Success
        }
    category = kwargs.get('category') or 'info'
    title = ''
    msg = args[0]
    if len(args) > 1:
        title = args[0]
        msg = args[1]
    level = levels[category]
    duration = kwargs.get('duration') or 10 if category=='error' else 5
    iface.messageBar().pushMessage(title, msg, level=level, duration=duration)    

def clearLayout(layout):
        try:
            for i in reversed(range(layout.count())): 
                l = layout.itemAt(i).layout()
                w = layout.itemAt(i).widget()
                if isinstance(w, QtWidgets.QComboBox):
                    w.addItem("", "")
                if isinstance(w, QgsMapLayerComboBox):
                    w.setAllowEmptyLayer(True)    
                if w:
                    layout.removeWidget(w)
                    w.setParent(None)
                    del w
                if l:
                    clearLayout(l)
                    layout.removeItem(l)
                    del l
        except:
            # print(layout, isinstance(layout, QtWidgets.QLayout))
            # print(sys.exc_info())
            pass

def openExplorer(file=None):
    root = os.path.dirname(__file__)
    path = root.replace("/", "\\")
    if file:
        path = os.path.join(root, file)
        path = path.replace("/", "\\")
    if os.path.isfile(path):
        p = subprocess.Popen(f'explorer /select, "{path}"')
        # p.terminate()
    if os.path.isdir(path):
        p = subprocess.Popen(f'explorer "{path}"')
        # p.terminate()
        
def openFile(file):
    path = os.path.join(os.path.dirname(__file__), file)
    url = QUrl.fromLocalFile(path)
    QtGui.QDesktopServices.openUrl(url)
    

def getSvgPath(svg, code=None):
    found = False
    path = svg
    try:
        plugFolder = os.path.join(os.path.dirname(__file__), 'svg')
        qgisFolder = os.path.join(os.environ.get('QGIS_PREFIX_PATH'), '.', 'svg')
        dd = {'plug':plugFolder, 'qgis':qgisFolder}
        if code!=None and code in dd:
            file = os.path.join(dd[code], svg)
            if os.path.exists(file):
                found = True
                path = file
        ld = QgsSettings().value('svg/searchPathsForSVG') or []
        for v in dd.values():
            ld.append(v)
        for d in ld:
            if found:
                break
            file = os.path.join(d, svg)
            if os.path.exists(file):
                found = True
                path = file
    except:
        print(sys.exc_info())
    return path

class toggleButton(QtWidgets.QPushButton):
    
    changed = pyqtSignal()
    
    def __init__(self, list):
        QtWidgets.QPushButton.__init__(self)
        self.list = list
        self.ind = 0
        self.set()
        super().clicked.connect(self.onClick)
        
    def onClick(self):
        self.ind += 1
        if self.ind>=self.count():
            self.ind = 0
        self.set()
        self.changed.emit()
    
    def count(self):
        return len(self.list)
    
    def set(self):
        if self.ind<self.count():
            self.setElement(self.list[self.ind])
    
    def setElement(self, elt):
        if isinstance(elt, str):
            super().setText(elt)
        if isinstance(elt, QtGui.QIcon):
            super().setIcon(elt)
        if isinstance(elt, tuple):
            for e in elt:
                self.setElement(e)
                
    def getElement(self):
        return (self.list[self.ind])
        
    def text(self):
        return super().text()
    
    def match(self, txt, elt):
        if isinstance(elt, str):
            return (elt==txt)
        if isinstance(elt, tuple):
            bool = False
            for e in elt:
                bool = bool or self.match(txt, e)
            return bool
    
    def setFromText(self, txt):
        for i,e in enumerate(self.list):
            if self.match(txt, e):
                self.ind = i
                self.set()
                self.changed.emit()
                break
            

class rdiTimer(QTimer):
    def __init__(self, obj=None, action=None):
        QTimer.__init__(self, obj)
        self.obj = obj
        self.param = None
        self.setAction(action)
        self.setMaxPts()
        self.setLabel()
        self.setCountdown()
        self.timeout.connect(self.inc)
        if self.obj:
            self.obj.destroyed.connect(self.clear)
     
    def setAction(self, action=None):
        self.action = action 
    
    def setMaxPts(self, nb=7):
        self.maxPts = nb  
        
    def setCountdown(self, d=0):
        self.countdown = int(d)
        
    def setLabel(self, label=None, deb="", fin=""):
        self.label = label
        self.deb = deb
        self.fin = fin
    
    def anim(self):
        if not self.param:
            self.param = rdiParam()
        return self.param.get('animation')
    
    def start(self, interval=None):
        self.stopReset()
        if self.anim():
            if not self.isActive():
                self.startTime = time.time()
                super().start(interval)
                self.count = -1
                self.inc()
        
    def stop(self, interval=None):
        if interval:
            try:
                flag = self.stopFlag
            except:
                self.stopFlag = time.time()
                self.stopTimeout = interval
        else:
            super().stop()
    
    def stopReset(self):
        try:
            del self.stopFlag
            del self.stopTimeout
        except:
            pass
            
    def stopCheck(self):
        if not self.anim():
            self.stop()
        try:
            duree = (time.time()-self.stopFlag)*1000
            if duree>self.stopTimeout:
                self.stop()
        except:
            pass
                
    def actuLabel(self):
        if self.label:
            try:
                txt = self.deb + self.pts() + self.fin
                self.label.setText(txt)
                QtWidgets.QApplication.processEvents()
            except:
                pass
    
    def remain(self):
        if self.countdown>0:
            d = round(self.countdown - (time.time()-self.startTime))
            return f"{d}s"
    
    def pts(self):
        txt = ""
        for i in range(self.count):
            txt += "."
        return txt
        
    def inc(self):
        self.count += 1
        if self.count>self.maxPts:
            self.count = 0
        try:
            self.action()
        except:
            # print(sys.exc_info())
            pass
        self.actuLabel()
        self.stopCheck()
    
    def clear(self):
        self.stop()
       

class setTimeout(QObject):
    def __init__(self, delay, callback, *args, **kwargs):
        QObject.__init__(self)
        self.timer = QTimer(self)
        self.callback = callback
        self.args = args
        self.kwargs = kwargs
        self.timer.timeout.connect(self.end)
        self.timer.destroyed.connect(self.clear)
        self.timer.start(delay)
        
    def end(self):
        self.timer.stop()
        self.callback(*self.args, **self.kwargs)
        
    def clear(self):
        self.timer.stop()


class clickLabel(QtWidgets.QLabel):
    
    clicked = pyqtSignal()
    
    def __init__(self, txt=""):
        QtWidgets.QLabel.__init__(self, txt)
        self.font = QtGui.QFont()
        self.setCursor(Qt.PointingHandCursor)
        

    def mousePressEvent(self, event):
        self.clicked.emit()
        
    def enterEvent(self, event):
        self.font.setBold(True)
        self.setFont(self.font)
        
    def leaveEvent(self, event):
        self.font.setBold(False)
        self.setFont(self.font)
        
        
        
class showDialog(QgsDialog):
   
    def __init__(self, title="", txt=""):

        buttons = QtWidgets.QDialogButtonBox.Ok
        QgsDialog.__init__(self, iface.mainWindow(),
                            fl=Qt.WindowFlags(),
                            buttons=buttons,
                            orientation=Qt.Horizontal)
        if title!="":
            self.setWindowTitle(title) 
        self.text = txt
        self.showText()
        self.show()

    def showText(self):
        sc = QtWidgets.QScrollArea(self)
        sc.setWidgetResizable(True)
        self.layout().addWidget(sc)
        w = QtWidgets.QWidget()
        gbox = QtWidgets.QGridLayout()
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        w.setLayout(gbox)
        sc.setWidget(w)
        sc.setFixedSize(600, 400)
        
        l = QtWidgets.QTextEdit(self.text, self)
        l.setReadOnly(True)
        l.setPlainText(self.text)
        gbox.addWidget(l)
        
        
               
    






