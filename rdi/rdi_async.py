# -*- coding: utf-8 -*-

import sys, os, time

from qgis.core import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtGui import *
from PyQt5.QtSql import *

from functools import partial

import time, sys

from .rdi_tools import makeUniqueIdent, niceDuration, quote, clearLayout




class abstractQuery():
    def __init__(self, psql):
        self.psql = psql
        self.description = ""
        self.table = None
        self.schema = None
        self.dispo = None
        self.dependency = None
        self.pkey = None
        self.geom = None
        self.type = None
        self.srid = None
        self.origin = None
        self.done = True
        self.dbtask = None
        self.error = []
    
    def setOrigin(self, table, schema, method=None):
        self.origin = True
        self.oriGeom = self.psql.getGeometryField(table, schema)
        self.oriType = self.psql.getGeomTypeUnique(table, schema)
        self.oriSrid = self.psql.getSrid(table, schema)
        if method=='dump':
            self.oriType = self.oriType.lower().replace('multi','').upper()
        if method=='cut':
            self.oriType = "MULTI" + self.oriType.lower().replace('multi','').upper()
        
    def isTable(self, table=None, schema=None):
        if not table:
            table = self.table
        if not schema:
            schema = self.schema
        if table:
            return self.psql.isTable(table, schema)
            
    def full(self, table=None, schema=None):
        if not table:
            table = self.table
        if not schema:   
            schema = self.schema
        full = self.psql.full(table, schema)
        return full  

    def makeCreate(self):
        pass
    
    def prepare(self):
        pass
        
    def endCreate(self):
        pass
        
    def kill(self):
        pass
    
    def create(self, db):
        self.dbtask = db
        if self.dbtask:

            self.execSql(self.makeCreate())
            
            if not self.geom:
                query = self.execSql(self.getGeomSql())
                if query.next():
                    self.geom = query.value(0)
            
            if self.geom:   
                col = f"{self.table}_geom"
                sql = f"DROP INDEX IF EXISTS {quote(col)} CASCADE; CREATE INDEX {quote(col)} ON {self.full()} USING gist({quote(self.geom)});"
                self.execSql(sql)
                
                if not self.type:
                    query = self.execSql(self.getTypeSql())
                    if query.next():
                        self.type = query.value(0)
                    if not self.type:
                        if self.origin:
                            self.type = self.oriType
                
                if not self.srid:
                    query = self.execSql(self.getSridSql())
                    if query.next():
                        self.srid = query.value(0)
                    if not self.srid:
                        if self.origin:
                            self.srid = self.oriSrid 
            
            if self.type and self.srid:
                sql = f"ALTER TABLE {self.full()} ALTER COLUMN {quote(self.geom)}" + \
                    f" TYPE geometry({self.type}, {self.srid}) USING ST_SetSRID({quote(self.geom)}, {self.srid});"
                self.execSql(sql)
             
            sql = self.endCreate()
            if sql:
                self.execSql(sql)
    
    def execSql(self, sql):
        query = self.dbtask.exec_(sql)
        self.addError(query)
        return query
    
    def addError(self, query):
        if query.lastError().type()!=QSqlError.NoError:
            self.error.append(query.lastError().text())
        
    def dumpError(self):
        for err in self.error:
            print(err)
    
    def setPkey(self, pkey):
        self.pkey = pkey
    
    def getGeomSql(self):
        schema = self.schema
        if not schema:
            schema = self.psql.PGschema
        sql = f"select column_name from information_schema.columns " + \
            f"	where table_name = '{self.table}' " + \
            f"	and udt_name='geometry' " + \
            f"	and table_schema='{schema}' ;"  
        return sql
    
    def getTypeSql(self):
        sql = f"SELECT geometryType({quote(self.geom)}) FROM {self.full()} limit 1;"
        return sql
        
    def getSridSql(self): 
        sql = f"SELECT st_srid({quote(self.geom)}) FROM {self.full()} limit 1;"
        return sql


class asyncQuery(QgsTask):
    def __init__ (self, psql, name, sql=None, query=None):
        super().__init__(name, QgsTask.CanCancel)
        self.psql = psql
        self.sql = sql
        self.query = query
        self.response = None
        self.name = name
        self.delay = None
        self.exception = None
        self.new = None

    def run(self):
        try:
            if self.sql:
                self.new = self.psql.newConn(self.name, False)
                if self.new.open():
                    query = self.new.exec_(self.sql)
                    if query.next():
                        self.response = query.value(0)
                
            if self.query:
                self.new = self.psql.newConn(self.name, True)
                if self.new.open():
                    self.query.create(self.new)
            
            if self.new!=None:
                self.new.close()
                QSqlDatabase.removeDatabase(self.name)
                self.new = None
                
            return True
        except:
            self.exception = sys.exc_info()
            return False
        
    def killConnexion(self):
        try:
            if self.new!=None:
                self.new.close()
                QSqlDatabase.removeDatabase(self.name)
                self.query.kill()
        except:
            print(sys.exc_info())
            pass
    
    def finished(self, result):
        if not result:
            print(self.exception)
        pass
        
    def cancel(self):
        self.killConnexion()
        super().cancel()
        

class globalTask(QgsTask):  
    def __init__(self, description):
        super().__init__(description, QgsTask.CanCancel)
        self.exception = None
        self.delay = None
        self.callin = None
        self.result = None

    def setDelay(self, delay):
        self.delay = delay
    
    def setCallin(self, callin, *args, **kwargs):
        self.callin = callin
        self.args = args
        self.kwargs = kwargs
    
    def run(self):
        if self.delay:
            time.sleep(self.delay)
        if self.callin:
            try:
                self.result = self.callin(*self.args, **self.kwargs)
            except:
                self.result = sys.exc_info()
        return True

    def finished(self, result):
        pass
        
    def cancel(self):
        super().cancel()
        

class mQueryTask():
    def __init__(self, query):
        self.query = query
        self.psql = query.psql
        self.manager = query.psql.manager
        self.initQuery()
        
    
    def makeTask(self, psql, name, sql=None, query=None):
        globals()[name] = asyncQuery(psql, name, sql=sql, query=query)
        globals()[name].taskCompleted.connect(partial(self.deleteTask, name))
        globals()[name].taskTerminated.connect(partial(self.deleteTask, name))
        self.name = name
        return globals()[name]

    def deleteTask(self, name=None):
        if not name:
            name = self.name
        try:
            del globals()[name]
        except:
            pass
            
    def initQuery(self):
        self.finished = False
        self.start = None
        self.stop = None
        if self.query.dependency:
            self.name = f"En attente de {self.query.table}:{makeUniqueIdent()}"
            self.task = self.makeTask(self.psql, self.name)
        else:
            self.query.prepare()
            self.psql.manager.waiting[self.query.full()] = self
            self.name = f"{self.query.description}:{makeUniqueIdent()}"
            self.task = self.makeTask(self.psql, self.name, query=self.query)
        self.task.taskCompleted.connect(self.setFinished)
        self.task.taskTerminated.connect(self.setTerminated)
        self.task.begun.connect(self.setBegun)
    
    # def tableReady(self):
        # return self.psql.isTable(self.query.table)
        
    def duree(self):
        d = None
        if self.query.dependency:
            return "déjà lancée"  
        else:
            if self.start:
                if self.stop:
                    d = int(self.stop - self.start)
                else:
                    d = int(time.time() - self.start)
                return niceDuration(d)
            else:
                return "-"           
            
    def setBegun(self):
        self.start = time.time()
        self.manager.writeTrace(f"({self.parent.inc}) Sub start: {self.name}")
    
    def setFinished(self):
        self.stop = time.time()
        self.manager.writeTrace(f"({self.parent.inc}) Sub end: {self.name}")
        self.finished = True
        self.query.dumpError()
        self.psql.manager.waiting[self.query.full()] = False
        del self.psql.manager.waiting[self.query.full()]
        self.manager.sendChanged()
    
    def setTerminated(self):
        self.finished = True
        self.manager.sendChanged()
        self.query.dumpError()
        # print('error', self.query.description)
        pass
    
    def cancel(self):
        self.task.cancel()
        self.deleteTask()
        self.finished = True


class mGlobalTask():
    def __init__(self, description):
        self.description = description
        self.name = description + ":" + makeUniqueIdent()
        self.subTasks = []
        self.mTasks = []
        self.finished = False
        self.task = self.makeTask(self.name)
        self.task.taskCompleted.connect(self.setFinished)
        self.task.taskTerminated.connect(self.end)
        self.task.begun.connect(self.setBegun)
        self.tm = QgsApplication.taskManager()
        self.manager = None
        self.callback = None
    
    def setManager(self, manager):
        self.manager = manager
    
    def makeTask(self, name):
        globals()[name] = globalTask(name)
        globals()[name].taskCompleted.connect(partial(self.deleteTask, name))
        globals()[name].taskTerminated.connect(partial(self.deleteTask, name))
        self.name = name
        return globals()[name]
    
    def deleteTask(self, name=None):
        if not name:
            name = self.name
        try:
            del globals()[name]
        except:
            pass
    
    def addSubTask(self, query, final=False):
        m = mQueryTask(query)
        m.parent = self
        sub = []
        if query.dependency:
            sub.append(query.dependency.task)
        if final:
            sub.extend(self.subTasks)
            self.task.addSubTask(m.task, sub, QgsTask.ParentDependsOnSubTask)
        else:
            self.task.addSubTask(m.task, sub)
        self.mTasks.append(m)
        self.subTasks.append(m.task)
        if not self.manager:
            self.manager = query.psql.manager
    
    def setCallback(self, mCallback, *args, **kwargs):
        self.callback = mCallback
        self.args = args
        self.kwargs = kwargs
        try:
            self.task.taskCompleted.connect(partial(mCallback, *args, **kwargs))
        except:
            self.task.taskCompleted.connect(mCallback)
        
    def hasToLaunch(self):
        bool = (len(self.mTasks)==0)
        for m in self.mTasks:
            if not m.query.dependency:
                bool = True
        return bool
    
    def launch(self):
        if self.hasToLaunch():
            try:
                self.tm.addTask(self.task)
            except:
                pass
        else:
            self.cancel()
        
    def end(self):
        self.finished = True
        if self.manager:
            self.manager.sendChanged()
        pass
    
    def setBegun(self):
        if self.manager:
            self.manager.writeTrace(f"({self.inc}) Global start: {self.name}")
        
    def setFinished(self):
        if self.manager:
            self.manager.writeTrace(f"({self.inc}) Global end: {self.name}")
        self.end()
    
    def activeTasks(self):
        list = []
        for m in self.mTasks:
            if not m.finished:
                list.append(m)
        return list
        
    def count(self):
        return len(self.mTasks)
    
    def cancelTask(self, task):
        try:
            task.cancel()
        except:
            pass
    
    def cancel(self):
        for m in self.mTasks:
            m.cancel()
        self.task.cancel()
        self.deleteTask()
        self.end()
    
class manageTasks(QObject):
    
    changed = pyqtSignal()
    
    def __init__(self, logs=False):
        QObject.__init__(self)
        self.list= []
        self.waiting = {}
        self.trace = None
        if logs:
            self.trace = traceTasks()
        self.inc = 0
        
    def add(self, item):
        if item in self.list:
            print('Achtung! task already added')
            return
        self.list.append(item)
        self.inc += 1
        item.inc = self.inc
        self.traceItem(item)
        item.setManager(self)
        item.launch()
        self.changed.emit()
    
    def traceItem(self, it):
        aff = f"Add task ({it.inc}) {it.description}:"
        for m in it.mTasks:           
            zone = m.query.description
            aff += f"\n      {zone}"
        self.writeTrace(aff)
        
    def writeTrace(self, content):
        if self.trace:
            self.trace.write(content)
    
    def sendChanged(self):
        self.actuList()
        self.changed.emit()
    
    def getId(self, item):
        for idx, it in enumerate(self.list):
            if it==item:
                return idx
                
    def remove(self, item):
        idx = self.getId(item)
        if idx!=None:
            it = self.list.pop(idx)
    
    def actuList(self):
        remove = []
        for it in self.list:
            if it.finished:
                remove.append(it)
        for it in remove:
            self.remove(it)
    
    def countAlive(self):
        tot = 0
        self.actuList()
        for it in self.list:
            list = it.activeTasks()
            nb = len(list)
            tot += nb
        return tot
        
    def makeLabLog(self, gbox, txt=""):
        l = QtWidgets.QLabel(txt)
        l.setWordWrap(True)
        gbox.addWidget(l)
        return l
        
    def setLog(self, textEdit):
        textEdit.setText("")
        tot = self.countAlive()
        s = "s" if tot>1 else ""
        txt = f"{tot} requête{s} active{s}"
        txt += f" (Qgis:{len(QgsApplication.taskManager().tasks())})"
        textEdit.setTextColor(QtGui.QColor("#000000"))
        textEdit.insertPlainText(txt)
        for it in self.list:
            aff = f"\n\n- {it.description}:"
            textEdit.setTextColor(QtGui.QColor("#000000"))
            textEdit.insertPlainText(aff)
            for m in it.mTasks:        
                zone = m.query.description.split("/")[0]
                if len(zone)>30:
                    zone = zone[:27] + "..."
                aff = f"\n     → {zone} ({m.duree()})"
                textEdit.setTextColor(QtGui.QColor("#555555"))
                textEdit.insertPlainText(aff)           

    def getLog(self):
        aff = ""
        tot = self.countAlive()
        for it in self.list:
            aff += f"\n\n- {it.description}:"
            for m in it.mTasks:        
                
                zone = m.query.description.split("/")[0]
                if len(zone)>30:
                    zone = zone[:27] + "..."
                aff += f"\n     → {zone} ({m.duree()})"

        s = "s" if tot>1 else ""
        txt = f"{tot} requête{s} active{s}"
        txt += f" (Qgis:{len(QgsApplication.taskManager().tasks())})"
        txt += aff
        return txt
        
    def killAll(self):
        for it in self.list:
            it.cancel()
        if (len(QgsApplication.taskManager().tasks())):
            QgsApplication.taskManager().cancelAll()
            
            
class traceTasks():
    def __init__(self):
        self.doss = os.path.join(os.path.dirname(__file__), 'logs')
        self.makeDoss()
        file = os.path.join(self.doss, f"trace_{makeUniqueIdent()}.txt")
        self.f = QFile(file)
    
    def makeDoss(self):
        if not os.path.isdir(self.doss):
            os.mkdir(self.doss)    
    
    def time(self):
        now = time.time()
        ml = str(now).split('.')[1][:3]
        horo = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(now))
        return f"{horo}.{ml}"
        
    def write(self, content):
        if not self.f.open(QIODevice.WriteOnly | QIODevice.Text):
            pass
        out = QTextStream(self.f)
        out << f"\n\n{self.time()}: " 
        out << content   
        




