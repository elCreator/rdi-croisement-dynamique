# -*- coding: utf-8 -*-
import os, sys, configparser, time
from functools import partial

from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtCore import *

from .rdi_meta import rdiMeta

class rdiParam():

    def __init__(self):
        self.meta = rdiMeta()
        self.plugin = self.meta.getPlugin()
        self.pluginName = self.meta.getName()
        
        self.file = os.path.join(os.path.dirname(__file__), "parameter.ini")
        self.config = configparser.ConfigParser(allow_no_value=True)
        self.config.optionxform = str
        self.lastReading = None
        # self.getParams()
        self.initData()
    
    def readParameters(self):
        defParams = self.makeDefaultparameters()
        self.needWriting = False
        if os.path.exists(self.file):
            try:
                self.config.read(self.file)
            except:
                s = open(self.file, mode='r', encoding='utf-8-sig').read()
                open(self.file, mode='w', encoding='utf-8').write(s)
                self.config.read(self.file)
            for s in self.config.sections():
                self.params[s] = dict((x,y) for x,y in self.config.items(s))  
        else:
            self.needWriting = True
        self.fusionParameters(self.params, self.meta.readIni("param"))
        self.fusionParameters(self.params, defParams)
        if self.needWriting:
            self.writeParameters()
        
    def fusionParameters(self, ref, new):
        if not ref or not new:
            self.needWriting = True
            return
        for s, elt in new.items():
            if not s in ref:
                ref[s] = {}
                self.needWriting = True
            for k,v in elt.items():
                if not k in ref[s]:
                    if k!="comment" and k[:1]!="#":
                        self.needWriting = True
                    ref[s][k] = v

    def formatParameters(self):     
        for section, dic in self.params.items():
            for k, v in dic.items():    
                nv = v
                if v=='True' or v==True:
                    nv = True
                    self.params[section][k] = nv
                    continue
                if v=='False' or v==False:
                    nv = False
                    self.params[section][k] = nv
                    continue
                if v=='None' or v==None:
                    nv = None
                    self.params[section][k] = nv
                    continue
                try:
                    if str(v).find('.')>=0:
                        nv = float(v)
                    else:
                        nv = int(v)
                except:
                    # print(sys.exc_info(), k, v)
                    pass
                self.params[section][k] = nv

    def writeParameters(self):     
        config = configparser.ConfigParser(allow_no_value=True)
        config.optionxform = str         
        config.set(None, "# Fichier de configuration du plugin rdi", None)
        config.set(None, "# A manipuler avec precautions, reserve aux utilisateurs avertis", None)
        config.set(None, "# Pour eviter les erreurs, il est preferable d'utiliser l'interface de parametrage du plugin", None)
        for section, dic in self.params.items():
            config.add_section(section)
            if 'comment' in dic:
                config.set(section, dic['comment'], None)
            for k, v in dic.items():
                if k!='comment':
                    config.set(section, k, str(v))
        f = open(self.file,'w')
        config.write(f)
        f.close()
        self.lastReading = None   
        self.meta.writeIni("param", self.params)
     
    def needReading(self):
        if not self.lastReading:
            return True
        else:
            delay = time.time() - self.lastReading
            if delay>10:
                return True
    
    def getParams(self):
        if self.needReading():
            self.params = {}
            self.readParameters()
            self.formatParameters()
            self.lastReading = time.time()
    
    def getSection(self, section):
        self.getParams()
        return self.params[section.upper()]
    
    def getValue(self, section, key):
        self.getParams()
        try:
            v = self.params[section.upper()][key]
        except:
            v = None
        return v
        
    def changeValue(self, section, key, value):
        self.getParams()
        if (section in self.params) and (key in self.params[section]):
            self.params[section][key] = value
        self.writeParameters()
        
    
    def find(self, key):
        self.getParams()
        error = rdiParamError(key)
        for section, elt in self.params.items():
            v = self.getValue(section, key)
            if v!=None:
                error.addValue(v)
                error.addSection(section, elt['comment'])
        value = error.getValue()
        return value, error
        
    def get(self, key, section=None):
        if section:
            return self.getValue(section, key)
        else:
            v, err = self.find(key)
            if err.status:
                return v
            else:
                print(err.dump())
                raise ValueError(err.dump())

    def getErr(self, key):
        v, err = self.find(key)
        return v, err
        
    def set(self, key, value, section=None):
        if section:
            self.changeValue(section, key, value)
        else:
            v, err = self.find(key)
            if err.status:
                self.changeValue(err.getSection(), key, value)
            else:
                print(err.dump())
                raise ValueError(err.dump())

    
    def makeDefaultparameters(self):
        params = {
            'CONN': {
                'comment': u"# Parametres de connexion a la base postgres/postgis",
                'host': "",
                'port': "",
                'user': "",
                'password': "",
                'db': "",
                'schema': "",
                'savePassword': True,
            },
            'ADMIN': {
                'comment': u"# Modele de donnees des tables d'administration",
                'schema': "",
                'tableAdmin': '_rdi_gestion_liste',
                'tableTmp': '_rdi_gestion_stockage',
                'tmpPrefixe': '_tmp_',
                'manualInsertion': False,
            },
            'EXE': {
                'comment': u"# Parametres divers pour l'execution",
                'root': u"rdi",
                'alea': u"Aléas",
                'enjeu': u"Enjeux",
                'enjeuLight': u"Enjeux associés",
                'stats': u"Statistiques",
                'buff': u"Tampons",
                'admin': False,
                'info': True,
                'help': True,
                'logs': False,
                'cbCompact': True,
                'blocDrop': False,
                'blocDropExpand': False,
                'queryTimeout': 900,
                'stockTimeout': 864000,
                'cluster': True,
                'clusterField': False,
                'clusterPrecision': 10,
                'clusterRound': True,
                'clusterBack': False,
                'buffer': 0,
                'bufferMin': -20,
                'bufferMax': 100,
                'simplify': 0,
                'simplifyMax': 20,
                'animation': True,
                'inversion': True,
                'cutting': False,
                'correction': False,
                'backMap': True,
                'baseAdmin': False,
                'entityCount': False,
                'autoGenAlert': 100,
            },
            'PAINT': {
                'comment': u"# Parametres globaux de dessin",
                'strokeUnit': "mm",
                'strokeWidth': "0.5",
                'aleaOpacity': "0.6",
                'bufferFill': "#20000000",
                'bufferStroke': "#000000",
                'bufferPen': "dot",
                'bufferWidth': 1.3,
                'statFill': "#10ff7400",
                'statStroke': "#ff7400",
                'statPen': "dash",
                'statWidth': 1,
                'rendererTag': "renderer-v2",
                'rendererUnit': "mm",
                'rendererTolerance': 25,
            },
        }
        
        return params
        

    def initData(self):
        self.translator = {
            'geom' : {
                'label': u"Géométrie",
                'info': u"Ces options impactent le croisement spatial (et donc la construction dynamique de la requête SQL) et le rendu de la couche extraite",
                'list': [
                    {
                        'label': u"Rendu cluster",
                        'help': 'p5-option-cluster',
                        'description': u"Regroupe l'affichage des points à proximité",
                        'ini': 'cluster',
                        'tab': ['parameter', 'dynamic', 'layer'],
                    },
                    {
                        'label': u"Somme sur attribut",
                        'help': 'p5-option-cluster',
                        'description': u"Permet de choisir le champs à sommer pour la clusterisation au lieu de compter les entités" + \
                                        "\nAttention, cela peu ralentir considérablement le calcul et l'affichage," + \
                                        "\lorsque le nombre d'entités résultantes (et/ou la valeur du champ sommé) est importante",
                        'ini': 'clusterField',
                        'tab': ['dynamic','layer'],
                        'dep': 'cluster'
                    },
                    {
                        'label': u"Points en fond",
                        'help': 'p5-option-cluster',
                        'description': u"Garde un affichage de l'ensemble des points en léger arrière plan",
                        'ini': 'clusterBack',
                        'tab': ['layer'],
                        'dep': 'cluster'
                    },
                    {
                        'label': u"Tampon (m)",
                        'help': 'p5-option-buffer',
                        'description': u"Ajoute une distance tampon lors des requêtes de croisement spatial",
                        'ini': 'buffer',
                        'tab': ['layer'],
                        'next': {
                            'type': 'range',
                            'min': self.get('bufferMin'),
                            'max': self.get('bufferMax'),
                        },
                    },
                    {
                        'label': u"Simplifier (m)",
                        'help': 'p5-option-simplify',
                        'description': u"Simplifie la géométrie pour accélérer le croisement spatial",
                        'ini': 'simplify',
                        'tab': ['layer'],
                        'next': {
                            'type': 'range',
                            'min': 0,
                            'max': self.get('simplifyMax'),
                        },
                    },
                    {
                        'label': u"Inversibilité croisement",
                        'help': 'p5-option-invert',
                        'description': u"Un clic droit sur l'enjeu permet d'inverser le résultat issu du croisement spatial" + \
                                        "\n(enjeux dans l'emprise OU enjeux hors emprise)",
                        'ini': 'inversion',
                    },
                    {
                        'label': u"Découper résultat",
                        'help': 'p5-option-cut',
                        'description': u"Découpe les enjeux surfaciques ou linéaires pour être strictement inclus dans l'emprise du croisement",
                        'ini': 'cutting',
                        'tab': ['layer'],
                    },
                    {
                        'label': u"Correction à la volée",
                        'help': 'p5-option-corr',
                        'description': u"Rajoute des corrections à la volée sur la géométrie" + \
                                        "\n(permet de contourner quelques erreurs de topologie, mais rajoute du temps de calcul)",
                        'ini': 'correction',
                        'tab': ['layer'],
                    },
                    {
                        'label': u"Décompte des entités",
                        'help': 'p5-option-count',
                        'description': u"Indique le nombre d'entités renvoyées à côté du nom de la couche",
                        'ini': 'entityCount',
                        'tab': ['layer'],
                    },
                ]
            },
            'plugin' : {
                'label': u"Affichage",
                'info': u"Ces options impactent l'affichage du plugin lui-même, comme notamment la présentation des listes de couches enjeux/aléas dans l'onglet de croisement dynamique (le premier)",
                'list': [
                    {
                        'label': u"Liste compacte",
                        'help': 'p5-option-compact',
                        'description': u"Regroupe les couches associées à un même item en liste déroulante",
                        'ini': 'cbCompact',
                        'tab': ['dynamic'],
                    },
                    {
                        'label': u"Arborescence en blocs",
                        'help': 'p5-option-block',
                        'description': u"Arborescence sous forme de blocs dépliables",
                        'ini': 'blocDrop',
                        'tab': ['parameter', 'dynamic'],
                    },
                    {
                        'label': u"Tout déplier",
                        'help': 'p5-option-block',
                        'description': u"Présente les blocs sous forme dépliée par défaut",
                        'ini': 'blocDropExpand',
                        'tab': ['dynamic'],
                        'func': ['self.majBlocState()'],
                        'dep': 'blocDrop'
                    },
                    {
                        'label': u"Animation",
                        'help': 'p5-option-anim',
                        'description': u"Défilement pointillé pour patienter pendant les traitements longs",
                        'ini': 'animation',
                        'tab': ['journal'],
                    },
                    {
                        'label': u"Explications",
                        'help': 'p5-option-explain',
                        'description': u"Affichage plus détaillé des fonctionnalités" +\
                                        "\nUtile au début... mais consomme beaucoup d'espace",
                        'ini': 'info',
                        'tab': ['all'],
                    },
                    {
                        'label': u"Aide en ligne",
                        'help': 'p5-option-help',
                        'description': u"Affichage d'un point d'interrogation cliquable" +\
                                        "\npour ouvrir l'aide en ligne sur l'objet ciblé",
                        'ini': 'help',
                        'tab': ['all'],
                    },
                    {
                        'label': u"Administration",
                        'help': 'p5-option-admin',
                        'description': u"Affichage des onglets d'administration",
                        'ini': 'admin',
                        'tab': ['all'],
                    },
                ]
            }
        }
        

class rdiParamError():
    def __init__(self, key):
        self.key = key
        self.values = []
        self.message = ""
        self.codes = []
        self.sections = []
        self.status = None

    def addValue(self, value):
        self.values.append(value)
        
    def addSection(self, section, text=""):
        self.codes.append(section)
        self.sections.append(text)

    def getValue(self):
        if len(self.values)==1:
            self.message = f"Valeur trouvée pour \"{self.key}\""
            self.status = True
            return self.values[0]
        else:
            self.status = False
            if len(self.values)==0:
                self.message = f"Aucune valeur correpondant à \"{self.key}\""
            else:
                self.message = f"Plusieurs correspondent à \"{self.key}\""
            return None
            
    def getSection(self):    
        if len(self.codes)==1:
            return self.codes[0]
            
    def dump(self):
        txt = self.message
        txt += f"\nStatus: {self.status}"
        txt += "\nValues: " + ", ".join(map(str, self.values))
        txt += "\nCodes: " + ", ".join(map(str, self.codes))
        txt += "\nSections: " + ", ".join(map(str, self.sections))
        return txt
        
        

class rdiParamElt(QObject):
    
    changed = pyqtSignal(dict)
    
    def __init__(self, elt, parent):
        QObject.__init__(self)
        # self.param = rdiParam()
        self.dock = parent
        self.param = self.dock.param
        self.elt = elt
        self.type = None
        self.widgets = []
        
    def newLine(self, layout):
        w, l = self.newBloc(layout)
        if 'description' in self.elt:
            w.setToolTip(self.elt['description'])
        self.layout = l
        self.widget = w
    
    def newBloc(self, layout):
        widget = QtWidgets.QWidget()   
        layout.addWidget(widget)
        hbox = QtWidgets.QHBoxLayout(widget)
        hbox.setSpacing(0)
        if self.elt.get('dep'):
            hbox.setContentsMargins(20,0,0,0)
        else:
            hbox.setContentsMargins(0,0,0,0)
        return widget, hbox
    
    def draw(self, layout):
        self.newLine(layout)
        self.paint()
        help = self.elt.get('help')
        if help:
            self.dock.makeHelpButton(self.layout, help)
        
    def paint(self, elt=None, layout=None):
        w = None
        if elt==None:
            elt = self.elt
        if layout==None:
            layout = self.layout
        
        type = elt.get('type') or 'checkbox'
        if type=='checkbox':
            w = self.addCb(elt, layout)
            if not self.type:
                self.type = 'boolean'
        elif type=='range':
            w = self.addRange(elt, layout)
            self.type = 'numeric'

        if w:
            self.widgets.append([w, type])

        if 'next' in elt:
            nw, nl = self.newBloc(layout)
            self.paint(elt['next'], nl)
            if w and type=='checkbox':
                w.stateChanged.connect(partial(self.show, w, nw))
                self.show(w, nw)
                pass   
     
    def addCb(self, elt, layout):
        # try:
        w = QtWidgets.QCheckBox(elt['label'])
        w.setChecked(self.value())
        layout.addWidget(w)
        w.stateChanged.connect(partial(self.changeValue))
        return w
        # except:
            # print(sys.exc_info())
            # pass
    
    def addRange(self, elt, layout):
        try:
            w = QtWidgets.QSlider(Qt.Horizontal)
            w.setRange(elt['min'], elt['max'])
            w.setValue(self.value())
            layout.addWidget(w)
            w.sliderReleased.connect(self.changeValue)
            
            l = QtWidgets.QSpinBox()
            l.setRange(elt['min'], elt['max'])
            l.setValue(self.value())
            layout.addWidget(l)
            l.editingFinished.connect(self.changeValue)
            
            l.valueChanged.connect(partial(self.setValueOnly, w, l))
            w.valueChanged.connect(partial(self.setValueOnly, l, w))  
            
            return w
        except:
            # print(sys.exc_info())
            pass 

    def setValueOnly(self, toObj, fromObj):
        toObj.blockSignals(True)
        toObj.setValue(fromObj.value())
        toObj.blockSignals(False)

    def show(self, cb, widget):
        if cb.isChecked():
            widget.show()
        else:
            widget.hide()

    def changeValue(self):
        value = self.getValue()
        if value!=None:   
            self.param.set(self.elt['ini'], value)
        else:
            print(f"Error compilation value for {self.elt['ini']}")
        self.changed.emit(self.elt)
    
    
    def value(self):
        return self.param.get(self.elt['ini'])
        
    def getValue(self):
        value = None
        values = []
        for w, type in self.widgets:
            if type=='checkbox':
                if self.type=='numeric':
                    v = 1 if w.isChecked() else 0
                else:
                    v = w.isChecked()
                values.append(v)
            elif type=='range':
                values.append(w.value())
        
        if self.type=='numeric':
            value = 1
            for v in values:
                value = value * v

        if self.type=='boolean':
            value = True
            for v in values:
                value = value and v

        return value
 