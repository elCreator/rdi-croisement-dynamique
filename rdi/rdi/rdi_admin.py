# -*- coding: utf-8 -*-

import sys, os, json, traceback
from functools import partial

from qgis.gui import *
from qgis.core import *
from qgis.utils import iface
from PyQt5 import QtGui, QtWidgets, uic, QtXml, QtSvg
from PyQt5.QtCore import *

from .rdi_param import rdiParam
from .rdi_tools import makeUniqueIdent, clearLayout, toggleButton, rdiTimer, clickLabel, showDialog, getSvgPath, setTimeout
from .rdi_trade import rdiTrade
from .rdi_async import mGlobalTask, manageTasks
from .rdi_layer import rdiLayer, rdiLayerField
from .rdi_statistic import rdiStatLayer


class rdiAdmin(QObject):

    dataChanged = pyqtSignal(int)
    datasChanged = pyqtSignal(list)

    def __init__(self, parent):
        QObject.__init__(self)
        self.dock = parent
        self.psql = parent.psql
        self.trade = rdiTrade(self)
        self.freeze = False
        self.actuParams()
        self.initData()
        self.imageFolder = os.path.join(os.path.dirname(__file__),'images')
        self.styleFolder = os.path.join(os.path.dirname(__file__),'styles')
        self.root = os.path.dirname(__file__).replace("\\","/")
        self.run()
    
    def actuParams(self):
        self.param = rdiParam()
        self.memoSchema = self.param.get('schema', 'CONN')  
        self.fullAdmin = self.psql.full(self.param.get('tableAdmin'), self.param.get('schema','ADMIN'))
        
    def run(self):
        self.widget = QtWidgets.QWidget()
        self.layout = QtWidgets.QVBoxLayout(self.widget)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0,0,0,0)
        self.initList()
        
        self.action = adminWidget(self.translator['action']['mod'], self.makeAction)
        self.item = adminWidget()
        self.filter = adminWidget(None, self.refreshElementList)
        self.element = adminWidget(None, self.makeModificationElement)
        self.mode = adminWidget('Postgis', self.makeInsertionMode)
        self.table = adminWidget(None, self.checkTableName)
        self.schema = adminWidget(self.psql.PGschema)
        self.layer = adminWidget(None, self.actuTableName)
        self.dump = adminWidget()
        self.layerFilter = adminWidget()
        
        self.eltName = adminWidget(None, self.autoNomAff)
        self.eltParent = adminWidget(None, self.autoNomAff)
        self.eltBloc = adminWidget()
        self.eltBlocRenameChoice = adminWidget()
        self.eltBlocRenameValue = adminWidget()
        self.eltTags = adminWidget()
        self.eltCluster = adminWidget()
        self.eltCode = adminWidget(None, self.codeChanged)
        self.eltStyle = adminWidget(None, self.autoStyle)
        self.eltStyleFill = adminWidget()
        self.eltStyleSvg = adminWidget()
        self.eltStyleEdit = adminWidget()
        self.eltStyleSize = adminWidget()
        self.eltOrder = adminWidget()
        
        self.modWidget = {'on':False}
        self.modWidget['butt'] = adminWidget()
        self.modWidget['list'] = [self.eltName, self.eltParent, self.eltTags, self.eltCode, self.eltBloc, self.eltCluster, self.eltStyle, self.eltStyleFill, self.eltStyleSvg, self.eltStyleEdit, self.eltStyleSize, self.eltOrder]
        for w in self.modWidget['list']:
            w.changed.connect(self.enabledModButton)
        
        l = QtWidgets.QLabel("Gestion des données")
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        self.layout.addWidget(l)
        
        
        sc = QtWidgets.QScrollArea()
        sc.setWidgetResizable(True)
        self.layout.addWidget(sc)
        w = QtWidgets.QWidget()
        w.setObjectName("scrollAreaWidgetContents")
        gbox = QtWidgets.QVBoxLayout()
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        w.setLayout(gbox)
        sc.setWidget(w)
        sc.setStyleSheet("QAbstractScrollArea {background-color: transparent;}  ");
        w.setStyleSheet("QWidget#scrollAreaWidgetContents {background-color: white; } ");
        
        
        wl = QtWidgets.QWidget()
        gbox.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "back.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setToolTip("Revenir aux modifications des couches existantes")
        butt.setMaximumWidth(30)
        butt.clicked.connect(partial(self.action.set, self.translator['action']['mod']))
        hbox.addWidget(butt)
        self.modButton = butt
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "add.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setToolTip("Ajouter une nouvelle couche")
        butt.setMaximumWidth(30)
        butt.clicked.connect(partial(self.action.set, self.translator['action']['add']))
        hbox.addWidget(butt)
        self.addButton = butt
        self.addLabel = QtWidgets.QLabel(self.translator['action']['add'])
        hbox.addWidget(self.addLabel)
        hbox.setAlignment(self.addLabel, Qt.AlignRight)
        
        combo = QtWidgets.QComboBox()
        combo.hide()
        hbox.addWidget(combo)
        for e in self.translator['action']['list']:
            s = self.translator['action'][e]
            combo.addItem(s, s)
        self.action.setWidget(combo)
        # ico = QtGui.QIcon(os.path.join(self.imageFolder,'switch.png'))
        # list = []
        # for e in self.translator['action']['list']:
            # list.append((ico, self.translator['action'][e]))
        # butt = toggleButton(list)
        # hbox.addWidget(butt)
        # self.action.setWidget(butt)
        
        self.action.setLayout(self.makeLayout(gbox))
        combo = QtWidgets.QComboBox()
        for s in self.itemList:
            n = self.param.get(s)
            img = os.path.join(self.imageFolder,f"{s}.png")
            if os.path.exists(img):
                combo.addItem(QtGui.QIcon(img), n, s)
            else:
                combo.addItem(n, s)
        hbox.addWidget(combo)
        self.item.setWidget(combo)
        filter = QtWidgets.QLineEdit()
        filter.setClearButtonEnabled(True)
        filter.setPlaceholderText("Mots-clefs du filtre ici")
        hbox.addWidget(filter)
        l = QtWidgets.QLabel()
        hbox.addWidget(l)
        filter.img = l
        src = os.path.join(self.imageFolder,'filterOn.png')
        pixmap = QtGui.QPixmap(src)
        pixmap = pixmap.scaledToHeight(20)
        l.setPixmap(pixmap)
        self.filter.setWidget(filter)
        self.memoId = None
        self.freeze = False
        self.dialogSVG = None
        self.action.restore()
        
        l = QtWidgets.QLabel("\n\n")
        gbox.addWidget(l)
        
    def reconnectButton(self): 
        wl = QtWidgets.QWidget()
        self.layout.addWidget(wl)
        vbox = QtWidgets.QVBoxLayout(wl)
        l = QtWidgets.QLabel()
        vbox.addWidget(l)
        l = QtWidgets.QLabel("A la fin du paramétrage, il est conseillé de reconnecter le plugin pour prendre en compte les modifications")
        l.setWordWrap(True)
        font = QtGui.QFont()
        font.setItalic(True)
        l.setFont(font)
        vbox.addWidget(l)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "plug.png"))
        butt = QtWidgets.QPushButton(ico, " Reconnecter")
        butt.setMaximumWidth(150)
        vbox.addWidget(butt)
        vbox.setAlignment(butt, Qt.AlignCenter)
        wl.butt = butt 
        self.reconnect = wl
        self.reconnect.hide()
        return wl
    
    
    def initList(self):
        self.itemList = ['enjeu', 'alea', 'stats']
        self.listElement = []
        self.alias = {}
        self.bloc = {}
        blocDict = {}
        for c in self.itemList:
            self.alias[c] = [(" ",'NULL')]
            self.bloc[c] = [(" ",'NULL')]
            blocDict[c] = {}
        memo = {}
        for elt in self.psql.getTableAdmin(compact=True):             
            id = elt.get('id')
            code = elt.get('code')
            name = elt.get('name')
            table = elt.get('tablename')
            bloc = elt.get('bloc')
            if not code:
                continue
            if not name:
                name = table
            memo[elt['id']] = name
            parent = elt.get('parent')
            aff = name
            if bloc:
                bloc = bloc.strip()
                if bloc!='' and bloc not in blocDict[code]:
                    self.bloc[code].append((bloc, bloc))
                    blocDict[code][bloc] = True
            if not table:
                aff = f"[] → {name} "
                self.alias[code].append((name, id))
            if parent:
                aff = f"[{memo[parent]}] {name}"
            self.listElement.append({'aff':aff, 'code':self.param.get(code), 'elt':elt})
            
    def makeLayout(self, parent):
        w = QtWidgets.QWidget()
        if isinstance(parent, adminWidget):
            parent = parent.layout
        parent.addWidget(w)
        layout = QtWidgets.QVBoxLayout(w)
        margins = layout.contentsMargins()
        margins.setTop(0)
        margins.setBottom(0)
        layout.setContentsMargins(margins)
        return layout
 
    def makeAction(self):
        action = self.action.get()
        if action==self.translator['action']['add']:
            self.item.widget.show()
            self.modButton.show()
            self.addLabel.show()
            self.addButton.hide()
            self.filter.widget.hide()
            self.filter.widget.img.hide()
            self.makeInsertion()
        if action==self.translator['action']['mod']:
            self.item.widget.hide()
            self.modButton.hide()
            self.addLabel.hide()
            self.addButton.show()
            self.filter.widget.show()
            self.filter.widget.img.show()
            self.makeModification()
        
    def relodAfterChanged(self):
        # self.reconnect.show()
        try:
            id = self.element.get().get('id')
        except:
            id = None
        self.dataChanged.emit(id)
    
    
    
    
    
    # INSERTION
    def makeInsertion(self):
        combo = QtWidgets.QComboBox()
        for s in [('Postgis', 'postgis.png'), ('Qgis', 'qgis.png'), ('Fichier', 'folder.png')]:
            img = os.path.join(self.imageFolder,s[1])
            combo.addItem(QtGui.QIcon(img), s[0], s[0])
        self.mode.setWidget(combo)
        self.action.add(self.mode.widget)
        self.mode.setLayout(self.makeLayout(self.action))
        self.mode.restore()


    def makeInsertionMode(self):
        mode = self.mode.get()
        if mode=='Postgis':
            self.makeInsertionPostgis()
        if mode=='Qgis':
            self.makeInsertionQgis()
        if mode=='Fichier':
            self.makeInsertionFichier()
    
    def saveInsertionMode(self):
        code = self.item.get()
        mode = self.mode.get()
        schema = self.schema.get()
        table = self.table.get()
        if not schema or not table:
            return
        if mode=='Postgis':
            QTimer.singleShot(1000, partial(self.endInsertion, self.psql.full(table, schema)))
        if mode=='Qgis':
            layer = self.layer.get()
            dump = self.dump.get() and self.dump.widget.isVisible()
            table = self.trade.createTableFromFeatures(layer, table, schema, self.endInsertion, dump)
        if mode=='Fichier':
            layer = self.layer.get()
            dump = self.dump.get() and self.dump.widget.isVisible()
            table = self.trade.createTableFromFile(layer, table, schema, self.endInsertion, dump) 
        if table:
            sql = f"INSERT INTO {self.fullAdmin} (code, tablename, schemaname)" + \
                f" VALUES ('{code}', '{table}', '{schema}')" + \
                f" RETURNING id"
            id = self.psql.sqlInsert(sql, True)
            if id!=None:
                self.initList()
                self.viewElt(id)
                self.relodAfterChanged()
    
    def viewElt(self, id=None):
        self.memoId = id
        self.action.set(self.translator['action']['mod'])
    
    
    def checkTableName(self):
        table = self.table.get()
        schema = self.schema.get()
        if self.table.error!=None:
            clearLayout(self.table.error)
            if self.psql.isTable(table, schema):
                l = QtWidgets.QLabel(f"La table est existante et sera écrasée")
                font = QtGui.QFont()
                font.setItalic(True)
                l.setFont(font)
                self.table.error.addWidget(l)
        self.refreshInstances()
        self.refreshDumpable()
        
    def refreshInstances(self):
        already = []
        self.table.clear()
        for it in self.listElement:             
            table = it['elt'].get('tablename')
            schema = it['elt'].get('schemaname')
            if table==self.table.get() and schema==self.schema.get():
                already.append(it)
        if len(already)>0:
            s = "s" if len(already)>1 else ""
            l = QtWidgets.QLabel(f"\nInstance{s} de cette table déjà importée{s} :")
            font = QtGui.QFont()
            font.setBold(True)
            l.setFont(font)
            self.table.add(l)
            for it in already:       
                hbox = QtWidgets.QHBoxLayout()
                self.table.add(hbox)
                
                ico = QtGui.QIcon(os.path.join(self.imageFolder, "delete.png"))
                b = QtWidgets.QPushButton(ico, "")
                b.setMaximumWidth(20)
                b.clicked.connect(partial(self.deleteInstance, it['elt']['id']))
                hbox.addWidget(b)
                
                l = QtWidgets.QLabel(f"{it['code']}: {it['aff']}")
                hbox.addWidget(l)
                ico = QtGui.QIcon(os.path.join(self.imageFolder, "see.png"))
                b = QtWidgets.QPushButton(ico, "")
                b.setMaximumWidth(30)
                hbox.addWidget(b)
                b.clicked.connect(partial(self.viewElt, it['elt']['id']))
                
    def refreshDumpable(self):
        table = self.table.get()
        schema = self.schema.get()
        layer = self.layer.get()
        return
        if self.dump.widget:
            if layer:
                if layer.geometryType()==0:
                    self.dump.widget.show()
                else:
                    self.dump.widget.hide()
            else:
                if self.psql.isGeomPoint(table, schema):
                    self.dump.widget.show()
                else:
                    self.dump.widget.hide()
    
    def endInsertion(self, full):
        t = full.split('.')
        schema = t[0].replace('"', "")
        table = t[1].replace('"', "")
        geom = self.setGeomTable(table, schema)
        self.setTypeTable(table, schema, geom)
        self.setIndexTable(table, schema, geom)
        self.setValidTable(table, schema)
        self.initList()
        self.refreshStyleList()
        if self.element.working:
            if self.element.working.full==full:
                try:
                    self.element.working.hide()
                except:
                    pass
                    
        try:
            elt = self.element.get()
            id = elt.get('id')
            elt = self.psql.getTableAdminElt(id)[0]
            self.showTableInfos(elt)
        except:
            pass
                    
    def setGeomTable(self, table, schema):
        geom = self.psql.getGeometryField(table, schema)
        if geom:
            geomS = f"'{geom}'"
        else:
            geomS = 'NULL'
        sql = f"UPDATE {self.fullAdmin} SET geom={geomS} WHERE tablename='{table}' and schemaname='{schema}'"
        self.psql.sqlSend(sql)
        return geom
        
    def setTypeTable(self, table, schema, geom=None):
        geomType = self.psql.getGeomType(table, schema, geom)
        type = None
        if self.psql.isGeomPoint(table, schema, geomType=geomType):
            type = "point"
        if self.psql.isGeomLinestring(table, schema, geomType=geomType):
            type = "line"
        if self.psql.isGeomPolygon(table, schema, geomType=geomType):
            type = "polygon"
        if type:
            typeS = f"'{type}'"
        else:
            typeS = 'NULL'
        sql = f"UPDATE {self.fullAdmin} SET type={typeS} WHERE tablename='{table}' and schemaname='{schema}'"
        self.psql.sqlSend(sql)
        return type
        
    def setIndexTable(self, table, schema, geom):
        index = self.psql.hasIndex(geom, table, schema)
        if index:
            indexS = 'TRUE'
        else:
            indexS = 'FALSE'
        sql = f"UPDATE {self.fullAdmin} SET index={indexS} WHERE tablename='{table}' and schemaname='{schema}'"
        self.psql.sqlSend(sql)
        return index
        
    def setValidTable(self, table, schema):
        valid = self.psql.isValidGeos(table, schema)
        if valid:
            validS = 'TRUE'
        else:
            validS = 'FALSE'
        sql = f"UPDATE {self.fullAdmin} SET valid={validS} WHERE tablename='{table}' and schemaname='{schema}'"
        self.psql.sqlSend(sql)
        return valid
    

    def makeDumpable(self, form):
        cb = QtWidgets.QCheckBox("Forcer géométrie simple")
        cb.setToolTip("Accélère le traitement du rendu cluster")
        cb.hide()
        # cb.setEnabled(False)
        form.addRow("", cb)
        self.dump.setWidget(cb)

    def makeAddButton(self):
        butt = QtWidgets.QPushButton("Ajouter")
        butt.setFixedSize(130, 30)
        self.mode.add(butt)
        self.mode.layout.setAlignment(butt, Qt.AlignCenter)
        butt.clicked.connect(self.saveInsertionMode)
        if self.psql.error:
            butt.setEnabled(False)

        

    # Postgis
    def makeInsertionPostgis(self):   
        w = QtWidgets.QWidget()
        self.mode.add(w)
        form = QtWidgets.QFormLayout(w)
        form.setContentsMargins(0,0,0,0) 
        combo = QtWidgets.QComboBox()
        for s in self.psql.getSchemas():
            combo.addItem(s, s)
        self.schema.setWidget(combo)
        form.addRow("Schéma", self.schema.widget)
        combo = QtWidgets.QComboBox()
        self.table.setWidget(combo)
        form.addRow("Table", self.table.widget)
        self.table.error = None
        self.makeDumpable(form)
        self.makeAddButton()
        self.table.setLayout(self.makeLayout(self.mode))
        self.schema.setCallback(self.refreshTablesList)
        self.schema.restore()
    
    def refreshTablesList(self):
        schema = self.schema.get()
        list = [""]
        list.extend(self.psql.getTables(schema))
        self.table.refresh(list)
        self.table.restore()
    
    
    # Qgis
    def makeInsertionQgis(self):   
        w = QtWidgets.QWidget()
        self.mode.add(w)
        form = QtWidgets.QFormLayout(w)
        form.setContentsMargins(0,0,0,0) 
        combo = QgsMapLayerComboBox()
        combo.setFilters(QgsMapLayerProxyModel.VectorLayer)
        combo.setExcludedProviders(['postgres'])
        form.addRow("Couche", combo)
        self.layer.setWidget(combo)
        cb = QtWidgets.QCheckBox("Inclure source Postgres")
        cb.stateChanged.connect(partial(self.actuComboLayer, combo, cb))
        form.addRow("", cb)
        self.layerFilter.setWidget(cb)
        
        combo = QtWidgets.QComboBox()
        for s in self.psql.getSchemas():
            combo.addItem(s, s)
        self.schema.setWidget(combo)
        form.addRow("Schéma", combo)
        l = QtWidgets.QLineEdit()
        self.table.setWidget(l)
        form.addRow("Table", l)
        self.schema.setCallback(self.checkTableName)
        self.table.error = QtWidgets.QVBoxLayout()
        form.addRow("", self.table.error)
        
        self.makeDumpable(form)
        self.makeAddButton()  
        
        self.table.setLayout(self.makeLayout(self.mode))
        self.table.set(self.layer.getText())       
        
        self.layer.freeze = True
        self.layerFilter.restore()
        self.layer.freeze = False
        self.layer.restore()
        self.schema.set(self.param.get('schema','ADMIN'))

    def actuTableName(self):
        self.table.set(self.layer.getText())
    
    def actuComboLayer(self, combo, cb):
        list = ['postgres']
        if cb.isChecked():
            list = []
        combo.setExcludedProviders(list)
        
    # Fichier    
    def makeInsertionFichier(self):
        w = QtWidgets.QWidget()
        self.mode.add(w)
        form = QtWidgets.QFormLayout(w)
        form.setContentsMargins(0,0,0,0) 
        hbox = QtWidgets.QHBoxLayout()
        form.addRow("Couche", hbox)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "folder.png"))
        b = QtWidgets.QPushButton(ico, "")
        b.setMaximumWidth(30)
        b.clicked.connect(self.openFile)
        hbox.addWidget(b)
        l = QtWidgets.QLineEdit()
        hbox.addWidget(l)
        l.hide()
        self.layer.setWidget(l)
        l = QtWidgets.QLabel()
        hbox.addWidget(l)
        self.layer.aff = l
        
        combo = QtWidgets.QComboBox()
        for s in self.psql.getSchemas():
            combo.addItem(s, s)
        self.schema.setWidget(combo)
        form.addRow("Schéma", combo)
        l = QtWidgets.QLineEdit()
        self.table.setWidget(l)
        form.addRow("Table", l)
        self.schema.setCallback(self.checkTableName)
        self.table.error = QtWidgets.QVBoxLayout()
        form.addRow("", self.table.error)
        
        self.makeDumpable(form)
        self.makeAddButton()  
        self.table.setLayout(self.makeLayout(self.mode))
        self.schema.set(self.param.get('schema','ADMIN'))
        
    def openFile(self):
        dialog = QtWidgets.QFileDialog()
        fileName = dialog.getOpenFileName(None, "Ouvrir une couche vecteur", "", "Vector Files (*.shp *.gpkg)")
        file = fileName[0]
        self.layer.set(file)
        self.layer.aff.setText(os.path.basename(file))
        nom, ext = os.path.splitext(os.path.basename(file))
        self.table.set(nom)
        QtWidgets.QApplication.processEvents()
        
    






    # SUPPRESSION
    def deleteInstance(self, id):
        sql = f"DELETE FROM {self.fullAdmin} WHERE id={id}"
        self.psql.sqlSend(sql)
        self.initList()
        # self.viewElt()
        self.action.restore()
        self.relodAfterChanged()

    def actualize(self):
        self.initList()
        self.action.restore()
        self.relodAfterChanged()
        








    # MODIFICATION
    def makeModification(self):
        w1 = QtWidgets.QWidget()
        h1 = QtWidgets.QHBoxLayout(w1)
        self.action.add(w1)
        self.action.choiceBar = w1
        combo = QtWidgets.QComboBox()
        h1.addWidget(combo)
        w2 = QtWidgets.QWidget()
        self.action.add(w2)
        w2.hide()
        self.action.multipleBar = w2
        self.action.multipleList = []
        self.element.setWidget(combo)
        self.element.setLayout(self.makeLayout(self.action))
        self.element.affList = None
        self.element.working = None
        self.refreshElementList()
        self.makeMultiple()
        if not self.element.get():
            self.makeModificationElement()
            
    def makeMultiple(self):
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "multiple.png"))
        b = QtWidgets.QPushButton(ico, "")
        b.setToolTip(f"Affecter une propriété à un ensemble de couches")
        b.setFixedSize(22, 22)
        b.clicked.connect(self.toggleMultiple)
        self.action.choiceBar.layout().addWidget(b)
        
        lay = QtWidgets.QVBoxLayout(self.action.multipleBar)
        l = QtWidgets.QLabel(f"Modification par lots :")
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        lay.addWidget(l)
        h2 = QtWidgets.QHBoxLayout()
        lay.addLayout(h2)
        l = QtWidgets.QLabel()
        h2.addWidget(l)
        self.action.multipleBar.label = l
        self.affMultipleCount()
        b = QtWidgets.QPushButton(ico, "")
        b.setFixedSize(22, 22)
        h2.addWidget(b)
        h2.setAlignment(b, Qt.AlignLeft)
        self.action.multipleBar.valid = b
        b.clicked.connect(self.chooseMultiple)
        b.state = "mod"
        self.chooseMultiple()
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "back.png"))
        b = QtWidgets.QPushButton(ico, "")
        b.setToolTip(f"Revenir à la modification unitaire")
        b.setFixedSize(22, 22)
        b.clicked.connect(self.toggleMultiple)
        h2.addWidget(b)
        h2.setAlignment(b, Qt.AlignRight)
    
    def makeModificationMultipleButton(self, form, target=None):
        hbox = QtWidgets.QHBoxLayout()
        form.addRow("", hbox)
        b = QtWidgets.QPushButton("Affecter")
        b.clicked.connect(partial(self.saveModificationMultiple, target))
        hbox.addWidget(b)
        hbox.setAlignment(Qt.AlignRight)
        form.addRow(" ", QtWidgets.QHBoxLayout())
        if len(self.action.multipleListChoosen)<1:
            b.setEnabled(False)
        
    def toggleMultiple(self):
        if self.action.multipleBar.isVisible():
            self.action.multipleBar.hide()
            self.action.choiceBar.show()
            if self.action.multipleMemo:
                self.element.set(self.action.multipleMemo)
            else:
                self.refreshElementList()
        else:
            self.action.multipleMemo = self.element.get()
            self.element.set(None)
            self.action.multipleBar.show()
            self.action.choiceBar.hide()
            self.refreshElementList()
    
    def setMultipleAll(self):
        state = self.action.multipleAllButton.isChecked()
        for w in self.action.multipleList:
            w.setChecked(state)
            
    def actuMultipleAll(self):
        try:
            self.action.multipleAllButton.blockSignals(True)
            if len(self.action.multipleListChoosen)>=len(self.action.multipleList):
                self.action.multipleAllButton.setChecked(True)
            else:
                self.action.multipleAllButton.setChecked(False)
            self.action.multipleAllButton.blockSignals(False)
        except:
            pass
    
    def affMultipleCount(self):
        self.action.multipleListChoosen = []
        for w in self.action.multipleList:
            if w.isChecked():
                self.action.multipleListChoosen.append(w.elt['id'])
        nb = len(self.action.multipleListChoosen)
        s = ""     
        if nb>1:
            s = "s"
        self.action.multipleBar.label.setText(f"{nb} élément{s} sélectionné{s}")
        self.actuMultipleAll()
    
    def chooseMultiple(self):
        if self.action.multipleBar.valid.state=="ok":
            self.action.multipleBar.valid.state = "mod"
            self.action.multipleBar.valid.setIcon(QtGui.QIcon(os.path.join(self.imageFolder, "modify.png")))
            self.action.multipleBar.valid.setToolTip(f"Modifier la sélection")
        else:
            self.action.multipleBar.valid.state = "ok"
            self.action.multipleBar.valid.setIcon(QtGui.QIcon(os.path.join(self.imageFolder, "ok.png")))
            self.action.multipleBar.valid.setToolTip(f"Valider la sélection")
        self.refreshElementList()
        
    def decomposeFilter(self, list, char):
        if isinstance(list, str):
            return list.lower().split(char)
        else:
            l = []
            for e in list:
                l.extend(e.split(char))
            return l
    
    def matchElement(self, it, needles):
        bool = False
        for e in needles:
            if bool:
                break
            if len(e)>0:
                for s in ['aff', 'code']:
                    if s in it and it[s]:
                        bool = bool or (it[s].lower().find(e)>=0)
                for k in ['code', 'tablename', 'name', 'tags', 'group']:
                    if bool:
                        break
                    if 'elt' in it and it['elt'] and it['elt'].get(k):
                        v = it['elt'][k].lower()
                        if len(v)>=len(e):
                            bool = bool or (v.find(e)>=0)
        return bool
        
    def getNeedles(self, needle):
        list = needle
        list = self.decomposeFilter(list, " ")
        list = self.decomposeFilter(list, ",")
        list = self.decomposeFilter(list, "+")
        return list
    
    def refreshElementList(self):
        needle = self.filter.get()
        bool = (len(needle)<1)
        needles = self.getNeedles(needle)
        list = [("", None)]
        for it in self.listElement:             
            if bool or self.matchElement(it, needles):
                code = it['elt'].get('code')
                img = os.path.join(self.imageFolder,f"{code}.png")
                if os.path.exists(img):
                    list.append((QtGui.QIcon(img), it['aff'], it['elt']))
                else:
                    list.append((it['aff'], it['elt']))
        self.element.refresh(list)
        self.element.set({'id':self.memoId})
        if not self.element.get():
            gbox = QtWidgets.QVBoxLayout()
            if self.action.multipleBar.isVisible():
                self.action.multipleList = []
                gbox.setSpacing(0)
                if self.action.multipleBar.valid.state=="ok":
                    lab = QtWidgets.QCheckBox("TOUS")
                    if len(self.action.multipleListChoosen)==len(list)-1:
                        lab.setChecked(True)
                    font = QtGui.QFont()
                    font.setItalic(True)
                    font.setBold(True)
                    lab.setFont(font)
                    lab.stateChanged.connect(self.setMultipleAll)
                    gbox.addWidget(lab)  
                    self.action.multipleAllButton = lab
                    # gbox.addWidget(QtWidgets.QLabel(" "))  
                    gbox.addSpacing(5)
                
            self.element.add(gbox)
            self.element.affList = gbox
            noIndex = False
            for l in list:
                indLib = len(l)-2
                indElt = len(l)-1
                if l[indElt]!=None: 
                    hbox = QtWidgets.QHBoxLayout()
                    hbox.setSpacing(0)
                    gbox.addLayout(hbox)
                    
                    aff = ""
                    if l[indElt].get('tablename') and not l[indElt].get('index'):
                        aff = "  ‼"
                        noIndex = True
                    
                    if self.action.multipleBar.isVisible():
                        if self.action.multipleBar.valid.state=="ok":
                            lab = QtWidgets.QCheckBox(l[indLib])
                            if l[indElt]['id'] in self.action.multipleListChoosen:
                                lab.setChecked(True)
                            lab.elt = l[indElt]
                            self.action.multipleList.append(lab)
                            lab.setIcon(l[0])
                            lab.stateChanged.connect(self.affMultipleCount)
                            hbox.addWidget(lab)  
                    else:
                        if len(l)>2:
                            ll = QtWidgets.QLabel()
                            ll.setPixmap(l[0].pixmap(15,15))
                        else:
                            c = l[indElt].get('code')[:1]
                            ll = QtWidgets.QLabel(f"[{c}] ")
                        ll.setMaximumWidth(20)
                        hbox.addWidget(ll)
                        lab = clickLabel(l[indLib] + aff)    
                        lab.clicked.connect(partial(self.element.set, {'id':l[indElt]['id']}))
                        hbox.addWidget(lab)  
            if not self.action.multipleBar.isVisible() and noIndex:
                l = QtWidgets.QLabel("\n‼ indique une table qui n'a pas d'index spatial")
                l.setWordWrap(True)
                font = QtGui.QFont()
                font.setItalic(True)
                l.setFont(font)
                gbox.addWidget(l)
                    
            if self.action.multipleBar.isVisible() and self.action.multipleBar.valid.state=="mod":     
                w = QtWidgets.QWidget()
                self.element.add(w)
                form = QtWidgets.QFormLayout(w)
                form.setContentsMargins(0,0,0,0)
                self.makeModificationElementStyle(form)
                self.makeModificationMultipleButton(form, "style")
                self.refreshStyleList()
                self.makeModificationElementBloc(form)
                self.makeModificationMultipleButton(form, "bloc")
                self.refreshBlocList()
     
    def makeModificationElementStyle(self, form):
        wl = QtWidgets.QLabel("Style")
        wf = QtWidgets.QComboBox()
        wf.setToolTip("Définit le rendu visuel de la couche, rendu par défaut de Qgis si laissé en \"Auto\"")
        self.eltStyle.setWidget(wf)
        hbox = QtWidgets.QHBoxLayout()
        form.addRow(wl, hbox)
        hbox.addWidget(wf)
        self.dock.makeHelpButton(hbox, 'p4-data-style')
        self.eltStyle.setLayout(QtWidgets.QVBoxLayout())
        form.addRow("", self.eltStyle.layout)
        self.eltStyle.detail = None
    
    def makeModificationElementBloc(self, form):
        wl = QtWidgets.QLabel("Bloc")
        hbox = QtWidgets.QHBoxLayout()
        form.addRow(wl, hbox)
        wf = QtWidgets.QComboBox()
        wf.setToolTip("Bloc de regroupement pour l'arboresence déroulante")
        wf.setEditable(True)
        wf.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        wf.editTextChanged.connect(self.enabledModButton)
        self.eltBloc.setWidget(wf)
        hbox.addWidget(wf)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "modify.png"))
        b = QtWidgets.QPushButton(ico, "")
        b.setFixedSize(22, 22)
        b.clicked.connect(self.renameBlocAff)
        hbox.addWidget(b)
        self.dock.makeHelpButton(hbox, 'p4-data-block')
        w = QtWidgets.QWidget()
        hbox = QtWidgets.QHBoxLayout(w)
        hbox.setSpacing(0)
        hbox.setContentsMargins(0,0,0,10)
        form.addRow(w)
        w.hide()
        self.eltBlocRename = w
        l = QtWidgets.QLabel(f"       →  Renommer ")
        hbox.addWidget(l)
        wf = QtWidgets.QComboBox()
        wf.currentIndexChanged.connect(self.renameBlocChange)
        self.eltBlocRenameChoice.setWidget(wf)
        hbox.addWidget(wf)
        wf = QtWidgets.QLineEdit()
        self.eltBlocRenameValue.setWidget(wf)
        hbox.addWidget(wf)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "ok.png"))
        b = QtWidgets.QPushButton(ico, "")
        b.setFixedSize(22, 22)
        b.clicked.connect(self.renameBlocGo)
        hbox.addWidget(b)
        self.eltBlocRename.ok = b
    
    def makeModificationElement(self):
        self.element.affList = None
        elt = self.element.get()
        if elt:
            self.freeze = True
            
            self.memoId = elt.get('id')
            table = elt.get('tablename')
            name = elt.get('name', "") or ""
            code = elt.get('code')
            children = self.getChildren()

            hbox = QtWidgets.QHBoxLayout()
            hbox.setContentsMargins(0,0,0,0)
            hbox.setSpacing(0)
            self.element.add(hbox)
            if table:
                schema = elt.get('schemaname') or self.psql.PGschema
                txt = f"BD: {table} ({schema})"
                unknown = "[?]"
                if self.param.get('baseAdmin'):
                    l = clickLabel(unknown)      
                    l.clicked.connect(self.modBase)
                    w = QtWidgets.QWidget()
                    self.element.add(w)
                    w.hide()
                    self.element.adminBase = w
                    hbox2 = QtWidgets.QHBoxLayout(w)
                    hbox2.setContentsMargins(0,0,0,20)
                    hbox2.setSpacing(0)
                    combo = QtWidgets.QComboBox()
                    for s in self.psql.getSchemas():
                        combo.addItem(s, s)
                    combo.setCurrentText(schema)
                    hbox2.addWidget(combo)
                    le = QtWidgets.QLineEdit(table)
                    hbox2.addWidget(le)
                    ico = QtGui.QIcon(os.path.join(self.imageFolder, "ok.png"))
                    b = QtWidgets.QPushButton(ico, "")
                    b.setFixedSize(22, 22)
                    b.clicked.connect(partial(self.trade.changeTableReference, table, schema, le, combo, self.actualize))
                    hbox2.addWidget(b)
                    ico = QtGui.QIcon(os.path.join(self.imageFolder, "close.png"))
                    b = QtWidgets.QPushButton(ico, "")
                    b.setFixedSize(22, 22)
                    b.clicked.connect(self.modBase)
                    # hbox2.addWidget(b)
                else:
                    l = QtWidgets.QLabel(unknown)
                self.element.tableType = l
                l.setMaximumWidth(20)
                hbox.addWidget(l)
            else:
                txt = f"Groupe pour choix multiple"
                self.element.tableType = None
            l = QtWidgets.QLabel(txt)
            l.setWordWrap(True)
            font = QtGui.QFont()
            font.setItalic(True)
            l.setFont(font)
            hbox.addWidget(l)
            
            if table:
                box = QtWidgets.QVBoxLayout()
                self.element.add(box)
                self.element.tableInfos = box
                full = self.psql.full(table, schema)
                running = ""
                if not self.psql.isTable(table, schema):
                    running = "       référence introuvable..."
                if full in self.psql.manager.waiting:
                    running = "       insertion en cours..."
                if running!="":
                    l = QtWidgets.QLabel(running)
                    l.full = full
                    l.setWordWrap(True)
                    font = QtGui.QFont()
                    font.setItalic(True)
                    l.setFont(font)
                    self.element.tableInfos.addWidget(l)
                    self.element.working = l
                else:
                    self.showTableInfos(elt)
            
            w = QtWidgets.QWidget()
            self.element.add(w)
            form = QtWidgets.QFormLayout(w)
            form.setContentsMargins(0,0,0,0)
            
            wl = QtWidgets.QLabel("Dans")
            wf = QtWidgets.QComboBox()
            wf.setToolTip("Appartenance de la couche ou du groupe")
            if len(children)>0:
                wf.setEnabled(False)
            for i,s in enumerate(self.itemList):
                n = self.param.get(s)
                img = os.path.join(self.imageFolder,f"{s}.png")
                if os.path.exists(img):
                    wf.addItem(QtGui.QIcon(img), n, s)
                else:
                    wf.addItem(n, s)
                if s==code:
                    wf.setCurrentIndex(i)
            self.eltCode.setWidget(wf)
            hbox = QtWidgets.QHBoxLayout()
            form.addRow(wl, hbox)
            hbox.addWidget(wf)
            self.dock.makeHelpButton(hbox, 'p4-data-in')
            
            wl = QtWidgets.QLabel("Alias")
            wf = QtWidgets.QLineEdit(name)
            wf.setToolTip("Alias de substitution qui s'affichera à la place du nom de la table")
            self.eltName.setWidget(wf)
            hbox = QtWidgets.QHBoxLayout()
            form.addRow(wl, hbox)
            hbox.addWidget(wf)
            self.dock.makeHelpButton(hbox, 'p4-data-aka')
            if not table:
                wf.setPlaceholderText("Alias du groupe obligatoire")
                self.eltName.label = wl

            self.makeModificationElementStyle(form)
            
            tags = elt.get('tags', "") or ""
            wl = QtWidgets.QLabel("Mots-clefs")
            wf = QtWidgets.QLineEdit(tags)
            wf.setToolTip("Mots-clefs utilisés dans la recherche pour filtrer les éléments\n(en plus d'autres champs comme l'alias, la table, le groupe, etc)")
            self.eltTags.setWidget(wf)
            hbox = QtWidgets.QHBoxLayout()
            form.addRow(wl, hbox)
            hbox.addWidget(wf)
            self.dock.makeHelpButton(hbox, 'p4-data-block')
            
            if table:
                wl = QtWidgets.QLabel("Groupe")
                hbox = QtWidgets.QHBoxLayout()
                form.addRow(wl, hbox)
                wf = QtWidgets.QComboBox()
                wf.setToolTip("Groupe de rattachement pour faire des listes déroulantes en mode \"compact\"")
                wf.setEditable(True)
                wf.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
                wf.editTextChanged.connect(self.enabledModButton)
                self.eltParent.setWidget(wf)
                hbox.addWidget(wf)
                self.nameAff = QtWidgets.QLabel()
                form.addRow(self.nameAff)
                self.dock.makeHelpButton(hbox, 'p4-data-group')
            else:
                self.eltParent.setWidget()
                self.nameAff = None
            
            self.makeModificationElementBloc(form)
                        
            if table:
                wl = QtWidgets.QLabel("Cluster")
                hbox = QtWidgets.QHBoxLayout()
                form.addRow(wl, hbox)
                rdiField = rdiLayerField(rdiLayer(elt), self.psql)
                rdiField.setNumeric()
                rdiField.makeCombo(hbox)
                wl.setToolTip("Choix par défaut du champ à sommer pour le rendu cluster")
                self.eltCluster.setWidget(rdiField.combo)
                self.dock.makeHelpButton(hbox, 'p4-data-cluster')
            if table:
                wl = QtWidgets.QLabel("Stats")
                hbox = QtWidgets.QHBoxLayout()
                form.addRow(wl, hbox)
                rdi = rdiLayer(elt)
                rdi.stat = rdiStatLayer(rdi, hbox, self.dock)
                rdi.stat.dataChanged.connect(self.enabledModButton)
                self.eltStats = rdi
                self.dock.makeHelpButton(hbox, 'p4-data-stats')
            else:
                self.eltStats = None
                
            wl = QtWidgets.QLabel("Ordre")
            hbox = QtWidgets.QHBoxLayout()
            form.addRow(wl, hbox)
            wf = QtWidgets.QSpinBox()
            order = elt.get('order', "") or ""
            if order!="":
                wf.setValue(order)
            wf.setToolTip("Ordonnacement des couches (supplantera l'ordre alphabétique)")
            self.eltOrder.setWidget(wf)
            hbox.addWidget(wf)
            self.dock.makeHelpButton(hbox, 'p4-data-order')
                
            hbox = QtWidgets.QHBoxLayout()
            self.element.add(hbox)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "back.png"))
            b = QtWidgets.QPushButton(ico, "")
            b.setFixedSize(20, 30)
            b.clicked.connect(self.element.set)
            hbox.addWidget(b)
            hbox.setAlignment(b, Qt.AlignLeft)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "save.png"))
            butt = QtWidgets.QPushButton(ico, "  Enregistrer")
            butt.setFixedSize(130, 30)
            hbox.addWidget(butt)
            hbox.setAlignment(butt, Qt.AlignCenter)
            butt.clicked.connect(self.saveModificationElement)
            self.modWidget['butt'].setWidget(butt)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "delete.png"))
            b = QtWidgets.QPushButton(ico, "")
            b.setFixedSize(20, 30)
            b.clicked.connect(partial(self.deleteInstance, elt.get('id')))
            hbox.addWidget(b)
            hbox.setAlignment(b, Qt.AlignRight)
            if len(children)>0:
                b.setEnabled(False)
                
            self.codeChanged()
            self.refreshStyleList()
            self.autoNomAff()
            butt.setEnabled(False)
            self.freeze = False
            
            if table:
                if elt.get('parent'):
                    l = QtWidgets.QLabel(f"\nRegroupé dans :")
                    font = QtGui.QFont()
                    font.setUnderline(True)
                    l.setFont(font)
                    self.element.add(l)
                    hbox = QtWidgets.QHBoxLayout()
                    self.element.add(hbox)
                    l = QtWidgets.QLabel(f" {self.eltParent.widget.currentText()}")
                    hbox.addWidget(l)
                    ico = QtGui.QIcon(os.path.join(self.imageFolder, "see.png"))
                    b = QtWidgets.QPushButton(ico, "")
                    b.setMaximumWidth(30)
                    hbox.addWidget(b)
                    b.clicked.connect(partial(self.element.set, {'id':elt['parent']}))
            else:
                l = QtWidgets.QLabel()
                self.element.add(l)
                hbox = QtWidgets.QHBoxLayout()
                self.element.add(hbox)
                ico = QtGui.QIcon(os.path.join(self.imageFolder, "refresh.png"))
                b = QtWidgets.QPushButton(ico, "")
                b.setIconSize(QSize(11, 11))
                b.setFixedSize(15, 15)
                b.clicked.connect(self.actualize)
                hbox.addWidget(b)
                l = QtWidgets.QLabel(f"Dépendances :")
                font = QtGui.QFont()
                font.setUnderline(True)
                l.setFont(font)
                hbox.addWidget(l)
                
                gbox = QtWidgets.QVBoxLayout()
                self.element.add(gbox)
                for it in children:             
                    hbox = QtWidgets.QHBoxLayout()
                    gbox.addLayout(hbox)
                    ico = QtGui.QIcon(os.path.join(self.imageFolder, "delete.png"))
                    b = QtWidgets.QPushButton(ico, "")
                    b.setMaximumWidth(20)
                    b.clicked.connect(partial(self.setParent, it['elt']['id'], 'NULL'))
                    hbox.addWidget(b)
                    
                    wf = QtWidgets.QSpinBox()
                    wf.id = it['elt']['id']
                    wf.setMaximumWidth(30)
                    order = it['elt'].get('order', "") or ""
                    if order!="":
                        wf.setValue(order)
                    hbox.addWidget(wf)
                    wf.valueChanged.connect(partial(self.setOrder, wf))
                    
                    l = QtWidgets.QLabel(f" {it['aff']}")
                    hbox.addWidget(l)
                    ico = QtGui.QIcon(os.path.join(self.imageFolder, "see.png"))
                    b = QtWidgets.QPushButton(ico, "")
                    b.setMaximumWidth(30)
                    hbox.addWidget(b)
                    b.clicked.connect(partial(self.element.set, {'id':it['elt']['id']}))
                hbox = QtWidgets.QHBoxLayout()
                gbox.addLayout(hbox)
                combo = QtWidgets.QComboBox()
                hbox.addWidget(combo)
                combo.addItem("", None)
                for it in self.listElement:             
                    if it['elt']['tablename'] and it['elt']['code']==code and it['elt']['parent']==None:
                        combo.addItem(it['aff'], it['elt']['id'])
                b = QtWidgets.QPushButton("Ajouter")
                b.setMaximumWidth(60)
                hbox.addWidget(b)
                b.clicked.connect(partial(self.setParent, combo, elt.get('id')))
        else:
            self.memoId = None
            self.refreshElementList()
            
    def modBase(self):
        if self.element.adminBase.isVisible():
            self.element.adminBase.hide()
        else:    
            self.element.adminBase.show()
    
    def renameBlocAff(self):
        if self.eltBlocRename.isVisible():
            self.eltBlocRename.hide()
        else:
            self.eltBlocRename.show()
    
    def renameBlocChange(self):
        val = self.eltBlocRenameChoice.get() or ""
        if val=="NULL":
            val = ""
        self.eltBlocRenameValue.set(val)
        if val=="":
            self.eltBlocRename.ok.setEnabled(False)
        else:
            self.eltBlocRename.ok.setEnabled(True)
        
    def renameBlocGo(self):
        bloc = self.eltBlocRenameValue.get()
        if bloc and bloc!="":
            sql = f"UPDATE {self.fullAdmin} SET bloc='{bloc}' WHERE bloc='{self.eltBlocRenameChoice.get()}'"
            self.psql.sqlSend(sql, True)
            self.initList()
            self.refreshBlocList()
    
    def codeChanged(self):
        self.refreshParentList()
        # self.refreshStyleList()
        self.refreshBlocList()
        
    def enabledModButton(self):
        elt = self.element.get()
        if elt==None:
            return
        if not self.freeze:
            butt = self.modWidget['butt'].widget
            butt.setEnabled(True)
        if not elt.get('tablename'):
            if self.eltName.get().strip()=="":
                butt.setEnabled(False)
                self.eltName.label.setStyleSheet("color:red;")
            else:
                self.eltName.label.setStyleSheet("")
    
    def getChildren(self, id=None):
        list = []
        if id==None:
            elt = self.element.get()
            id = elt['id']
        for it in self.listElement:             
            if it['elt']['parent']==id:
                list.append(it)
        return list
    
    def setParent(self, target, parent):
        id = None
        if isinstance(target, int):
            id = target
        if isinstance(target, QtWidgets.QComboBox):
            id = target.currentData()
        if id:
            sql = f"UPDATE {self.fullAdmin} SET parent={parent} WHERE id={id}"
            self.psql.sqlSend(sql, True)
            self.initList()
            self.action.restore()
            self.relodAfterChanged()
            
    def setOrder(self, spin):
        id = spin.id
        order = spin.value()
        sql = f"UPDATE {self.fullAdmin} SET \"order\"={order} WHERE id={id}"
        self.psql.sqlSend(sql, True)
        self.initList()
    
    def askTableInfos(self, elt):
        table = elt.get('tablename')
        schema = elt.get('schemaname')
        self.setTypeTable(table, schema)
            
    def showTableInfos(self, elt):
        table = elt.get('tablename')
        schema = elt.get('schemaname')
        if elt.get('type'):
            type = elt.get('type')
        else:
            type = self.setTypeTable(table, schema)
        img = f"{type}.png"
        ico = QtGui.QIcon(os.path.join(self.imageFolder, img))
        self.element.tableType.setPixmap(ico.pixmap(15,15))
        clearLayout(self.element.tableInfos)
        geom = elt.get('geom')
        if not geom:
            geom = self.setGeomTable(table, schema)
        index = elt.get('index')
        if index==None:
            index = self.setIndexTable(table, schema, geom)
        if not index:
            hbox = QtWidgets.QHBoxLayout()
            hbox.setAlignment(Qt.AlignRight)
            self.element.tableInfos.addLayout(hbox)
            l = QtWidgets.QLabel("Créer un index spatial")
            hbox.addWidget(l)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "index.png"))
            b = QtWidgets.QPushButton(ico, "")
            b.setMaximumWidth(30)
            b.clicked.connect(partial(self.addSpatialIndex, elt, b))
            hbox.addWidget(b)
        valid = elt.get('valid') 
        if not valid:
            if valid==False:
                txt = "Réparer la géométrie"
                img = "fix.png"
                func = self.fixGeometry
            else:
                txt = "Tester la géométrie"
                img = "try.png"
                func = self.tryGeometry
            hbox = QtWidgets.QHBoxLayout()
            hbox.setAlignment(Qt.AlignRight)
            self.element.tableInfos.addLayout(hbox)
            l = QtWidgets.QLabel(txt)
            hbox.addWidget(l)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, img))
            b = QtWidgets.QPushButton(ico, "")
            b.setMaximumWidth(30)
            b.clicked.connect(partial(func, elt, b))
            hbox.addWidget(b)
    
    def addSpatialIndex(self, elt, butt):
        butt.setEnabled(False)
        id = elt.get('id')
        table = elt.get('tablename')
        schema = elt.get('schemaname')
        geom = elt.get('geom')
        if not geom:
            geom = self.psql.getGeometryField(table, schema)
            self.setGeomTable(table, schema)
        self.psql.makeSpatialIndex(table, schema, geom)
        self.setIndexTable(table, schema)
        self.initList()
        self.action.restore()
        self.relodAfterChanged()
    
    def tryGeometry(self, elt, butt):
        butt.setEnabled(False)
        id = elt.get('id')
        table = elt.get('tablename')
        schema = elt.get('schemaname')
        self.setValidTable(table, schema)
        self.initList()
        self.action.restore()
        self.relodAfterChanged()
        
    def fixGeometry(self, elt, butt):
        butt.setEnabled(False)
        id = elt.get('id')
        table = elt.get('tablename')
        schema = elt.get('schemaname')
        geom = elt.get('geom')
        if not geom:
            geom = self.psql.getGeometryField(table, schema)
            self.setGeomTable(table, schema)
        self.psql.makeValidGeos(table, schema, geom)
        valid = self.psql.isValidGeos(table, schema, geom)
        self.setValidTable(table, schema)
        self.initList()
        self.action.restore()
        self.relodAfterChanged()
     
    def refreshParentList(self):
        if self.eltParent.widget!=None:
            elt = self.element.get()
            code = self.eltCode.get()
            self.eltParent.refresh(self.alias[code])
            self.eltParent.set(elt.get('parent'), True)
    
    def refreshBlocList(self):
        elt = self.element.get()
        if elt==None:
            list = [(" ",'NULL')]
            list.extend(self.psql.getTableAdminBlocs(mixed=True))
            self.eltBloc.refresh(list)
            self.eltBlocRenameChoice.refresh(list)
        else:
            code = self.eltCode.get()
            self.eltBloc.refresh(self.bloc[code])
            self.eltBloc.set(elt.get('bloc'), True)
            self.eltBlocRenameChoice.refresh(self.bloc[code])
            self.eltBlocRenameChoice.set(elt.get('bloc'), True)
    
    
    def getFileStyle(self, a, data):
        for file in os.listdir(self.styleFolder):
            try:
                name, ext = os.path.splitext(file)
                if ext.lower()!='.json':
                    continue
                f = open(os.path.join(self.styleFolder, file), 'r')
                js = json.loads(f.read())
                f.close()
                b = {'file':file, 'do':'file', 'content':a['content'], 'label':js['name']}
                if 'target' in js:
                    b['target'] = js['target']
                if 'vars' in js:
                    b['vars'] = js['vars']
                if os.path.isfile(os.path.join(self.styleFolder, name+".freeze")):
                    if file==data['jsStyle'].get('file'):
                        b['do'] = None
                        b['content'] = elt.get('style')
                        toGray = True
                    else:
                        continue
                self.actuStyleItem(b, data)
            except:
                print(sys.exc_info())
                print(traceback.format_exc())
                pass
    
    def refreshStyleList(self):
        self.eltStyle.widget
        elt = self.element.get()
        toGray = False
        if elt==None:
            data = {'elt':{}, 'jsStyle':{}, 'code':None, 'list':[], 'ind':0, 'found':0, 'isPoint':False, 'isPolygon':False, 'isLine':False, 'isGroup':True}
        else:
            jsStyle = self.jsStyle(elt.get('style'))
            code = self.eltCode.get()
            table = elt.get('tablename')
            schema = elt.get('schemaname')
            isPoint = self.psql.isGeomPoint(table, schema)
            isLine = self.psql.isGeomLinestring(table, schema)
            isPolygon = self.psql.isGeomPolygon(table, schema)
            data = {'elt':elt, 'jsStyle':jsStyle, 'code':code, 'list':[], 'ind':0, 'found':0, 'isPoint':isPoint, 'isPolygon':isPolygon, 'isLine':isLine, 'isGroup':table==None}
        for a in self.translator['style']:
            if 'type' in a and a['type']=='file':
                self.getFileStyle(a, data)
            else:
                self.actuStyleItem(a, data)
        self.eltStyle.refresh(data['list'])
        self.eltStyle.set(data['found'])
        if toGray:
            self.eltStyle.widget.setItemData(data['found'], QtGui.QColor('#888888'), Qt.TextColorRole)
        
    def actuStyleItem(self, a, data):
        if (not 'target' in a) or (data['isGroup']) or (a['target']=="point" and data['isPoint']) or (a['target']=="polygon" and data['isPolygon']) or (a['target']=="line" and data['isLine']):
            if 'file' in a:
                ico = QtGui.QIcon(os.path.join(self.imageFolder, 'file.png'))
                if a['do']==None:
                    ico = QtGui.QIcon(os.path.join(self.imageFolder, 'file-off.png'))
                data['list'].append((ico, a['label'], a))
            else:
                data['list'].append((a['label'], a))
            if data['elt'].get('style'):
                if 'content' in a:
                    js = a['content'].split('_self_')[0]
                    if 'file' in a:
                        if data['jsStyle'].get('file')==a['file']:
                            data['found'] = data['ind']
                    elif js!="" and data['elt'].get('style').lower().find(js)>=0:
                        data['found'] = data['ind']
                    elif js=="" and not data['found']:
                        data['found'] = data['ind']
            data['ind'] += 1
        return ""
    
    def autoNomAff(self):
        # if self.freeze:
            # return
        elt = self.element.get()
        txt = self.eltName.get()
        if self.nameAff!=None:
            if txt=="":
                txt = elt.get('tablename')
            if self.eltParent.widget!=None:
                parent = self.eltParent.getText()
                if parent.strip()!="":
                    txt = f"{parent} ({txt})"
                    self.eltCode.widget.setEnabled(False)
                else:
                    self.eltCode.widget.setEnabled(True)
            self.nameAff.setText(f"       →  {txt}")
    
    def autoStyle(self, data=None):
        self.eltStyle.clear()
        elt = self.element.get()
        if elt==None:
            js = {}
            txt = ""
        else:
            js = self.jsStyle(elt.get('style'))
            txt = elt.get('style') or ""
        style = self.eltStyle.get()
        content = style.get('content')
        self.memoStyle = {}
        do = style.get('do')
        self.eltStyle.detail = None
        self.eltStyleFill.setWidget()
        self.eltStyleSvg.setWidget()
        self.eltStyleEdit.setWidget()
        self.eltStyleSize.setWidget()
        

        if do=='edit':
            w = QtWidgets.QLineEdit(txt)
            self.eltStyle.add(w)
            self.eltStyle.detail = w
            self.eltStyleEdit.setWidget(w)
            
        if do=='qml':
            hbox = QtWidgets.QHBoxLayout()
            self.eltStyle.add(hbox)
            hsbox = QtWidgets.QHBoxLayout()
            hbox.addLayout(hsbox)
            hbox.setAlignment(hsbox, Qt.AlignLeft)
            l = QtWidgets.QLabel(f" Voir")
            hsbox.addWidget(l)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "qml.png"))
            bView = QtWidgets.QPushButton(ico, "")
            bView.setMaximumWidth(30)
            hsbox.addWidget(bView)
            hsbox = QtWidgets.QHBoxLayout()
            hbox.addLayout(hsbox)
            hbox.setAlignment(hsbox, Qt.AlignRight)
            l = QtWidgets.QLabel(f" Charger")
            hsbox.addWidget(l)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "folder.png"))
            bOpen = QtWidgets.QPushButton(ico, "")
            bOpen.setMaximumWidth(30)
            hsbox.addWidget(bOpen)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "qgis.png"))
            bQgis = QtWidgets.QPushButton(ico, "")
            bQgis.setMaximumWidth(30)
            hsbox.addWidget(bQgis)
            bView.clicked.connect(partial(self.qmlChooseAction, 'qml'))
            bOpen.clicked.connect(partial(self.qmlChooseAction, 'open'))
            bQgis.clicked.connect(partial(self.qmlChooseAction, 'qgis'))
            self.eltStyle.bView = bView
            
            w = QtWidgets.QWidget()
            self.eltStyle.add(w)
            hbox = QtWidgets.QHBoxLayout(w) 
            l = QtWidgets.QLabel()
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "qgis.png"))
            l.setMaximumWidth(20)
            l.setPixmap(ico.pixmap(15,15))
            hbox.addWidget(l)
            combo = QgsMapLayerComboBox()
            combo.setFilters(QgsMapLayerProxyModel.VectorLayer)
            hbox.addWidget(combo)
            self.selLoadedLayer(combo)
            w.combo = combo
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "ok.png"))
            b = QtWidgets.QPushButton(ico, "")
            b.setMaximumWidth(30)
            hbox.addWidget(b)
            b.clicked.connect(self.getStyleFromLayer)
            w.butt = b
            self.eltStyle.wQgis = w
            w = QtWidgets.QWidget()
            self.eltStyle.add(w)
            hbox = QtWidgets.QHBoxLayout(w) 
            l = QtWidgets.QLabel()
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "folder.png"))
            l.setMaximumWidth(20)
            l.setPixmap(ico.pixmap(15,15))
            hbox.addWidget(l)
            l = QtWidgets.QLabel()
            hbox.addWidget(l)
            w.label = l
            w.file = ""
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "ok.png"))
            b = QtWidgets.QPushButton(ico, "")
            b.setMaximumWidth(30)
            hbox.addWidget(b)
            b.clicked.connect(self.getStyleFromQml)
            w.butt = b
            self.eltStyle.wOpen = w
            self.eltStyle.detail = {'qml':True, 'value':txt}
            self.qmlChooseAction()

  
        if do=='simple':
            fill = js.get('fill')
            size = js.get('size') or 1
            if data:
                fill = data.get('fill', fill)
                size = data.get('size', size)
            self.memoStyle['fill'] = fill
            hbox = QtWidgets.QHBoxLayout()
            self.eltStyle.add(hbox)
            w = QgsColorButton()
            hbox.addWidget(w)
            self.eltStyle.detail = w
            self.eltStyleFill.setWidget(w)
            self.eltStyleFill.set(fill)
            w.colorChanged.connect(partial(self.reloadStyleSVG, w))
            w = QtWidgets.QDoubleSpinBox()
            self.eltStyleSize.setWidget(w)
            w.setRange(0.1, 10)
            try:
                w.setStepType(QAbstractSpinBox.AdpativeDecimalStepType)
            except:
                pass
            w.setValue(float(size))
            hbox.addWidget(w)
            w.valueChanged.connect(self.enabledModButton)
            w.valueChanged.connect(partial(self.changeStyleDetail, 'size', w))
            
            self.eltStyle.detail = {'simple':True, 'fill':fill, 'size':size}

            
        if do=='svg':
            file = js.get('marker', {}).get('svg')
            code = js.get('marker', {}).get('code') or None
            fill = js.get('marker', {}).get('fill')
            size = js.get('marker', {}).get('size') or 1
            if data:
                file = data.get('svg', file)
                code = data.get('code', code)
                fill = data.get('fill', fill)
                size = data.get('size', size)
            self.memoStyle['svg'] = file
            self.memoStyle['fill'] = fill    
            self.memoStyle['size'] = size
            param = {}
            if fill:
                param['fill'] = fill

            if not file or file=='None':
                ico = QtGui.QIcon(os.path.join(self.imageFolder, "svg.png"))
                butt = QtWidgets.QPushButton(ico, "")
                file2 = None
            else:
                file2 = getSvgPath(file, code)
                ico = svgIco(file2, param)
                butt = QtWidgets.QPushButton(ico, "")
            hbox = QtWidgets.QHBoxLayout()
            self.eltStyle.add(hbox)
            butt.setMaximumWidth(50)
            hbox.addWidget(butt)
            self.eltStyleSvg.setWidget(butt)
            butt.clicked.connect(partial(self.openSVG, file2, param))  
            self.enabledModButton()
            if file2 and ico.isFillable():
                w = QgsColorButton()
                hbox.addWidget(w)
                self.eltStyleFill.setWidget(w)
                self.eltStyleFill.set(fill)
                w.colorChanged.connect(partial(self.reloadStyleSVG, w))
            
            w = QtWidgets.QDoubleSpinBox()
            self.eltStyleSize.setWidget(w)
            w.setRange(0.1, 10)
            try:
                w.setStepType(QAbstractSpinBox.AdpativeDecimalStepType)
            except:
                pass
            w.setValue(float(size))
            hbox.addWidget(w)
            w.valueChanged.connect(self.enabledModButton)
            w.valueChanged.connect(partial(self.changeStyleDetail, 'size', w))
            
            self.eltStyle.detail = {'image':True, 'svg':file, 'code':code, 'fill':fill, 'size':size}
        
        
        if do=='file':
            self.eltStyle.detail = {'file':style['file']}
            if 'vars' in style:
                self.eltStyle.detail['widgets'] = {}
                form = QtWidgets.QFormLayout()
                form.setContentsMargins(0,0,0,0)
                self.eltStyle.add(form)
                list = [""]
                try:
                    table = elt.get('tablename')
                except:
                    table = None
                if table:
                    list.extend(self.psql.getColumnsTable(table, elt.get('schemaname')))
                for k,c in style.get('vars').items():
                    l = QtWidgets.QLabel(c['label'])
                    if table:
                        combo = QtWidgets.QComboBox()
                    else:
                        combo = QtWidgets.QLineEdit()
                    wf = combo
                    w = adminWidget()
                    w.setWidget(combo)
                    w.refresh(list)
                    w.changed.connect(self.enabledModButton)
                    self.eltStyle.detail['widgets'][k] = w
                    v = js.get(k, {}).get('col')
                    if not v:
                        v = c.get('default')
                    w.set(v)
                    if 'coeff' in c:
                        wf = QtWidgets.QHBoxLayout()
                        wf.setSpacing(0)
                        wf.addWidget(combo)
                        v = js.get(k, {}).get('coeff')
                        if not v:
                            v = c.get('coeff', {}).get('default')
                        list2 = []
                        if 'unit' in c['coeff'] and c['coeff']['unit'] in self.translator['units']:
                            for kk,vv in self.translator['units'][c['coeff']['unit']].items():
                                list2.append((kk,vv))
                        if len(list2)>0:
                            label2 = None
                            combo2 = QtWidgets.QComboBox()
                            combo2.setMaximumWidth(50)
                        else:
                            l = QtWidgets.QLabel("*")
                            l.setMaximumWidth(6)
                            wf.addWidget(l)
                            combo2 = QtWidgets.QSpinBox()
                            combo2.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)
                            combo2.setMaximumWidth(44)
                            combo2.setRange(1, 1000000)
                        w2 = adminWidget()
                        w2.setWidget(combo2)
                        w2.refresh(list2)
                        w2.changed.connect(self.enabledModButton)
                        w.coeff = w2
                        w2.set(v, True)
                        wf.addWidget(combo2)    
                    form.addRow(l, wf)
                if not table:
                    l = QtWidgets.QLabel("La paramétrisation du style au niveau du groupe impose de retrouver les mêmes noms de champs dans toutes les couches associées")
                    l.setWordWrap(True)
                    font = QtGui.QFont()
                    font.setItalic(True)
                    l.setFont(font)
                    self.eltStyle.add(l)
            
        
    def changeStyleDetail(self, name, widget):
        self.eltStyle.detail[name] = widget.value()
     
    def makeStyle(self):
        style = self.eltStyle.get()
        if style:
            content = style.get('content')
            w = self.eltStyle.detail
            if w:
                if isinstance(w, QtWidgets.QComboBox):    
                    content = content.replace('_self_', f"{w.currentData()}")
                if isinstance(w, QgsColorButton):    
                    content = content.replace('_self_', f"{w.color().name()}")
                if isinstance(w, QtWidgets.QLineEdit):    
                    content = content.replace('_self_', f"{w.text()}")
                if isinstance(w, dict):    
                    if 'qml' in w:
                        content = w['value'].replace("'","''")
                    elif 'simple' in w:
                        content = content.replace('_self_', f"{w['fill']}") 
                        content = content.replace('_self2_', f"{w['size']}") 
                    elif 'image' in w:
                        content = content.replace('_self_', f"{w['svg']}")
                        content = content.replace('_self1_', f"{w['code']}")
                        content = content.replace('_self2_', f"{w['fill']}") 
                        content = content.replace('_self3_', f"{w['size']}") 
                    elif 'file' in w:
                        file = style['file']
                        txt = f'"{file}"'
                        if 'vars' in style:
                            for k,v in style.get('vars').items():
                                try:
                                    s = w['widgets'][k]
                                    col = s.get()
                                    try:
                                        coeff = s.coeff.get()
                                    except:
                                        coeff = None
                                except:
                                    col = c.get('default')
                                    coeff = c.get('coeff', {}).get('default')
                                if coeff:
                                    coeff = f', "coeff":{coeff}'
                                else:    
                                    coeff = ""
                                txt += f', "{k}":{{"col":"{col}"{coeff}}}'
                        content = content.replace('_self_', txt) 
                    else:
                        content = None
            if content:
                return f"'{content}'"
            else:
                return 'NULL'
        else:
            return 'NULL'          
        
    def jsStyle(self, style):
       
        try:
            jsStyle = json.loads(style)
        except:
            # print(sys.exc_info())
            jsStyle = {}
            
        if jsStyle==None:
            return {}    
            
        return jsStyle
        
    def saveModificationElement(self): 
        elt = self.element.get()
        fields = []
        fields.append(f"code='{self.eltCode.get()}'")
        fields.append(f"name='{self.eltName.get()}'")
        fields.append(f"tags='{self.eltTags.get()}'")
        fields.append(f"style={self.makeStyle()}")
        if self.eltStats!=None:
            fields.append(f"stats={self.eltStats.stat.setMemorize()}")
        fields.append(f"bloc='{self.eltBloc.getText().strip()}'")
        fields.append(f"cluster='{self.eltCluster.get()}'")
        order = self.eltOrder.get()
        if order==0:
            order = 'NULL'
        fields.append(f'"order"={order}')
        if self.eltParent.widget!=None:
            parent = self.eltParent.get()
            if not parent or parent=='NULL':
                parentNew = self.eltParent.getText().strip()
                if parentNew!="":
                    sql = f"INSERT INTO {self.fullAdmin} (code, name)" + \
                        f" VALUES ('{self.eltCode.get()}', '{parentNew}')" + \
                        f" RETURNING id"
                    print(sql)
                    parent = self.psql.sqlInsert(sql)
                else:
                    parent = 'null'
            fields.append(f"parent={parent}")
        sql = f"UPDATE {self.fullAdmin} SET " + ", ".join(fields) + \
            f" WHERE id={elt.get('id')}"
        # print(sql)
        self.psql.sqlSend(sql, True)
        self.initList()
        self.action.restore()
        self.relodAfterChanged()
        
    def saveModificationMultiple(self, target):
        field = ""
        if target=="style":
            field = f"style={self.makeStyle()}"
        if target=="bloc":
            field = f"bloc='{self.eltBloc.getText().strip()}'"
        if field!="":
            ids = ",".join(map(str, self.action.multipleListChoosen))
            sql = f"UPDATE {self.fullAdmin} SET {field}" + \
                f" WHERE id in ({ids})"
            self.psql.sqlSend(sql, True)
            self.initList()
            self.datasChanged.emit(self.action.multipleListChoosen)
        
    def openSVG(self, file, param):
        if not self.dialogSVG:
            dialog = svgDialog(file, param)
            dialog.accepted.connect(partial(self.reloadStyleSVG, dialog))
            dialog.finished.connect(self.closeSVG)
            self.dialogSVG = dialog
    
    def closeSVG(self):
        self.dialogSVG = None
    
    def reloadStyleSVG(self, dialog=None):
        if dialog!=None:
            if isinstance(dialog, svgDialog):    
                code, file = dialog.choice()
                self.memoStyle['code'] = code
                self.memoStyle['svg'] = file
                dialog.clean()
            if isinstance(dialog, QgsColorButton):    
                self.memoStyle['fill'] = dialog.color().name()
            if isinstance(dialog, QtWidgets.QDoubleSpinBox):    
                self.memoStyle['size'] = dialog.value()    
        self.autoStyle(self.memoStyle)
        
    def importSVG(self):
        dialog = QtGui.QFileDialog()
        fname = dialog.getOpenFileName(None, "Import SVG", "", "SVG files (*.svg)")
         
    def selLoadedLayer(self, combo):
        elt = self.element.get()
        if elt==None:
            return
        ids = []
        for p in [elt.get('id'), elt.get('parent'), self.eltParent.get()]:
            if p!=None and p!='NULL' and p!='null' and p not in ids:
                ids.append(p)
                for it in self.getChildren(p):
                    ids.append(it['elt']['id'])
        layers = self.dock.dyn.getAll()
        layer = None
        for id in ids:
            if layer!=None:
                break
            for k,v in layers.items():
                if layer!=None:
                    break
                if id==k:
                    layer = v
        combo.setLayer(layer)     
  
    def getStyleFromLayer(self):
        layer = self.eltStyle.wQgis.combo.currentLayer()
        if layer!=None:
            xml = QtXml.QDomDocument()
            layer.exportNamedStyle(xml)
            txt = xml.toString()
        else:
            txt = ""
        self.setStyleFromText(txt)
        
    def getFileFromQml(self, file):
        self.eltStyle.wOpen.file = file[0]
        self.eltStyle.wOpen.label.setText(file[0])
        
    def getStyleFromQml(self):
        f = QFile(self.eltStyle.wOpen.file)
        content = ""
        if f.open(QIODevice.ReadOnly | QIODevice.Text):
            buff = QTextStream(f)
            while not buff.atEnd():
                line = buff.readLine()
                content += line
            f.close()
        self.setStyleFromText(content)
        
    def setStyleFromText(self, txt):
        self.eltStyle.detail['value'] = txt
        self.enabledModButton()
        self.qmlChooseAction()
        
    def qmlChooseAction(self, code=None):
        self.eltStyle.wQgis.hide()
        self.eltStyle.wOpen.hide()
        txt = self.eltStyle.detail['value']
        if txt!="":
            self.eltStyle.bView.setEnabled(True)
        else:
            self.eltStyle.bView.setEnabled(False)
        if code=='qml':
            dialog = showDialog("Contenu du QML", txt)
        if code=='qgis':
            self.eltStyle.wQgis.show()
        if code=='open':
            self.eltStyle.wOpen.butt.setEnabled(False)
            self.eltStyle.wOpen.show()
            dialog = QtWidgets.QFileDialog()
            fileName = dialog.getOpenFileName(None, "Open QML", "", "QML Files (*.qml)")
            file = fileName[0]
            self.eltStyle.wOpen.file = file
            self.eltStyle.wOpen.label.setText(os.path.basename(file))
            QtWidgets.QApplication.processEvents()
            self.eltStyle.wOpen.butt.setEnabled(True)

        

    def initData(self):
        self.svgFolder = os.path.join(os.path.dirname(__file__),'svg')
        self.translator = {
            'style': [
                {
                    'label':'Auto',
                    'do': None,
                },
                {
                    'label':'Simple',
                    'do': 'simple',
                    'content': '{"fill":"_self_", "size":"_self2_"}',
                },
                {
                    'label':'Image',
                    'target': 'point',
                    'do': 'svg',
                    'content': '{"marker":{"svg":"_self_", "code":"_self1_", "fill":"_self2_", "size":"_self3_"}}',
                },
                {
                    'type': 'file',
                    'content': '{"file":_self_}',
                },
                {
                    'label':'Personnalisé',
                    'do': 'qml',
                    'content': '<!doctype_self_',
                },
                {
                    'label':'Texte brut',
                    'do': 'edit',
                    'content': '_self_',
                },
            ],
            
            'action': {
                'mod': "  Modifier",
                'add': "  Ajouter dans",
                'list': ['mod', 'add'],

            },
            
            'units': {
                'meter': {
                    'm': 1,
                    'mm': 1000,
                }
            },
            
        }




        

class adminWidget(QObject):
    
    changed = pyqtSignal()
    
    def __init__(self, default=None, callback=None, *args, **kwargs):
        QObject.__init__(self)
        self.widget =None
        self.layout = None
        self.memo = default
        self.setCallback(callback, *args, **kwargs)
        self.freeze = False
    
    def setLayout(self, layout):
        self.layout = layout
        
    def setCallback(self, callback=None, *args, **kwargs):
        self.callback = callback
        self.args = args
        self.kwargs = kwargs
        
    def setWidget(self, widget=None):
        self.widget = widget
        if widget!=None:
            if isinstance(self.widget, QgsMapLayerComboBox):
                self.widget.layerChanged.connect(self.onChanged)
            if isinstance(self.widget, toggleButton):
                self.widget.changed.connect(self.onChanged)
            if isinstance(self.widget, QtWidgets.QComboBox):
                self.widget.currentIndexChanged.connect(self.onChanged)
            if isinstance(self.widget, QtWidgets.QCheckBox):
                self.widget.stateChanged.connect(self.onChanged)
            if isinstance(self.widget, QtWidgets.QLineEdit):
                self.widget.textChanged.connect(self.onChanged)
            if isinstance(self.widget, QgsColorButton):
                self.widget.colorChanged.connect(self.onChanged)
            if isinstance(self.widget, QtWidgets.QSpinBox):
                self.widget.valueChanged.connect(self.onChanged)   
        
    def refresh(self, l):
        if isinstance(self.widget, QtWidgets.QComboBox):
            if isinstance(l, list):
                self.clear()
                self.freeze = True
                while self.widget.count():
                    self.widget.removeItem(0)
                for e in l:
                    if isinstance(e, tuple):
                        self.widget.addItem(*e)
                    else:
                        self.widget.addItem(e, e)
                self.freeze = False
        
    def onChanged(self):
        if self.freeze:
            return
        self.save()
        self.clear()
        if self.callback:
            self.callback(*self.args, **self.kwargs)
        self.changed.emit()
    
    def get(self):
        try:
            if isinstance(self.widget, QgsMapLayerComboBox):
                return self.widget.currentLayer()
            if isinstance(self.widget, QtWidgets.QComboBox):
                return self.widget.currentData()
            if isinstance(self.widget, QtWidgets.QCheckBox):
                return self.widget.isChecked()
            if isinstance(self.widget, QtWidgets.QLineEdit):
                return self.widget.text()
            if isinstance(self.widget, toggleButton):
                return self.widget.text()
            if isinstance(self.widget, QgsColorButton):
                return self.widget.color().name()
            if isinstance(self.widget, QtWidgets.QSpinBox):
                return self.widget.value()
        except:
            pass
    
    def getText(self):
        try:
            if isinstance(self.widget, QgsMapLayerComboBox):
                return self.widget.currentLayer().name()
            if isinstance(self.widget, QtWidgets.QComboBox):
                return self.widget.currentText()
            if isinstance(self.widget, QtWidgets.QCheckBox):
                return self.widget.isChecked()
            if isinstance(self.widget, QtWidgets.QLineEdit):
                return self.widget.text()
            if isinstance(self.widget, toggleButton):
                return self.widget.text()   
            if isinstance(self.widget, QgsColorButton):
                return self.widget.color().name()
            if isinstance(self.widget, QtWidgets.QSpinBox):
                return self.widget.value()
        except:
            pass
                
    def set(self, val, data=None):
        try:
            if isinstance(self.widget, QgsMapLayerComboBox):
                self.widget.setLayer(val)
            elif isinstance(self.widget, QtWidgets.QComboBox):
                if data or val==None:
                    for i in range(self.widget.count()):
                        if self.widget.itemData(i)==val:
                            self.widget.setCurrentIndex(i)
                            break
                elif isinstance(val, dict):
                    for i in range(self.widget.count()):
                        data = self.widget.itemData(i)
                        if data and isinstance(data, dict):
                            if val.get('id')==data.get('id'):
                                self.widget.setCurrentIndex(i)
                                break
                elif isinstance(val, int):
                    self.widget.setCurrentIndex(val)
                else:
                    self.widget.setCurrentText(val)
            if isinstance(self.widget, QtWidgets.QCheckBox):
                self.widget.setChecked(val)
            if isinstance(self.widget, QtWidgets.QLineEdit):
                if val:
                    self.widget.setText(f"{val}")
                else:
                    self.widget.setText(f"")
            if isinstance(self.widget, toggleButton):
                self.widget.setFromText(val)
            if isinstance(self.widget, QgsColorButton):
                color = QtGui.QColor(val)
                self.widget.setColor(color)
            if isinstance(self.widget, QtWidgets.QSpinBox):
                if val:
                    self.widget.setValue(val)
        except:
            # print(sys.exc_info())
            pass
        self.save()
            
    def save(self):
        self.memo = self.get()
            
    def restore(self):
        if self.get()!=self.memo:
            self.set(self.memo)
        else:
            self.onChanged()
    
    def clear(self):
        if self.layout!=None:
            clearLayout(self.layout)
            
    def add(self, object):
        if self.layout!=None:
            if isinstance(object, QtWidgets.QWidget):
                self.layout.addWidget(object)
            if isinstance(object, QtWidgets.QLayout):
                self.layout.addLayout(object)
    
        


class jsStyle():
    def __init__(self):
        self.w = 1



class svgDialog(QgsDialog):
   
    def __init__(self, file=None, param={}):

        buttons = QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel
        QgsDialog.__init__(self, iface.mainWindow(),
                            fl=Qt.WindowFlags(),
                            buttons=buttons,
                            orientation=Qt.Horizontal)
        
        self.svgFolder = os.path.join(os.path.dirname(__file__), 'svg')
        plugFolder = os.path.join(os.path.dirname(__file__), 'svg')
        qgisFolder = os.path.join(os.environ.get('QGIS_PREFIX_PATH'), '.', 'svg')
        self.maxRow = 8
        self.target = None
        self.list = []
        self.file = file
        self.fileList = {}
        self.manager = manageTasks()
        self.setWindowTitle("Sélection de l'icone SVG") 
        self.makeIcoBloc('plug', plugFolder)
        self.makeIcoBloc('qgis', qgisFolder, 2)
        self.makeLabel()
        self.show()

    def makeIcoBloc(self, code, p, deep=0):
        titles = {'plug':"SVG du plugin", 'qgis':"SVG Qgis"}
        title = titles[code]
        w = QtWidgets.QLabel(title, self)
        w.setMinimumWidth((self.maxRow+1)*60)
        self.layout().addWidget(w)
        
        sc = QtWidgets.QScrollArea(self)
        sc.setWidgetResizable(True)
        self.layout().addWidget(sc)
        w = QtWidgets.QWidget()
        w.setMinimumWidth(self.maxRow*60)
        gbox = QtWidgets.QGridLayout()
        gbox.setSpacing(0)
        gbox.setAlignment(Qt.AlignTop)
        w.setLayout(gbox)
        sc.setWidget(w)
        
        gbox.code = code
        gbox.i = 1
        gbox.j = 0
        self.fileList[p] = {'layout':gbox, list:[], 'sc':sc}
        if deep>0:
            l = QtWidgets.QLabel("Recherche en cours...")
            gbox.addWidget(l)
            timer = rdiTimer(self)
            timer.setLabel(l, deb=l.text())
            timer.setMaxPts(10)
            timer.start(200)
            gbox.timer = timer
            self.asyncBrowse(p, deep)
        else:
            self.browse(p)
            self.loadIcoInBloc(p)
                
    def loadIcoInBloc(self, p):
        gbox = self.fileList[p]['layout']
        try:
            gbox.timer.stop()
        except:
            pass
        clearLayout(gbox)
        list = self.fileList[p]['list']
        for file in list:
            gbox.j += 1
            if gbox.j>self.maxRow:
                gbox.j = 1
                gbox.i += 1
            l = svgChoice(file)
            l.code = gbox.code
            self.list.append(l)
            l.clicked.connect(partial(self.selectIcoSVG, l))
            gbox.addWidget(l, gbox.i, gbox.j)
            if self.file==file:
                self.target = l
        if gbox.i==1:
            for k in range(self.maxRow-gbox.j):
                gbox.j += 1
                l = svgChoice()
                gbox.addWidget(l, gbox.i, gbox.j)
        if gbox.i>1:
            nb = min(gbox.i, 5)
            self.fileList[p]['sc'].setMinimumHeight(nb*40)
    
    def asyncBrowse(self, p, deep):
        m = mGlobalTask(f"Récupération svg dans {p}")
        m.task.setCallin(self.browse, p, deep)
        m.setCallback(self.loadIcoInBloc, p)
        self.manager.add(m)
    
    def browse(self, p, deep=0):
        list = []
        for f in os.listdir(p):
            file = os.path.join(p, f)
            name, ext = os.path.splitext(f)
            if ext.lower()=='.svg':
                l = svgChoice(file)
                list.append(file)
            else:
                if deep>0 and os.path.isdir(file):
                    list.extend(self.browse(file, deep-1))
        if p in self.fileList:
            self.fileList[p]['list'] = list
        return list
    
    def makeLabel(self):
        self.choiceLabel = QtWidgets.QLabel("", self)
        self.layout().addWidget(self.choiceLabel)
        if self.target:
            self.selectIcoSVG(self.target)
  
    def selectIcoSVG(self, target):
        if self.target:
            self.target.setSelected(False)
        self.target = target   
        self.target.setSelected(True)   
        self.choiceLabel.setText(os.path.basename(self.target.file))
        
    def choice(self):
        if self.target:
            path = self.target.file.replace("\\","/")
            tt = path.split('svg/')
            if len(tt)>1:
                file = tt[1]
            else:
                file = os.path.basename(path)
            return self.target.code, file

    def clean(self):
        for l in self.list:
            if l!=self.target:
                l.ico.clean()


    
        
class svgChoice(QtWidgets.QLabel):

    clicked = pyqtSignal()
    
    def __init__(self, file=None, param={}):
        QtWidgets.QLabel.__init__(self)
        self.svgFolder = os.path.join(os.path.dirname(__file__), 'svg')
        self.tmpFolder = 'tmp'
        self.makeTmp()
        self.file = file
        if file:
            self.ico = svgIco(file, param)
            pixmap = self.ico.pixmap(40,40)
            self.setPixmap(pixmap)
            self.setToolTip(file)

    def mousePressEvent(self, event):
        self.clicked.emit()
        
    def setSelected(self, bool):
        if bool:
            self.setStyleSheet("border: 2px solid black;")
            pixmap = self.ico.pixmap(60,60)
            self.setPixmap(pixmap)
        else:
            self.setStyleSheet("border: None;")
            pixmap = self.ico.pixmap(40,40)
            self.setPixmap(pixmap)
        
    def makeTmp(self):
        tmp = os.path.join(self.svgFolder, self.tmpFolder)
        if not os.path.isdir(tmp):
            os.mkdir(tmp)


class svgIco(QtGui.QIcon):
    
    def __init__(self, file, param={}):
        super(svgIco, self).__init__(file)
        self.file = file
        self.initData()
        self.changeIco(param)
    
    def initData(self):
        self.defaultParam = {
                'param(fill)': '#000000',
                'param(fill-opacity)': '1',
                'param(outline)': '#000000',
                'param(outline-opacity)': '1',
                'param(outline-width)': '1',
            }
        self.svgFolder = os.path.join(os.path.dirname(__file__), 'svg')
        self.tmpFolder = 'tmp'
        self.makeTmp()
        self.fill = False
        self.svg = None
        self.tmp = None
        
    def changeIco(self, param={}):
        self.param = param
        bool = False
        if param:
            bool = True
        if self.file and self.file!='None':
            self.deleteFile(self.tmp)
            self.tmp = self.copySVG(self.file, bool)
            if self.tmp:
                self.addFile(self.tmp)
    
    def makeTmp(self):
        tmp = os.path.join(self.svgFolder, self.tmpFolder)
        if not os.path.isdir(tmp):
            os.mkdir(tmp)
    
    def clean(self):
        # self.deleteFile(self.tmp)
        pass
    
    def copySVG(self, file, force=False):
        d, n = os.path.split(file)
        if not self.param:
            n = "default_" + n
        tmp = makeUniqueIdent()
        file2 = os.path.join(self.svgFolder, self.tmpFolder, n)
        if not os.path.exists(file2) or force:
            self.svg = self.readFile(file)
            if not self.svg:
                return None
            svg = self.replaceParam(self.svg)
            self.writeFile(file2, svg)
        return file2
        
    def replaceParam(self, svg):
        if svg:
            svg2 = svg
            for k,v in self.getParams().items():
                while svg2.find(k)>=0:
                    e = svg2[svg2.find(k):]
                    e = e.split('"')[0]
                    svg2 = svg2.replace(e, v)
            return svg2
     
    def isFillable(self):
        if not self.svg:
            self.svg = self.readFile(self.file)
        if self.svg!=None and self.svg.find('param(fill)')>=0:
            return True
        
    def getParams(self):
        param = self.defaultParam
        if 'fill' in self.param:
            param['param(fill)'] = self.param['fill']
            param['param(outline)'] = self.param['fill']
        return param
            
    def readFile(self, file):
        f = QFile(file)
        if not f.open(QIODevice.ReadOnly | QIODevice.Text):
            return
        content = ""
        buff = QTextStream(f)
        while not buff.atEnd():
            line = buff.readLine()
            content += line
        f.close()
        return content
        
    def writeFile(self, file, content):
        f = QFile(file)
        if not f.open(QIODevice.WriteOnly | QIODevice.Text):
            return
        out = QTextStream(f)
        out << content   
        f.close()

    def deleteFile(self, file):
        if file and os.path.exists(file):
            os.remove(file)
            
            
            
            
            