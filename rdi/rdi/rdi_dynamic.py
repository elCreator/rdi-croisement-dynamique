# -*- coding: utf-8 -*-

import os, logging, sys, json
from functools import partial

from qgis.core import *
from qgis.utils import iface
from PyQt5 import QtGui, QtWidgets, uic, QtXml
from PyQt5.QtCore import *

from .rdi_param import rdiParam
from .rdi_tools import quote, getSvgPath


class rdiDynamic():

    def __init__(self):
        self.actuParams()
        self.rdiGroup = self.param.get('root')
        self.map = QgsProject.instance()
        self.root = self.map.layerTreeRoot()
        self.map.layersWillBeRemoved.connect(self.removeChecked)
        self.groups = list(map(self.param.get, ['enjeu', 'enjeuLight', 'alea', 'buff', 'stats']))

    def actuParams(self):
        self.param = rdiParam()

    def addLayer(self, rdi):
        title = rdi.coll.title()
        if title and title!="":
            group = self.makeGroup(title, self.rdiGroup)
        else:
            group = self.makeGroup(self.rdiGroup)
        return self.finaliseLayer(rdi, group)    

    def removeMapLayer(self, rdi):
        try:
            QgsProject.instance().removeMapLayer(rdi.layerId)
            iface.mapCanvas().refresh()
        except:
            # print('error remove')
            # print(sys.exc_info())
            pass
    
    def removeLayer(self, rdi, keepTampon=False):
        if not keepTampon:
            self.removeTampon(rdi)   
        self.removeLight(rdi)
        self.removeMapLayer(rdi)

    def majLayer(self, rdi):
        pos = self.getLayerPosition(rdi)
        group = rdi.layer.group
        self.removeLayer(rdi, True)
        return self.finaliseLayer(rdi, group, pos)    

    def finaliseLayer(self, rdi, group, pos=-1):
        layer = None
        try:
            self.map.addMapLayer(rdi.vlayer, False)
            if pos>=0:
                layer = group.insertLayer(pos,rdi.vlayer)
            else:
                layer = group.addLayer(rdi.vlayer)
            layer.group = group
        except:
            pass
        return layer
    
    def removeChecked(self, list):
        for id in list:
            layer = self.map.mapLayer(id)
            try:
                rdi = layer.rdi
                if not rdi.freeze and rdi.cb.isChecked():
                    rdi.cb.setChecked(False)
            except:
                # print(sys.exc_info())
                pass
    


    def addTampon(self, rdi):
        title = self.param.get('buff')
        group = self.makeGroup(title, self.rdiGroup)
        group.setExpanded(False)
        self.removeTampon(rdi)    
        layer = None
        try:
            self.map.addMapLayer(rdi.tampon.vlayer, False)
            layer = group.addLayer(rdi.tampon.vlayer)
            layer.group = group
        except:
            print(sys.exc_info())
            pass
        return layer  

    def removeTampon(self, rdi):
        self.removeMapLayer(rdi.tampon)
        
        
    def addLight(self, rdi):
        title = self.param.get('enjeuLight')
        group = self.makeGroup(title, self.rdiGroup)
        group.setExpanded(False)
        self.removeLight(rdi)    
        layer = None
        try:
            self.map.addMapLayer(rdi.light.vlayer, False)
            layer = group.addLayer(rdi.light.vlayer)
            layer.group = group
        except:
            print(sys.exc_info())
            pass
        return layer  

    def removeLight(self, rdi):
        self.removeMapLayer(rdi.light)
    
    def getLayerPosition(self, rdi):
        pos = -1
        try:
            group = rdi.layer.group
            for l in group.findLayers():
                pos = pos + 1
                if l==layer:
                    return pos
        except:
            pass
        return pos

    
    def getLayerByName(self, layerName, code=None):
        layer = None
        if not code:
            code = self.rdiGroup
        group = self.root.findGroup(code)
        for l in self.map.mapLayersByName(layerName):
            if l.type()==QgsMapLayer.VectorLayer and l.dataProvider().name()=="postgres":
                layer = l
        return layer
    
    def makeGroup(self, code, parent=None):
        if parent:
            p = self.makeGroup(parent)
        else:
            p = self.root
        group = self.root.findGroup(code)
        if not group:
            if code in self.groups:
                ind = self.groups.index(code)
            else:
                ind = 0
            group = p.insertGroup(ind, code)
        return group
        
        
    def makeStyle(self, rdi):
        if not rdi.style:
            rdi.style = rdiStyle(rdi)
        else:
            rdi.style.recup()
            
            
    def makeTampon(self, rdi):
        if not rdi.tampon.style:
            rdi.tampon.style = rdiStyle(rdi.tampon)
        else:
            rdi.tampon.style.recup() 
            
    def makeLight(self, rdi):
        if not rdi.light.style:
            rdi.light.style = rdiStyle(rdi.light)
        else:
            rdi.light.style.recup() 
    
    def getAll(self):
        dict = {}
        for layer in self.map.mapLayers().values():
            try:
                id = layer.rdiId
                dict[id] = layer
            except:
                pass
        return dict
    
    def closeAll(self):
        root = QgsProject.instance().layerTreeRoot()
        try:
            group = root.findGroup(self.rdiGroup)
            group.removeAllChildren()
            root.removeChildNode(group)
        except:
            pass






class rdiStyle():
    def __init__(self, rdi):
        self.rdi = rdi
        rdi.style = self
        self.param = rdiParam()
        self.actuStyle()
        self.cluster = False
        self.filterList = None
        self.isFilterable = False
        self.memoType = None
        self.getCatalog()
        self.getLayer()
        self.run()
    
    def actuStyle(self):
        self.json = self.rdi.jsStyle()
     
    def getLayer(self):
        try:
            self.layer = self.rdi.vlayer
            # self.layer = self.rdi.layer.layer()
        except:
            self.layer = None
            
    def unpack(self):
        try:
            if self.layer:
                self.renderer = self.layer.renderer()
                self.symbol = None
                self.trySymbolFromSimple()
                self.trySymbolFromCluster()
                if self.symbol:
                    self.type = self.symbol.type()
                return True
        except:
            print(sys.exc_info())
            pass
                
    def recup(self):
        self.getLayer()
        if not self.loadXML():
            self.run()

    def run(self):
        if self.unpack():
            self.makeType()
            self.layer.triggerRepaint()
            iface.layerTreeView().refreshLayerSymbology(self.layer.id())
    
    def trySymbolFromSimple(self):
        try:
            symbol = self.renderer.symbol()
            self.symbol = symbol
            self.clustered = False
        except:
            pass
            
    def trySymbolFromCluster(self):
        try:
            symbol = self.renderer.clusterSymbol()
            self.symbol = symbol
            self.clustered = True
        except:
            pass 


    
    def saveStyle(self):
        if self.unpack():
            self.saveXML()
    
    def reset(self):
        try:
            del self.rendererXML
        except:
            pass
    
    def getXML(self):
        try:
            print('ok')
            xml = QtXml.QDomDocument()
            self.layer.exportNamedStyle(xml)
            return xml  
        except:
            print(sys.exc_info())
            pass 
    
    def saveXML(self):
        try:
            xml = QtXml.QDomDocument()
            self.layer.exportNamedStyle(xml)
            self.rendererXML = xml.toString()   
        except:
            pass 
            
    def loadXML(self, txt=None):
        try:
            if not txt:
                txt = self.rendererXML
            xml = QtXml.QDomDocument()
            xml.setContent(txt)
            tag = self.param.get('rendererTag')
            nodes = xml.elementsByTagName(tag)
            if nodes.count()>1 and not self.isClusterable():
                rcluster = nodes.at(0)
                rembed = nodes.at(1)
                parent = rcluster.parentNode()
                parent.replaceChild(rembed, rcluster)
            self.layer.importNamedStyle(xml)
            if self.isClusterable() and nodes.count()<2:
                if self.unpack():
                    sls = self.symbol.symbolLayers()
                    if len(sls)==1:
                        self.makeStyleCluster(sls[0])
            
            return True
        except:
            # print(sys.exc_info())
            pass
        
    def getFill(self):        
        try:
            fill = self.fill
            return fill
        except:
            pass  


    # CREATION STYLE
    def makeType(self):
        try:
            if 'xml' in self.json:
                self.loadXML(self.rdi.styleContent())
            elif 'file' in self.json:
                self.makeStyleFiles()
            else:
                if self.type==0:
                    self.makeStylePoint()
                if self.type==1:
                    self.makeStyleLine()
                if self.type==2:
                    self.makeStylePolygon()
            if self.type==2:
                op = self.param.get("aleaOpacity") or 0.6
                self.layer.setOpacity(op)
            self.memoType = self.type
            if 'opacity' in self.rdi.params:
                self.layer.setOpacity(self.rdi.params['opacity'])
        except ValueError as err:
            print(f"type failure for {self.rdi.layerName()}:{self.rdi.tableName()}")
            print(sys.exc_info())
            print(err)
    
  
    def isMulti(self):
        bool = False
        if len(self.rdi.geomType)==0:
            bool = True
        for t in self.rdi.geomType:
            if t.lower().find('multi')>=0:
                bool = True
            if t.lower().find('geometry')>=0:
                bool = True
        return bool
        
    def isPoint(self):
        bool = True
        if len(self.rdi.geomType)==0:
            bool = False
        for t in self.rdi.geomType:
            bool = bool and (t.lower()=='point')
        return bool
        
    def isFontSymbol(self, sl):
        return (sl.layerType().lower().find('font')>=0)
     
        
    def isClusterable(self):
        bool = self.rdi.cluster
        bool = bool and self.isPoint()
        bool = bool and not self.isMulti()
        return bool
        







    # FROM FILES
    def addFilterCond(self):
        cond = ""
        if (self.filter!=""):
            cond = " and "
        return cond
        
    def addFilterElt(self, dict, field):
        if field['col']=="":
            return
        coeff = 1
        try:
            c = int(field['coeff'])
            if c>0:
                coeff = c
        except:
            coeff = 1
        if 'val' in dict:
            self.filter += f"{self.addFilterCond()}{quote(field['col'])}={dict['val']*coeff}"
        if 'min' in dict:
            self.filter += f"{self.addFilterCond()}{quote(field['col'])}>={dict['min']*coeff}"
        if 'max' in dict:
            self.filter += f"{self.addFilterCond()}{quote(field['col'])}<{dict['max']*coeff}"

    def getSymbolDefault(self, elt={}):
        p = {'color':self.catalog['default']['color']}
        for k,v in elt.items():
            if isinstance(v, str):
                p[k] = v
        if self.type!=1 and (not 'outline_width' in elt or float(elt['outline_width'])<=0):
            p['outline_style'] = 'no'
        if self.type==0:
            p['name'] = 'circle'
            return QgsMarkerSymbol.createSimple(p)
        if self.type==1:
            return QgsLineSymbol.createSimple(p)
        if self.type==2:
            return QgsFillSymbol().createSimple(p)
    
    def makeStyleFiles(self):
        file = self.json['file']
        try:
            f = open(os.path.join(os.path.dirname(__file__), 'styles', file))
            self.legend = json.loads(f.read())
            f.close()
            name = self.legend.get('name') or file.split('.')[0]
            if 'list' in self.legend and 'vars' in self.legend:
                self.isFilterable = True
                if self.rdi.coll and self.rdi.coll.matchCode('alea'):
                    item = self.rdi.coll.dock.legend.add(name, self.legend, self.rdi)
                else:
                    item = None
                self.filterList = []
                symbol = self.getSymbolDefault()
                self.renderer = QgsRuleBasedRenderer(symbol)
                rules = self.renderer.rootRule()
                rules.removeChild(rules.children()[0])
                for i,elt in enumerate(self.legend['list']):  
                    symbol = self.getSymbolDefault(elt)
                    self.filter = ""
                    for k,v in self.legend['vars'].items():
                        if k in elt and k in self.json:
                            self.addFilterElt(elt[k], self.json[k])
                    if self.filter!="":
                        rule = self.renderer.Rule(symbol=symbol, filterExp=self.filter, label=elt['label'])
                        if not item or self.rdi.coll.dock.filterMode!="Légende" or item.elts[i].isChecked():   
                            rules.appendChild(rule)
                    self.filterList.append(self.filter)
                self.layer.setRenderer(self.renderer) 
                # self.rdi.setContextFromLegend(item)
            else:
                symbol = self.getSymbolDefault(self.legend)
                self.renderer.setSymbol(symbol)
        except:
            print(sys.exc_info())
            pass





        
        
    # POLYGON
    def makeStylePolygon(self):
        # self.symbol.setColor(QtGui.QColor(self.catalog['default']['color']))
        if self.json=={}:
            if self.rdi.isStat:
                self.styleFromParam('stat')
                return
                
        fill = self.getFill()
        if fill:
            self.symbol.setColor(QtGui.QColor(fill))
        self.fill = self.symbol.color().name()
        
        if 'fill' in self.json:
            self.symbol.setColor(QtGui.QColor(self.json['fill']))
            self.fill = self.symbol.color().name()    
        if 'tampon' in self.json:
            self.styleFromParam('buffer')

    
    def styleFromParam(self, code):
        sl = self.symbol.symbolLayer(0)
        fill = self.param.get(f"{code}Fill") or '#20000000'
        sl.setFillColor(QtGui.QColor(fill))
        fill = self.param.get(f"{code}Stroke") or '#000000'
        sl.setStrokeColor(QtGui.QColor(fill))
        style = Qt.DotLine
        pen = self.param.get(f"{code}Pen") or 'dot'
        if pen in self.translator['style']['pen']:
            style =self.translator['style']['pen'][pen]
        sl.setStrokeStyle(style)
        coeff = float(self.param.get(f"{code}Width")) or 1
        sl.setStrokeWidth(sl.strokeWidth()*coeff)
        
        
    

    
            
    
    # POINT
    def makeStylePoint(self):
        self.cluster = False
        if self.isClusterable():
            self.makeStyleCluster()
        else:
            self.makeStyleSimple()
        
    def makeStyleCluster(self, symbolLayer=None):
        c = 1.7
        renderer = QgsPointClusterRenderer() 
        ns = renderer.clusterSymbol()  
        try:
            try:
                qgsUnit =self.translator['style']['unit'][self.param.get('rendererUnit')]
            except:
                qgsUnit = QgsUnitTypes.RenderMillimeters
            renderer.setToleranceUnit(qgsUnit)
            renderer.setTolerance(self.param.get('rendererTolerance'))
            if 'size' in self.json:
                size = float(self.json['size'])
            else:
                size = 1
            s = renderer.embeddedRenderer().symbol()
            if not symbolLayer:
                symbolLayer, fill = self.getSymbology() 
            if symbolLayer:
                s.changeSymbolLayer(0, symbolLayer.clone())
                nsl = symbolLayer.clone()
            elif fill:
                s.setColor(QtGui.QColor(fill))
                s.setSize(s.size()*size)
            
            toChange = -1
            for i, sl in enumerate(ns.symbolLayers()):
                if self.isFontSymbol(sl):
                    # sl.setFontFamily("Verdana Pro Black")
                    sl.setFontFamily("Arial Black")
                    sl.setColor(QtGui.QColor("#000000"))
                    sl.setStrokeColor(QtGui.QColor("#ffffff"))
                    sl.setStrokeWidth(0.25)
                    sl.setSize(sl.size()*c*0.8)
                    if self.rdi.field.get()!="":
                        precision = self.param.get('clusterPrecision')
                        expr = f"@cluster_size/{precision}"
                        if self.param.get('clusterRound'):
                            expr = f"round({expr})"
                        sl.setDataDefinedProperty(QgsSymbolLayer.PropertyCharacter, QgsProperty.fromExpression(expr))
                else:
                    if symbolLayer:
                        nsl.setSize(nsl.size()*c)
                        toChange = i
                    else:
                        if fill:
                            sl.setColor(QtGui.QColor(fill))
                        sl.setSize(sl.size()*size*c)
                    self.fill = sl.color().name()
            if toChange>-1:
                ns.changeSymbolLayer(toChange, nsl)
        except:
            print(sys.exc_info())
        self.layer.setRenderer(renderer)
        self.cluster = True
    
    def makeStyleSimple(self):
        symbol = None
        symbolLayer, fill = self.getSymbology() 

        # print(self.json)
        # if 'fill' in self.json:    
            # fill = self.json['fill']
        if 'size' in self.json:
            size = float(self.json['size'])
        else:
            size = 1
        if fill:
            symbol = QgsMarkerSymbol.createSimple({'name':'circle', 'color':fill})
            symbol.setSize(symbol.size()*size)
        if symbolLayer:
            symbol = QgsMarkerSymbol.createSimple({'name':'circle', 'color':self.catalog['default']['color']})
            symbol.changeSymbolLayer(0, symbolLayer)
        if symbol:
            self.renderer.setSymbol(symbol)
            self.fill = symbol.color().name()

    def getSymbology(self):
        symbolLayer = None
        fill = self.getFill()
        if not fill:
            fill = self.symbol.color().name()
        if 'marker' in self.json:
            marker = self.json['marker']
            if 'svg' in marker:
                code = self.json.get('code') or None
                path = getSvgPath(marker['svg'], code)
                symbolLayer = QgsSvgMarkerSymbolLayer(path)
            if 'fill' in marker:    
                fill = marker['fill']
                if symbolLayer:
                    symbolLayer.setColor(QtGui.QColor(fill))
                    symbolLayer.setStrokeColor(QtGui.QColor(fill))
            if 'size' in marker:
                size = float(marker['size'])
                if symbolLayer:
                    symbolLayer.setSize(symbolLayer.size()*size)
        else:
            if 'fill' in self.json:    
                fill = self.json['fill']
            size = 1
            if 'size' in self.json:
                size = float(self.json['size'])
            symbol = QgsMarkerSymbol.createSimple({'name':'circle', 'color':fill})
            symbol.setSize(symbol.size()*size)
            # symbolLayer = symbol.symbolLayer(0) 
        return symbolLayer, fill





    # LINESTRING
    def makeStyleLine(self):
        if self.symbol!=None:
            fill = self.getFill()
            if fill:
                self.symbol.setColor(QtGui.QColor(fill))
            
            if 'fill' in self.json:
                self.symbol.setColor(QtGui.QColor(self.json['fill']))
            if 'size' in self.json:
                size = float(self.json['size'])
                width = self.param.get('strokeWidth') or 1
                qgsUnit = QgsUnitTypes.RenderPixels
                unit = self.param.get('strokeUnit') or 'mm'
                if unit in self.translator['style']['unit']:
                    qgsUnit =self.translator['style']['unit'][unit]
                try:
                    self.symbol.setWidthUnit(qgsUnit)
                except:
                    pass
                self.symbol.setWidth(width*size)
            self.fill = self.symbol.color().name()

    


 
 
    # LIBRARY
    def getCatalog(self):
        self.svgFolder = os.path.join(os.path.dirname(__file__),'svg')
        self.root = os.path.dirname(__file__).replace("\\","/")
        self.catalog = {
                'default': {
                    'color':'#5555AA'
                },
            }
        
        self.translator = {
                'style': {
                    'pen': {
                        'no': Qt.NoPen,
                        'dot': Qt.DotLine,
                        'solid': Qt.SolidLine,
                        'dash': Qt.DashLine,
                    },
                    'unit': {
                        'mm': QgsUnitTypes.RenderMillimeters,
                        'pixel': QgsUnitTypes.RenderPixels,
                        'map': QgsUnitTypes.RenderMapUnits,
                    }
                },
        
            }
            
        

        












