# -*- coding: utf-8 -*-

import os, sys, configparser, shutil, zipfile
from functools import partial

from qgis.gui import *
from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic, QtNetwork, QtXml, QtDesigner
from PyQt5.QtCore import *

from .rdi_tools import flashMsg
from .rdi_meta import rdiMeta


class rdiMaj(QObject):

    def __init__(self):
        QObject.__init__(self)
        self.meta = rdiMeta()
        self.name = self.meta.getName()
        self.version = ""
        self.dispo = False
        
    def test(self):
        url = 'http://piece-jointe-carto.developpement-durable.gouv.fr/DEPT067A/QGIS/plugins/plugins.xml'
        self.doRequest(url) 
        self.meta.writeIni("name", self.name)
    
    def aff(self, txt):
        xml = QtXml.QDomDocument()
        xml.setContent(txt)
        tag = "pyqgis_plugin"
        nodes = xml.elementsByTagName(tag)
        for n in range(nodes.count()):
            node = nodes.at(n)
            name = node.toElement().attribute("name")
            if name==self.name:
                self.version = node.toElement().attribute("version")
                if self.compare():
                    self.dispo = True
                    msg = f"Une nouvelle version du plugin {self.name} est disponible. Allez dans le menu des extensions pour faire la mise à jour."
                    flashMsg(msg, category='error')  

    
    def compare(self):
        ref = self.meta.getVersion().split('.')
        new = self.version.split('.')
        for i in range(len(new)):
            n = int(new[i])
            if i<len(ref):
                r = int(ref[i])
                if r>n:
                    return False
                if r<n:
                    return True
            else:
                return True

    def doRequest(self, url):
        req = QtNetwork.QNetworkRequest(QUrl(url))
        self.nam = QtNetwork.QNetworkAccessManager()
        self.nam.finished.connect(self.handleResponse)
        self.nam.get(req)

    def handleResponse(self, reply):
        er = reply.error()
        if er == QtNetwork.QNetworkReply.NoError:
            bytes_string = reply.readAll()
            self.aff(str(bytes_string, 'utf-8'))
        else:
            # print("Error occured: ", er)
            # print(reply.errorString())
            pass



    def manager(self):
        # manager = QtDesigner.QExtensionManager()
        # plugin = manager.findChild(self.name)
        # print(plugin)
        
        pass
        
        
        










