# -*- coding: utf-8 -*-

import os, sys, configparser, shutil, zipfile
from functools import partial

from qgis.gui import *
from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtCore import *

from .rdi_postgis import rdiPostgis
from .rdi_param import rdiParam
from .rdi_tools import quote, clearLayout
from .rdi_meta import rdiMeta

class rdiSave(QObject):

    def __init__(self, parent):
        QObject.__init__(self)
        self.dock = parent
        self.config = os.path.join(os.path.dirname(__file__), "config")
        self.param = rdiParam()
        self.meta = rdiMeta()
        self.locals = ["svg", "styles", "config"]
        
    def makeFolder(self):
        if not os.path.exists(self.config):
            os.mkdir(self.config)
    
    def getName(self, file):
        base, ext = os.path.splitext(file)
        if ext==".ini":
            return base
    
    def getFile(self, name):
        return os.path.join(self.config, name+".ini")
    
    def listFiles(self):
        self.makeFolder()
        self.list = []
        for f in os.listdir(self.config):
            name = self.getName(f)
            if name:
                self.list.append(name)
                
    def setLayout(self, layout):
        self.layout = layout
    
    def makeListed(self, layout):
        self.setLayout(layout)
        self.listFiles()
        if len(self.list)>0:
            self.makeEntete()
            self.makeCombo()
            self.dock.makeInfoLabel(self.layout, f"Les paramètres en gras diffèrent de la connexion actuelle")
            self.makeInfo()
            self.actu()
            return True
    
    def makeEntete(self):
        l = QtWidgets.QLabel("\n\n")
        self.layout.addWidget(l)
        l = QtWidgets.QLabel("Tester une autre config:")
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        l.setWordWrap(True)
        self.layout.addWidget(l)
    
    def makeCombo(self, compact=False):
        w = QtWidgets.QWidget()
        self.layout.addWidget(w)
        hbox = QtWidgets.QHBoxLayout(w)
        hbox.setSpacing(0)
        if compact:
            hbox.setContentsMargins(0,0,0,0)
        self.combo = QtWidgets.QComboBox()
        hbox.addWidget(self.combo)
        self.combo.setIconSize(QSize(12,12))
        for f in self.list:
            if self.checkInfo(f):
                ico = QtGui.QIcon(os.path.join(self.dock.imageFolder, "plug-ok.png"))
                self.combo.addItem(ico, f, f)
            else:
                self.combo.addItem(f, f)
        self.combo.currentIndexChanged.connect(self.actu)
        ico = QtGui.QIcon(os.path.join(self.dock.imageFolder, "plug.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setToolTip(f"Se connecter")
        butt.setMaximumWidth(30)
        hbox.addWidget(butt)
        butt.clicked.connect(self.restitute)
        self.loadButton = butt
        
    def makeInfo(self, compact=False):
        l = QtWidgets.QTextEdit()
        l.setReadOnly(True)
        l.setAcceptRichText(True)
        self.layout.addWidget(l)
        l.setStyleSheet("QTextEdit {background-color:transparent; margin-left:10px;}");
        self.info = l
        if compact:
            self.info.setVisible(False)
    
    def addMention(self, label, value, ref=None, crlf="\n"):
        self.info.setTextColor(QtGui.QColor("#666666"))
        self.info.setFontItalic(True)
        self.info.insertPlainText(f"{crlf}{label}: ")
        self.info.setFontItalic(False)
        if not self.isSameValue(value, ref):
            self.info.setFontWeight(QtGui.QFont.Bold)
        self.info.setTextColor(QtGui.QColor("#000000"))
        self.info.insertPlainText(value)
        self.info.setFontWeight(QtGui.QFont.Normal)
    
    def isSameValue(self, val, ref):
        return str(val)==str(ref)
    
    def checkInfo(self, name):
        file = self.getFile(name)
        config = configparser.ConfigParser(allow_no_value=True)
        config.optionxform = str
        config.read(file)
        ident = True
        try:
            conn = config["CONN"]
            admin = config["ADMIN"]
            ident = ident and self.isSameValue(conn['host'], self.param.get('host'))
            ident = ident and self.isSameValue(conn['port'], self.param.get('port'))
            ident = ident and self.isSameValue(conn['user'], self.param.get('user'))
            ident = ident and self.isSameValue(conn['db'], self.param.get('db'))
            ident = ident and self.isSameValue(conn['schema'], self.param.get('schema', 'CONN'))
            ident = ident and self.isSameValue(admin['tableTmp'], self.param.get('tableTmp'))
            ident = ident and self.isSameValue(admin['schema'], self.param.get('schema', 'ADMIN'))
            ident = ident and self.isSameValue(admin['tableAdmin'], self.param.get('tableAdmin'))
        except:
            ident = False
        return ident
    
    def setInfo(self, name):
        file = self.getFile(name)
        config = configparser.ConfigParser(allow_no_value=True)
        config.optionxform = str
        config.read(file)
        self.info.setText("")
        try:
            conn = config["CONN"]
            admin = config["ADMIN"]
            self.addMention("Hôte", conn['host'], self.param.get('host'), "")
            self.addMention("      →port", conn['port'], self.param.get('port'))
            self.addMention("Utilisateur", conn['user'], self.param.get('user'))
            self.addMention("Base", conn['db'], self.param.get('db'))
            self.addMention("Execution", conn['schema'], self.param.get('schema', 'CONN'))
            self.addMention("      →table", admin['tableTmp'], self.param.get('tableTmp'))
            self.addMention("Administration", admin['schema'], self.param.get('schema', 'ADMIN'))
            self.addMention("      →table", admin['tableAdmin'], self.param.get('tableAdmin'))
            return True
        except:
            txt = f"Le fichier ne correspond pas aux standards du plugin"
            self.info.setTextColor(QtGui.QColor("#666666"))
            self.info.setFontItalic(True)
            self.info.setFontWeight(QtGui.QFont.Normal)
            self.info.setText(txt)
            # print(sys.exc_info())
        
    def actu(self):
        name = self.combo.currentText()
        ok = self.setInfo(name)
        if not ok:
            self.loadButton.setEnabled(False)
        else:
            self.loadButton.setEnabled(True)
        
    def memorize(self):
        name,  ok = QtWidgets.QInputDialog.getText(None, 'Enregistrement', f"Entrez un nom de configuration")
        name = name.strip()
        if ok and name!="":
            file = self.getFile(name)
            shutil.copy2(self.param.file, file)

    def restitute(self):
        file = self.getFile(self.combo.currentText())
        shutil.copy2(file, self.param.file)
        self.dock.rebuilt()

    def backup(self):
        target = self.meta.readIni("archive")
        dialog = QtGui.QFileDialog()
        dialog.setFileMode(QtGui.QFileDialog.Directory)
        dialog.setOption(QtGui.QFileDialog.ShowDirsOnly, True)
        target = dialog.getExistingDirectory(None, 'Choisir un emplacement', target)
        if target and target!="":
            self.meta.writeIni("archive", target)
            target = os.path.join(target, "rdi_plugin_archive")
            if os.path.exists(target):
                shutil.rmtree(target)
            os.mkdir(target)
            for d in self.locals:
                src = os.path.join(os.path.dirname(__file__), d)
                dst = os.path.join(target, d)
                shutil.copytree(src, dst, ignore=self.ignorePath(os.path.join(src, "tmp")))
            shutil.copy2(self.param.file, target)
            
            shutil.make_archive(target, 'zip', target)
            shutil.rmtree(target)

    def restore(self):
        target = self.meta.readIni("archive")
        dialog = QtGui.QFileDialog()
        dialog.setNameFilter("ZIP Files (*.zip)")
        target, mime = dialog.getOpenFileName(None, 'Archive à restaurer', target, filter="ZIP (*.zip)")
        dst = os.path.dirname(__file__)
        if os.path.exists(target):
            zip = zipfile.ZipFile(target, 'r')
            for f in zip.namelist():
                zip.extract(f, dst)


    def ignorePath(self, path):
        def ignoref(directory, contents):
            return (f for f in contents if directory==path)
        return ignoref



