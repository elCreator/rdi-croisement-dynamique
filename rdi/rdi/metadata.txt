# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=RDI Croisement Dynamique
qgisMinimumVersion=3.4
description=Croisement dynamique et affichage des enjeux dans les emprises des aléas
version=1.14
author=Eric Lefevre (DDT-67)
email=eric.lefevre@bas-rhin.gouv.fr

about=Ce plugin nécessite la connexion à une base Postgis avec un schéma accessible en écriture

tracker=https://gitlab.com/elCreator/rdi-croisement-dynamique/-/incidents
repository=https://gitlab.com/elCreator/rdi-croisement-dynamique.git
# End of mandatory metadata

# Recommended items:

hasProcessingProvider=no
# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=python

homepage=https://gitlab.com/elCreator/rdi-croisement-dynamique
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=

Category of the plugin: Raster, Vector, Database or Web
# category=

# If the plugin can run on QGIS Server.
server=False

