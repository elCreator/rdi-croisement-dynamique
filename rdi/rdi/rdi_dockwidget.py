# -*- coding: utf-8 -*-

import os, logging, json, time, sys
from functools import partial

from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtCore import *

from .rdi_postgis import rdiPostgis
from .rdi_dynamic import rdiDynamic
from .rdi_param import rdiParam, rdiParamElt
from .rdi_layer import rdiLayer, rdiLayersCollection
from .rdi_tools import rdiTimer, setTimeout, clearLayout, clickLabel, openExplorer, openFile
from .rdi_admin import rdiAdmin
from .rdi_legend import rdiLegend
from .rdi_statistic import rdiStatistic
from .rdi_save import rdiSave
from .rdi_maj import rdiMaj
from .rdi_atlas import rdiAtlas

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'base.ui'))


class rdiDockWidget(QtWidgets.QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, parent=None):
        """Constructor."""
        super(rdiDockWidget, self).__init__()
        self.setupUi(self)
        layout = QtWidgets.QGridLayout()
        layout.setAlignment(Qt.AlignTop)
        layout.setSpacing(0)
        layout.setContentsMargins(0,0,0,0)
        self.dockWidgetContents.setLayout(layout)
        self.layout = layout
        self.parent = parent
        self.alea = None
        self.enjeu = None
        self.filterMode = None
        self.offline = False
        self.imageFolder = os.path.join(os.path.dirname(__file__),'images')
        self.initData()
        self.majDispo()
        self.waitWidget()
    
    def majDispo(self):
        self.maj = rdiMaj()
        self.maj.test()
    
    def actuParams(self):
        self.param = rdiParam()
        self.dyn.actuParams()   
        self.psql.actuParams()
        
    def majBlocState(self):
        self.blocState = {}
        if self.param.get('blocDropExpand'):
            for b in self.psql.getTableAdminBlocs():
                self.blocState[b] = True

    def actuTabs(self, list):
        for code in list:
            if code=='all':
                ind = self.tab.currentIndex()
                self.builtWidget()
                self.tab.setCurrentIndex(ind)
            if code=='dynamic':
                clearLayout(self.lDyn)
                self.buildTabDyn()
            if code=='parameter':    
                clearLayout(self.lPar)
                self.buildTabPar()
            if code=='filter':
                clearLayout(self.lFil)
                self.buildTabFil()
            if code=='statistic':
                clearLayout(self.lStat)
                self.buildTabStat()
            if code=='database':        
                clearLayout(self.lBd)
                self.buildTabBd()
            if code=='journal':    
                clearLayout(self.lLog)
                self.buildTabLog()
            if code=='cure':    
                clearLayout(self.lCure)
                self.buildTabCure()
            if code=='layer':    
                for enjeu in self.enjeu.getElts():
                    enjeu.cluster = self.param.get('cluster')
                self.actuLayersEnjeu()
                self.actuLayersAlea()
                
    
    def recupPassword(self):
        try:
            password = self.parent.passwordTmp
            del self.parent.passwordTmp
        except:
            password = None
        return password
    
    def connectSubClasses(self):
        self.param = rdiParam()
        self.dyn = rdiDynamic()
        self.psql = rdiPostgis(self.builtWidget, self.waitConn, self.recupPassword())
        self.stat = rdiStatistic(self)
        self.save = rdiSave(self)
        self.atlas = rdiAtlas(self)
    
    def waitWidget(self):      
        l = QtWidgets.QLabel("\n")
        self.layout.addWidget(l)
        self.waitConn = l
        self.connectSubClasses()
    
    def brokeWidget(self):
        clearLayout(self.layout)

        l = QtWidgets.QLabel()
        self.layout.addWidget(l)
        l = QtWidgets.QLabel("Connexion perdue")
        l.setWordWrap(True)
        self.layout.addWidget(l)
        l = QtWidgets.QLabel()
        self.layout.addWidget(l)
        
        w = QtWidgets.QWidget()
        self.layout.addWidget(w)
        hbox = QtWidgets.QHBoxLayout(w)
        hbox.setSpacing(0)
        hbox.setContentsMargins(0,0,0,0)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "plug.png"))
        butt = QtWidgets.QPushButton(ico, " Ressayer")
        butt.setMaximumWidth(200)
        hbox.addWidget(butt)
        butt.clicked.connect(self.rebuilt)  

    def setOffline(self, state):
        self.offline = state
    
    def builtOffline(self):
        self.setOffline(True)
        self.builtWidget()
            
    def builtWidget(self):
        clearLayout(self.layout)
        if self.psql.error and not self.offline:
            l = QtWidgets.QLabel()
            self.layout.addWidget(l)
            gb = QtWidgets.QGroupBox("Echec de connexion:")
            self.layout.addWidget(gb)
            lgb = QtWidgets.QVBoxLayout(gb)
            l = QtWidgets.QLabel(self.psql.error)
            l.setWordWrap(True)
            lgb.addWidget(l)
            
            w = QtWidgets.QWidget()
            self.layout.addWidget(w)
            hbox = QtWidgets.QHBoxLayout(w)
            hbox.setSpacing(0)
            hbox.setContentsMargins(0,0,0,0)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "config.png"))
            butt = QtWidgets.QPushButton(ico, " Configurer")
            butt.setMaximumWidth(150)
            butt.clicked.connect(self.parent.configure)
            hbox.addWidget(butt)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "plug.png"))
            butt = QtWidgets.QPushButton(ico, " Ressayer")
            butt.setMaximumWidth(200)
            hbox.addWidget(butt)
            butt.clicked.connect(self.rebuilt)  

            self.save.makeListed(self.layout)
            
            l = QtWidgets.QLabel()
            self.layout.addWidget(l)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, "plug-ko.png"))
            butt = QtWidgets.QPushButton(ico, " Hors connexion")
            butt.setMaximumWidth(200)
            self.layout.addWidget(butt)
            self.layout.setAlignment(butt, Qt.AlignRight)
            butt.clicked.connect(self.builtOffline)
            
        else:            
            self.majBlocState()
            
            if self.offline:
                w = QtWidgets.QWidget()
                self.layout.addWidget(w)
                hbox = QtWidgets.QHBoxLayout(w)
                # hbox.setSpacing(0)
                # hbox.setContentsMargins(0,0,0,0)
                ico = QtGui.QIcon(os.path.join(self.imageFolder,'plug-ko.png'))
                l = QtWidgets.QLabel()
                l.setMaximumWidth(20)
                l.setPixmap(ico.pixmap(15,15))
                hbox.addWidget(l)
                l = QtWidgets.QLabel(f"Hors connexion")
                font = QtGui.QFont()
                font.setBold(True)
                l.setFont(font)
                hbox.addWidget(l)
                ico = QtGui.QIcon(os.path.join(self.imageFolder, "plug.png"))
                butt = QtWidgets.QPushButton(ico, " Se connecter")
                butt.setMaximumWidth(150)
                butt.clicked.connect(self.rebuilt)
                hbox.addWidget(butt)
            
            self.tab = QtWidgets.QTabWidget()
            self.tab.tabBarDoubleClicked.connect(self.openNotice)
            self.layout.addWidget(self.tab) 
            self.lDyn = self.makeTab('dyn')
            self.lPar = self.makeTab('par')
            self.lFil = self.makeTab('fil')
            self.lStat = self.makeTab('stat')
            if self.param.get('admin'):
                self.lBd = self.makeTab('bd')
                self.lLog = self.makeTab('log')
                self.lCure = self.makeTab('cure')
            else:
                self.lBd = None
                self.lLog = None
                self.lCure = None
            self.buildTabDyn()
            self.buildTabFil()
            self.buildTabPar()
            self.buildTabStat()
            self.buildTabBd()
            self.buildTabLog()
            self.buildTabCure()

        
    def makeTab(self, code):
        item = self.tabDef.get(code)
        tip = item.get('info')
        if not item.get('admin') or self.param.get('admin'):
            w = QtWidgets.QWidget()
            l = QtWidgets.QVBoxLayout()
            l.setSpacing(0)
            l.setContentsMargins(0,0,0,0)
            l.setAlignment(Qt.AlignTop)
            self.makeInfoLabel(l, tip + f"\n(double-clic sur icône onglet pour afficher l'aide)")
            w.setLayout(l)
            ico = QtGui.QIcon(os.path.join(self.imageFolder, item.get('img')))
            i = self.tab.addTab(w, ico, "")
            self.tabInd[i] = item
            if tip:
                self.tab.setTabToolTip(i, tip)
            return l
    
    def rebuilt(self, ):
        self.closeAllLayer()
        clearLayout(self.layout)
        self.setOffline(False)
        self.waitWidget() 
   
    def makeInfoLabel(self, layout, text):
        if self.param.get('info') and text:
            l = QtWidgets.QLabel(text)
            l.setWordWrap(True)
            font = QtGui.QFont()
            font.setItalic(True)
            l.setFont(font)
            layout.addWidget(l)

    def makeHelpButton(self, layout, tag=None, lib=False):
        if self.param.get('help'):
            ico = QtGui.QIcon(os.path.join(self.imageFolder,'help.png'))
            l = clickLabel()
            l.setToolTip(u"Aide en ligne ")
            l.setMaximumWidth(20)
            l.setPixmap(ico.pixmap(15,15))
            l.clicked.connect(partial(self.parent.notice, tag))
            if lib:
                hbox = QtWidgets.QHBoxLayout()
                hbox.setAlignment(Qt.AlignRight)
                layout.addLayout(hbox)
                lab = QtWidgets.QLabel(u"Aide en ligne ")
                font = QtGui.QFont()
                font.setItalic(True)
                lab.setFont(font)
                hbox.addWidget(lab)
                hbox.addWidget(l)
            else:
                layout.addWidget(l)
            
    def openNotice(self, ind):
        tag = self.tabInd[ind].get('help')
        self.parent.notice(tag)

    
    # ONGLET CROISEMENT DYNAMIQUE
    def buildTabDyn(self):
        if self.lDyn==None:
            return
        
        item = self.tabDef.get('dyn')
        
        self.infoTask = QtWidgets.QLabel()
        self.infoTask.setAlignment(Qt.AlignRight)
        font = QtGui.QFont()
        font.setPointSize(2)
        self.infoTask.setFont(font)
        self.lDyn.addWidget(self.infoTask)
        
        self.recup = self.dyn.getAll()
        txt = u"\n\nCe premier bloc regroupe toutes les couches d'enjeux, " + \
                "c'est à dire celles dont les informations seront extraites à l'intérieur " + \
                "(ou à l'extérieur en activant l'inversibilité sur clic droit)" + \
                "des aléas"
        self.makeInfoLabel(self.lDyn, txt)
        self.enjeu = self.makeBlocLayerFromList(self.lDyn, "enjeu", self.psql.getTableAdmin('enjeu', True), self.actuLayersEnjeu)
        txt = u"\n\nCe deuxième bloc regroupe toutes les couches d'aléas, " + \
                "c'est à dire celles dont les périmètres vont servir pour extraire les enjeux" + \
                "(ce sont donc forcément des polygones)"
        self.makeInfoLabel(self.lDyn, txt)
        self.alea = self.makeBlocLayerFromList(self.lDyn, "alea", self.psql.getTableAdmin('alea', True), self.actuLayersAlea)
        
        bool = False
        for k,v in self.recup.items():
            self.dyn.removeMapLayer(v.rdi)
            if v.rdi.coll.matchCode('alea'):
                bool = True
        if bool:
            self.actuLayersEnjeu()
            
        if self.param.get('cluster') and self.param.get('clusterField'):
            self.enjeu.addLayerField()

        item = self.tabDef.get('dyn')
        self.makeHelpButton(self.lDyn, item.get('help'), lib=True)
        
    def makeBlocLayerFromList(self, layout, code, list, actu=None):
        coll = rdiLayersCollection(self, code)
        if actu:
            coll.setActu(actu)
        coll.construct(layout)
        parents = {}
        for elt in list:
            rdi = rdiLayer(elt)
            target = rdi
            parent = coll.find(rdi.parent())
            if parent and self.param.get('cbCompact'):
                parent.addCombo(rdi)
                target = parent
            else: 
                force = self.param.get('cbCompact') and self.hasChildrens(rdi, list)
                coll.add(rdi, force)
            if rdi.id() in self.recup.keys() and (not parent or parent.id() not in parents):
                target.recup(self.recup[rdi.id()])  
                del self.recup[rdi.id()]
                if parent and self.param.get('cbCompact'):
                    parents[parent.id()] = True

        return coll
        
    def hasChildrens(self, rdi, list):
        bool = False
        if rdi.tableName():
            bool = True
        for elt in list:
            if bool:
                break
            if elt.get('parent')==rdi.id():
                bool = True
        return bool
      
    def actuLayersEnjeu(self, enjeu=None):
        try:
            aleaList = self.alea.getEltsSelected()
            if enjeu:
                self.psql.getExtractEnjeu(enjeu=enjeu, aleaList=aleaList)   
            else:
                for enjeu in self.enjeu.getEltsSelected():
                    self.psql.getExtractEnjeu(enjeu=enjeu, aleaList=aleaList)   
        except:
            print(sys.exc_info())
            pass
    
    def actuLayersAlea(self, alea=None):
        try:
            if alea:
                self.psql.getExtractAlea(alea=alea)   
            else:
                for alea in self.alea.getEltsSelected():
                    self.psql.getExtractAlea(alea=alea)  
        except:
            print(sys.exc_info())
            pass
                    
     
     
     
     
     
     
     
     
     
    # ONGLET LEGENDE ET FILTRES
    def buildTabFil(self):  
        if self.lFil==None:
            return
        self.makeFilters(self.lFil)
        
        item = self.tabDef.get('fil')
        self.makeHelpButton(self.lFil, item.get('help'), lib=True)

    def makeFilters(self, layout):
        first = None
        w = QtWidgets.QGroupBox("Filtrer sur aléas")
        layout.addWidget(w)
        vbox = QtWidgets.QVBoxLayout(w)
        vbox.setSpacing(2)
        for v in ["Aucun", "Légende", "Sélection", "Visible"]:
            b = QtWidgets.QRadioButton(v, w)
            b.clicked.connect(partial(self.toggleFilter, b))
            vbox.addWidget(b)
            if self.filterMode==v or self.filterMode==None and not first:
                first = b
            if v=="Visible":
                # b.setEnabled(False)
                pass

        sc = QtWidgets.QScrollArea()
        sc.setWidgetResizable(True)
        layout.addWidget(sc)
        w = QtWidgets.QWidget()
        w.setObjectName("scrollAreaWidgetContents")
        gbox = QtWidgets.QVBoxLayout()
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        w.setLayout(gbox)
        sc.setWidget(w)
        sc.setStyleSheet("QAbstractScrollArea {background-color: transparent;}  ");
        w.setStyleSheet("QWidget#scrollAreaWidgetContents {background-color: white; } ");

        self.labelTitle = QtWidgets.QLabel()
        font = QtGui.QFont()
        font.setItalic(True)
        self.labelTitle.setFont(font)
        gbox.addWidget(self.labelTitle)

        w = QtWidgets.QWidget()
        gbox.addWidget(w)
        vbox = QtWidgets.QVBoxLayout(w)
        self.widgetFilterlegend = w
        self.legend = rdiLegend(self, vbox)
        for v in self.dyn.getAll().values():
            if v.rdi.style.isFilterable:
                try:
                    name = v.rdi.style.json['func']
                    legend = v.rdi.style.legend
                    self.legend.add(name, legend, v.rdi)
                except:
                    pass
                
        w = QtWidgets.QWidget()
        gbox.addWidget(w)
        hbox = QtWidgets.QHBoxLayout(w)
        self.widgetFilterSelect = w
        ico = QtGui.QIcon(os.path.join(self.imageFolder,'filter.png'))
        butt = QtWidgets.QPushButton(ico, " Filtrer")
        hbox.addWidget(butt)
        butt.clicked.connect(self.filterSelection)
        self.widgetFilterSelect.butt = butt
        ico = QtGui.QIcon(os.path.join(self.imageFolder,'back.png'))
        butt = QtWidgets.QPushButton(ico, " Annuler")
        hbox.addWidget(butt)
        butt.clicked.connect(self.filterReset)
        self.widgetFilterSelect.buttCancel = butt
        self.widgetFilterSelect.buttCancel.setEnabled(False)
        
        w = QtWidgets.QWidget()
        gbox.addWidget(w)
        hbox = QtWidgets.QHBoxLayout(w)
        self.widgetFilterVisible = w
        ico = QtGui.QIcon(os.path.join(self.imageFolder,'filter.png'))
        butt = QtWidgets.QPushButton(ico, " Filtrer")
        hbox.addWidget(butt)
        butt.clicked.connect(self.filterVisible)
        self.widgetFilterVisible.butt = butt
        ico = QtGui.QIcon(os.path.join(self.imageFolder,'back.png'))
        butt = QtWidgets.QPushButton(ico, " Annuler")
        hbox.addWidget(butt)
        butt.clicked.connect(self.filterReset)
        self.widgetFilterVisible.buttCancel = butt
        self.widgetFilterVisible.buttCancel.setEnabled(False)

        first.click()

    
    def toggleFilter(self, b):
        self.filterMode = b.text()
        if b.text()=="Aucun":
            self.labelTitle.setText(f"")
        if b.text()=="Légende":
            self.labelTitle.setText(f"Filtrer depuis les items de la légende")
            self.widgetFilterlegend.show()
            self.legend.makeContext()
        else:
            self.widgetFilterlegend.hide()
        if b.text()=="Sélection":
            self.labelTitle.setText(f"Filtrer depuis votre sélection")
            self.widgetFilterSelect.show()
        else:
            self.widgetFilterSelect.hide()
        if b.text()=="Visible":
            self.labelTitle.setText(f"Filtrer depuis les éléments visibles")
            self.widgetFilterVisible.show()
        else:
            self.widgetFilterVisible.hide()
        self.filterReset()
    
    def filterSelection(self):
        self.widgetFilterSelect.buttCancel.setEnabled(True)
        for v in self.dyn.getAll().values():
            sel = v.selectedFeatures()
            pkey = v.rdi.getIdentFeature()
            lst = list(map(lambda x: x.attribute(x.fieldNameIndex(pkey)), sel))
            v.rdi.setContextFromList(pkey, lst)
        self.actuLayersAlea()
        self.actuLayersEnjeu()
        
    def filterVisible(self):
        self.widgetFilterVisible.buttCancel.setEnabled(True)
        for v in self.dyn.getAll().values():
            pkey = v.rdi.getIdentFeature() 
            vis = []
            renderer = v.rdi.vlayer.renderer().clone()
            ctx = QgsRenderContext()
            renderer.startRender(ctx, QgsFields())
            for feature in v.rdi.vlayer.getFeatures():
                ctx.expressionContext().setFeature(feature)
                if renderer.willRenderFeature(feature, ctx):
                    vis.append(feature)
            renderer.stopRender(ctx)
            lst = list(map(lambda x: x.attribute(x.fieldNameIndex(pkey)), vis))
            v.rdi.setContextFromList(pkey, lst)
        self.actuLayersAlea()
        self.actuLayersEnjeu()
        
    def filterReset(self):
        self.widgetFilterSelect.buttCancel.setEnabled(False)
        for v in self.dyn.getAll().values():
            v.rdi.context = None
        self.actuLayersAlea()
        self.actuLayersEnjeu()
    
                


        
    # ONGLET PARAMETRES
    def buildTabPar(self):  
        if self.lPar==None:
            return
        self.makeParameters(self.lPar)
        
        item = self.tabDef.get('par')
        self.makeHelpButton(self.lPar, item.get('help'), lib=True)
        
    def makeParameters(self, layout):
        l = QtWidgets.QLabel("Options du plugin")
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        layout.addWidget(l)

        sc = QtWidgets.QScrollArea()
        sc.setWidgetResizable(True)
        layout.addWidget(sc)
        w = QtWidgets.QWidget()
        w.setObjectName("scrollAreaWidgetContents")
        gbox = QtWidgets.QVBoxLayout()
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        w.setLayout(gbox)
        sc.setWidget(w)
        sc.setStyleSheet("QAbstractScrollArea {background-color: transparent;}  ");
        w.setStyleSheet("QWidget#scrollAreaWidgetContents {background-color: white; } ");

        for b, bloc in self.param.translator.items():
            self.makeInfoLabel(gbox, "\n\n" + bloc['info'])
            l = QtWidgets.QLabel()
            gbox.addWidget(l)
            gb = QtWidgets.QGroupBox(bloc['label'])
            gbox.addWidget(gb)
            lgb = QtWidgets.QVBoxLayout(gb) 
            for elt in bloc['list']:
                if not 'dep' in elt or self.param.get(elt['dep']):
                    p = rdiParamElt(elt, self)
                    p.changed.connect(self.changeParameter)
                    p.draw(lgb)

    def changeParameter(self, elt):
        self.actuParams()
        if 'func' in elt:
            for f in elt['func']:
                exec(f)
        if 'tab' in elt:
            self.actuTabs(elt['tab'])
        






    # ONGLET STATISTIQUES
    def buildTabStat(self):
        if self.lStat==None:
            return
        self.makeStat(self.lStat)
        
        item = self.tabDef.get('stat')
        self.makeHelpButton(self.lStat, item.get('help'), lib=True)
    
    def makeStat(self, layout):
        self.stat.load(layout)
        self.enjeu.changed.connect(self.stat.makeStatLayers)
        
        
        
        
        



    # ONGLET BASE DE DONNEES
    def buildTabBd(self):  
        if self.lBd==None:
            return
        self.makeBD(self.lBd)
        
        item = self.tabDef.get('bd')
        self.makeHelpButton(self.lBd, item.get('help'), lib=True)
    
    def makeBD(self, layout):
        admin = rdiAdmin(self)
        layout.addWidget(admin.widget)
        admin.dataChanged.connect(self.reloadBD)
        admin.datasChanged.connect(self.reloadBDlist)
        reco = admin.reconnectButton()
        layout.addWidget(reco)
        reco.butt.clicked.connect(self.rebuilt) 

    def reloadBDlist(self, list):
        self.actuTabs(['dynamic', 'statistic'])
        for id in list:
            rdi = self.alea.find(id) or self.enjeu.find(id)
            self.reloadBDrdi(rdi)
                    
    def reloadBD(self, id=None):
        self.actuTabs(['dynamic', 'statistic'])
        rdi = self.alea.find(id) or self.enjeu.find(id)
        self.reloadBDrdi(rdi)
        
    def reloadBDrdi(self, rdi):
        if rdi!=None:
            # rdi.resetRenderer()
            if rdi.style!=None:
                rdi.style.actuStyle()
            if rdi.checked:
                if rdi.coll.matchCode('alea'):
                    self.actuLayersAlea(rdi)
                if rdi.coll.matchCode('enjeu'):
                    self.actuLayersEnjeu(rdi)

    # ONGLET JOURNALISATION
    def buildTabLog(self):  
        if self.lLog==None:
            return
        self.makeLogsInfo(self.lLog)
        
        item = self.tabDef.get('log')
        self.makeHelpButton(self.lLog, item.get('help'), lib=True)
        
    def makeLogsInfo(self, layout):      
        gb = QtWidgets.QGroupBox("Journal des tâches")
        layout.addWidget(gb)
        lgb = QtWidgets.QVBoxLayout(gb)
        lgb.setSpacing(0)
        lgb.setContentsMargins(0,0,0,0)
        sc = QtWidgets.QScrollArea()
        sc.setWidgetResizable(True)
        lgb.addWidget(sc)
        w = QtWidgets.QWidget()
        gbox = QtWidgets.QVBoxLayout(w)
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        l = QtWidgets.QTextEdit()
        l.setReadOnly(True)
        gbox.addWidget(l)
        self.tasksLog = l
        sc.setWidget(w)
        sc.setStyleSheet("QScrollArea {background-color:transparent;}");
        w.setStyleSheet("background-color:transparent;");
        
        wl = QtWidgets.QWidget()
        layout.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout()
        hbox.setContentsMargins(15,0,5,5)
        hbox.setSpacing(1)
        wl.setLayout(hbox)
        cb = QtWidgets.QCheckBox("Actualisation auto")
        if self.param.get('animation'):
            cb.setChecked(True)
        else:
            cb.setEnabled(False)
        cb.stateChanged.connect(self.checkActiveTasksStart)
        self.actuAuto = cb
        hbox.addWidget(cb)
        
        ico = QtGui.QIcon(os.path.join(self.imageFolder,'stop.png'))
        butt = QtWidgets.QPushButton(ico, '')
        butt.setToolTip("Tout arrêter")
        butt.setMaximumWidth(30)
        hbox.addWidget(butt)
        butt.clicked.connect(self.psql.manager.killAll)
        butt.setEnabled(False)
        self.stopAll = butt
        ico = QtGui.QIcon(os.path.join(self.imageFolder,'refresh.png'))
        butt = QtWidgets.QPushButton(ico, '')
        butt.setToolTip("Actualiser")
        butt.setMaximumWidth(30)
        hbox.addWidget(butt)
        butt.clicked.connect(self.checkActiveTasks)

        self.psql.manager.changed.connect(self.checkActiveTasksStart)
        self.psql.manager.changed.connect(self.showCharge)
        self.timer = rdiTimer(self, self.checkActiveTasks)
        self.checkActiveTasks()

    def checkActiveTasksStart(self):
        try:
            if self.actuAuto.isChecked():
                self.timer.start(1000)
                self.checkActiveTasks()
            else:
                self.timer.stop()
        except:
            self.timer.stop()
            pass
    
    def checkActiveTasks(self):
        try:
            nb = self.psql.manager.countAlive()
            self.psql.manager.setLog(self.tasksLog)
            QtWidgets.QApplication.processEvents()
            if nb==0:
                self.stopAll.setEnabled(False)
                self.timer.stop(10000)
            else:
                self.stopAll.setEnabled(True)
        except:
            self.timer.stop()
            pass
        
    def showCharge(self):
        i = self.psql.manager.countAlive()
        r = min(255, i*50)
        g = 255 - min(255, max(0,i-5)*10)
        b = 0
        a = min(i*30, 255)
        color = QtGui.QColor(r, g, b, a)
        self.infoTask.setStyleSheet(f"background-color:{color.name(QtGui.QColor.HexArgb)};")
        if i>0:
            s = "s" if i>1 else ""
            txt = f"{i} requête{s} active{s}"
            self.infoTask.setToolTip(txt)
        else:
            self.infoTask.setToolTip(None)

        






    # ONGLET MAINTENANCE
    def buildTabCure(self):  
        if self.lCure==None:
            return
        self.makeCure(self.lCure)
        
        item = self.tabDef.get('cure')
        self.makeHelpButton(self.lCure, item.get('help'), lib=True)
    
    def makeCure(self, layout):
        
        
        gb = QtWidgets.QGroupBox("Maintenance")
        layout.addWidget(gb)
        lgb = QtWidgets.QVBoxLayout(gb)
        lgb.setSpacing(0)
        lgb.setContentsMargins(1,1,1,1)
        version = "1.1"
        txt = ""
        txt += f"Plugin version {self.maj.meta.getVersion()}"
        if self.offline:
            txt += f"\nMode déconnecté"
        else:
            txt += f"\nConnexion: {self.psql.PGuser} (sur {self.psql.PGhost})"
            txt += f"\nBase de données: {self.psql.PGdb}"
            txt += f"\n     Execution sur {self.psql.getSchema(self.param.get('schema', 'CONN'))} ({self.param.get('tableTmp')})"
            txt += f"\n     Administration sur {self.psql.getSchema(self.param.get('schema', 'ADMIN'))} ({self.param.get('tableAdmin')})"
        l = QtWidgets.QLabel(txt)
        l.setWordWrap(True)
        font = QtGui.QFont()
        font.setItalic(True)
        l.setFont(font)
        lgb.addWidget(l)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "save.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setIconSize(QSize(12,12))
        butt.setToolTip("Enregistrer une copie de toute la configuration")
        butt.setMaximumWidth(30)
        lgb.addWidget(butt)
        lgb.setAlignment(butt, Qt.AlignRight)
        butt.clicked.connect(self.save.memorize)
        
        self.save.listFiles()
        if len(self.save.list)>0:
            wl = QtWidgets.QWidget()
            layout.addWidget(wl)
            hbox = QtWidgets.QHBoxLayout(wl)
            hbox.setContentsMargins(30,0,0,0)
            l = QtWidgets.QLabel("Changer d'environnement")
            l.setWordWrap(True)
            hbox.addWidget(l)
            self.save.setLayout(hbox)
            self.save.makeCombo(compact=True)
            self.save.makeInfo(compact=True)
        
        
        sc = QtWidgets.QScrollArea()
        sc.setWidgetResizable(True)
        layout.addWidget(sc)
        w = QtWidgets.QWidget()
        w.setObjectName("scrollAreaWidgetContents")
        gbox = QtWidgets.QVBoxLayout()
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        w.setLayout(gbox)
        sc.setWidget(w)
        sc.setStyleSheet("QAbstractScrollArea {background-color: transparent;}  ");
        w.setStyleSheet("QWidget#scrollAreaWidgetContents {background-color: white; } ");
        layout2 = gbox
        
        if self.maj.dispo:
            txt = f"\n     → MàJ disponible: v{self.maj.version}"
            l = QtWidgets.QLabel(txt)
            font = QtGui.QFont()
            font.setBold(True)
            font.setPointSize(10)
            l.setFont(font)
            layout2.addWidget(l)
        
        wl = QtWidgets.QLabel("\n")
        layout.addWidget(wl)
        wl = QtWidgets.QWidget()
        layout2.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        
        w = QtWidgets.QWidget()
        hbox.addWidget(w)
        hbox.setAlignment(w, Qt.AlignLeft)
        hbox2 = QtWidgets.QHBoxLayout(w)
        hbox2.setSpacing(0)
        hbox2.setContentsMargins(0,0,0,0)
        b = QtWidgets.QPushButton("")
        b.setToolTip("Teste le nombre de requêtes stockées en base de données")
        b.setFixedSize(15, 32)
        hbox2.addWidget(b)
        hbox2.setAlignment(b, Qt.AlignLeft)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "delete.png"))
        butt = QtWidgets.QPushButton(ico, ' Purger stockage\n    temporaire')
        butt.setIconSize(QSize(25,25))
        butt.setToolTip("Supprime toutes les requêtes stockées en base de données")
        butt.setMinimumWidth(120)
        hbox2.addWidget(butt)
        hbox2.setAlignment(butt, Qt.AlignLeft)
        b.clicked.connect(partial(self.startPurge, butt, self.psql.nbTmp))
        butt.clicked.connect(partial(self.startPurge, butt, self.psql.purge))
        w = QtWidgets.QWidget()
        w.setMaximumWidth(80)
        hbox.addWidget(w)
        hbox.setAlignment(w, Qt.AlignRight)
        hbox2 = QtWidgets.QHBoxLayout(w)
        hbox2.setSpacing(0)
        hbox2.setContentsMargins(0,0,0,0)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "config.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setIconSize(QSize(25,25))
        butt.setToolTip("Configurer")
        hbox2.addWidget(butt)
        butt.clicked.connect(self.parent.configure)  
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "plug.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setIconSize(QSize(25,25))
        butt.setToolTip("Reconnecter")
        hbox2.addWidget(butt)
        butt.clicked.connect(self.rebuilt)  
        
        wl = QtWidgets.QLabel("\n")
        layout2.addWidget(wl)
        wl = QtWidgets.QLabel()
        layout2.addWidget(wl)
        wl = QtWidgets.QLabel(f"Ouvrir")
        font = QtGui.QFont()
        font.setBold(True)
        wl.setFont(font)
        layout2.addWidget(wl)
        wl = QtWidgets.QWidget()
        layout2.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "folder.png"))
        butt = QtWidgets.QPushButton(ico, ' Dossier')
        butt.setToolTip("Ouvre le dossier du plugin")
        hbox.addWidget(butt)
        butt.clicked.connect(openExplorer)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "file.png"))
        butt = QtWidgets.QPushButton(ico, ' parameter.ini')
        butt.setToolTip("Ouvre le fichier de paramétrage")
        hbox.addWidget(butt)
        butt.clicked.connect(partial(openFile, 'parameter.ini'))
        
        wl = QtWidgets.QLabel("\n\n")
        layout2.addWidget(wl)
        wl = QtWidgets.QLabel(f"Sauvegarde locale")
        font = QtGui.QFont()
        font.setBold(True)
        wl.setFont(font)
        layout2.addWidget(wl)
        wl = QtWidgets.QWidget()
        layout2.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "backup.png"))
        butt = QtWidgets.QPushButton(ico, " Enregistrer")
        butt.setToolTip("Enregistrer les fichiers persos\nPermet de conserver la paramétrisation locale lors d'une mise à jour du plugin")
        butt.setMaximumWidth(300)
        hbox.addWidget(butt)
        butt.clicked.connect(self.save.backup)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "restore.png"))
        butt = QtWidgets.QPushButton(ico, " Restaurer")
        butt.setToolTip("Restorer les fichiers persos depuis une sauvegarde locale")
        butt.setMaximumWidth(300)
        hbox.addWidget(butt)
        butt.clicked.connect(self.save.restore)
        
        wl = QtWidgets.QLabel("\n\n")
        layout2.addWidget(wl)
        wl = QtWidgets.QLabel(f"Génération Automatique")
        font = QtGui.QFont()
        font.setBold(True)
        wl.setFont(font)
        layout2.addWidget(wl)
        self.atlas.make(layout2)

        
    
    def startPurge(self, obj, action):
        obj.setEnabled(False)
        obj.memoText = obj.text()
        obj.setText("...")
        QtWidgets.QApplication.processEvents()
        # self.psql.purge(self.endPurge, obj)
        action(self.endPurge, obj)
    
    def endPurge(self, txt, obj):
        obj.setText(txt)
        obj.timeout = setTimeout(5000, self.resetPurge, obj)
        
    def resetPurge(self, obj):
        del obj.timeout
        try:
            obj.setText(obj.memoText)
            obj.setEnabled(True)
        except:
            pass
    


    
    def initData(self):
        self.tabDef = {
            'dyn':{
                'img': 'analyze.png', 
                'title': u"Analyser",
                'info': u"Onglet de croisement dynamiques des enjeux avec les aléas",
                'help': 'p5-analyze',
                },
            'par':{
                'img': 'check.png', 
                'title': u"Options du plugin",
                'info': u"Onglet pour configurer le comportement du plugin\nPersonnalisation des croisements spatiaux et de l'affichage",
                'help': 'p5-option',
                },
            'fil':{
                'img': 'filter.png', 
                'title': u"Filtre sur aléas",
                'info': u"Onglet pour filtrer le croisement sur un sous-ensemble des aléas",
                'help': 'p5-filter',
                },
            'stat':{
                'img': 'stats.png', 
                'title': u"Statistiques",
                'info': u"Onglet pour générer des statistiques sur les données d'enjeux",
                'help': 'p5-stats',
                },

            'bd':{
                'img': 'database.png', 
                'title': u"Gestion des données",
                'info': u"Onglet pour administrer les couches du plugin\nCliquer sur + pour importer des couches\nOu choisir la couche à personnaliser\nNécessite des droits d'écritures sur le schéma défini en admin",
                'help': 'p4-data',
                'admin': True,
                },
            'log':{
                'img': 'journal.png', 
                'title': u"Journal des tâches",
                'info': u"Onglet de visualisation des requetes envoyées au serveur et en attente de traitement",
                'help': 'p4-logs',
                'admin': True,
                },
            'cure':{
                'img': 'wheel.png', 
                'title': u"Maintenance",
                'info': u"Onglet de maintenance générale\nNettoyage des couches temoraires\nParamétrage de la connexion",
                'help': 'p4-cure',
                'admin': True,
                }, 
        }
        self.tabInd = {}



    # fin du plugin
    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()
        
        
    def closeAllLayer(self):
        try:
            self.dyn.closeAll()
        except:
            pass



