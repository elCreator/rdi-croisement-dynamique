var summaryActive = null;
var summ = null;
var notice = null;
function summaryIndicatorInit() {
	notice = document.getElementById('notice');
	notice.addEventListener('scroll', summaryIndicator, false);
	summ = document.getElementById('summary');
	summaryIndicator();
}	
function summaryIndicator(ev) {
	memo = null;
	var items = summ.getElementsByTagName('a')
	for (var a=0; a<items.length; a++) {
		var item = items[a];
		var ref = item.getAttribute('href').replace("#","");
		var height = notice.getBoundingClientRect().height;
		var hsum = summ.getBoundingClientRect().height;
		if (ref && ref!="") {
			var d = document.getElementById(ref);
				if (d) {
					var r = d.getBoundingClientRect();
					if (r.top>0 && r.top<height) lightSummaryItem(item, hsum)
					else shutSummaryItem(item)
					if (r.top<30) memo = item;
				}	
		}
	}
	// if (summaryActive) shutSummaryItem(summaryActive);
	if (memo) lightSummaryItem(memo);
	// summaryActive = memo;
}
function lightSummaryItem(item, height) {
	item.style.borderLeft = '2px solid #aaaaaa';
	var r = item.getBoundingClientRect();
	if (r.top<0) summ.scrollTop = summ.scrollTop + r.top - 20;
	if (r.top>height) summ.scrollTop = summ.scrollTop + (r.top-height) + 20;
}
function shutSummaryItem(item) {
	item.style.borderLeft = '2px solid #ffffff';
}

function flat(elt) {
	['summary', 'notice'].forEach( id => {
		var d = document.getElementById(id)
		d.setAttribute('class', "global-block-print");
		d.style.pageBreakBefore = 'always';
	});
	['global-title','summary-title'].forEach( id => {
		var d = document.getElementById(id)
		d.style.display = 'block';
	});
	if (elt) {
		elt.setAttribute('onclick', "unflat(this);")
		elt.setAttribute('title',"Revenir visualisation normale")
		elt.setAttribute('src',"../images/back.png")
		print();
	} else {
		document.body.style.fontFamily = "auto";
	}
}
function unflat(elt) {
	['summary', 'notice'].forEach( id => {
		var d = document.getElementById(id)
		d.setAttribute('class', "global-block global-block-"+id);
	});
	['global-title','summary-title'].forEach( id => {
		var d = document.getElementById(id)
		d.style.display = 'none';
	});
	if (elt) {
		elt.setAttribute('onclick', "flat(this);")
		elt.setAttribute('title',"Imprimer la notice")
		elt.setAttribute('src',"../images/print.png")
	} else {
		document.body.style.fontFamily = "Liberation Serif";
	}
}

function affDiv(elt) {
	target = elt.getAttribute('target')
	div = document.getElementById(target)
	if (div.style.display=='none') {
		div.style.display = 'block';
	} else {
		div.style.display = 'none';
	}
}