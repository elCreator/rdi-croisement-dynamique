# -*- coding: utf-8 -*-

import os, logging, sys
from functools import partial

from qgis.core import *
from qgis.utils import iface
from PyQt5 import QtGui, QtWidgets, uic, QtXml
from PyQt5.QtCore import *

from .rdi_param import rdiParam
from .rdi_tools import quote


memoRdiLegend = {}


class rdiLegend():
    def __init__(self, dock, layout):
        self.dock = dock
        self.items = {}
        self.ori = {}
        self.layout = layout
    
    def setlayout(self, layout):
        self.layout = layout
    
    def add(self, name, catalog, rdi):
        self.ori[rdi] = name
        if not name in self.items:
            item = rdiLegendItem(name, catalog, self.layout)
            item.clicked.connect(partial(self.makeContext, item))
            self.items[name] = item
        else:
            item = self.items[name]
            item.show()
        return item
    
    def makeContext(self, item=None):
        for rdi,name in self.ori.items():
            if not item:
                item = self.items[name]
            if item.name==name:
                rdi.setContextFromLegend(item)
        self.dock.actuLayersAlea()
        self.dock.actuLayersEnjeu()
    
    def remove(self, rdi):
        if rdi in self.ori:
            del self.ori[rdi]
            self.clean()
        
    def clean(self):
        lst = self.ori.values()
        for name,item in self.items.items():
            if not name in lst:
                item.hide()


class rdiLegendItem(QObject):
    
    clicked = pyqtSignal()
    
    def __init__(self, name, catalog, layout):
        QObject.__init__(self)
        self.name = name
        self.catalog = catalog
        self.layout = layout
        self.elts = []
        self.draw()
        
    def draw(self):
        if 'list' in self.catalog:
            if not self.name in memoRdiLegend:
                memoRdiLegend[self.name] = {}
            # l = QtWidgets.QLabel()
            # self.layout.addWidget(l)
            # self.space = l
            gb = QtWidgets.QGroupBox(self.name)
            self.layout.addWidget(gb)
            lgb = QtWidgets.QVBoxLayout(gb) 
            lgb.setSpacing(0)
            for i,e in enumerate(self.catalog['list']):
                elt = rdiLegendItemElt(i, e, lgb)
                if i in memoRdiLegend[self.name] and memoRdiLegend[self.name][i]==False:
                    elt.setChecked(False)
                elt.clicked.connect(self.click)
                self.elts.append(elt)  
            self.group = gb
    
    def click(self, ind, checked):
        memoRdiLegend[self.name][ind] = checked
        self.clicked.emit()
    
    def hide(self):
        # self.space.hide()
        self.group.hide()
        
    def show(self):
        # self.space.show()
        self.group.show()
    


class rdiLegendItemElt(QObject):
    
    clicked = pyqtSignal(int, bool)
    
    def __init__(self, ind, elt, layout):
        QObject.__init__(self)
        self.layout = layout
        self.elt = elt
        self.ind = ind
        self.draw()
        
    def draw(self):
        hbox = QtWidgets.QHBoxLayout()
        hbox.setSpacing(5)
        self.layout.addLayout(hbox)
        l = QtWidgets.QLabel()
        l.setStyleSheet(f"background-color:{self.elt['color']};")
        l.setFixedSize(17, 17)
        hbox.addWidget(l)
        cb = QtWidgets.QCheckBox(self.elt['label'])
        cb.setChecked(True)
        cb.stateChanged.connect(self.click)
        hbox.addWidget(cb)
        self.cb = cb
        
    def click(self):
        self.clicked.emit(self.ind, self.cb.isChecked())
    
    def setChecked(self, bool):
        self.cb.setChecked(bool)
    
    def isChecked(self):
        return self.cb.isChecked()
                
   




