# -*- coding: utf-8 -*-

import sys, os, json
from functools import partial

from qgis.gui import *
from qgis.core import *
from qgis.utils import iface
from PyQt5 import QtGui, QtWidgets, uic, QtXml, QtSvg
from PyQt5.QtCore import *

from .rdi_param import rdiParam
from .rdi_tools import quote, makeUniqueIdent, clearLayout, rdiTimer
from .rdi_layer import rdiLayer, rdiLayersCollection
from .rdi_async import mGlobalTask, manageTasks, abstractQuery

from .rdi_postgis import tmpQuery




class rdiStatistic():

    def __init__(self, dock):
        self.dock = dock
        self.psql = dock.psql
        self.initData()
    
    def load(self, layout):
        self.layout = layout
        self.makeTitle()
        self.makeStatChoice()
        self.makeButton()
        self.makeStatLayers()
        self.makeStatResults()

    def makeTitle(self):
        l = QtWidgets.QLabel("Statistiques")
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        self.layout.addWidget(l)
        
    def makeStatChoice(self):
        form = QtWidgets.QFormLayout()
        self.layout.addLayout(form)
        form.setContentsMargins(20,10,10,10)
        wl = QtWidgets.QLabel("Table ")
        combo = QtWidgets.QComboBox()
        combo.addItem("Aucune", None)
        ico = QtGui.QIcon(os.path.join(self.dock.imageFolder, 'stats.png'))
        for elt in self.psql.getTableAdmin('stats', True):
            rdi = rdiLayer(elt)
            table = rdi.tableName()
            if table:
                name = rdi.layerName()
                combo.addItem(ico, name, elt)
        self.statChoice = combo
        form.addRow(wl, combo)
        combo.currentIndexChanged.connect(self.actuChoiceKey)
        wl = QtWidgets.QLabel("Clé ")
        combo = QtWidgets.QComboBox()
        self.statChoiceKey = combo
        form.addRow(wl, combo)
        self.statChoiceKey.label = wl
        self.actuChoiceKey()
        l = QtWidgets.QLabel("Sur les enjeux:")
        font = QtGui.QFont()
        font.setUnderline(True)
        l.setFont(font)
        self.layout.addWidget(l)
        
        sc = QtWidgets.QScrollArea()
        sc.setWidgetResizable(True)
        self.layout.addWidget(sc)
        w = QtWidgets.QWidget()
        w.setObjectName("scrollAreaWidgetContents")
        gbox = QtWidgets.QVBoxLayout(w)
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        sc.setWidget(w)
        sc.setStyleSheet("QAbstractScrollArea {background-color: transparent;}  ");
        w.setStyleSheet("QWidget#scrollAreaWidgetContents {background-color: white; } ");
        self.layoutEnjeu = gbox
        
        sc.mini = 50
        sc.setMaximumHeight(sc.mini)
        self.scrollEnjeu = sc
        # self.layoutEnjeu = QtWidgets.QVBoxLayout()
        # self.layout.addLayout(self.layoutEnjeu)
        

        
    def makeStatResults(self):
        l = QtWidgets.QLabel("")
        self.layout.addWidget(l)
        l = QtWidgets.QLabel("Résultats:")
        font = QtGui.QFont()
        font.setUnderline(True)
        l.setFont(font)
        self.layout.addWidget(l)
        self.layoutStats = QtWidgets.QVBoxLayout()
        
        self.layout.addLayout(self.layoutStats)
        self.coll = rdiLayersCollection(self.dock, 'stats')
        self.coll.makeList(self.layoutStats)
        for rdi in self.rdiList:
            rdi2 = rdiLayer(rdi.meta)
            rdi2.isStat = True
            self.count += 1
            rdi2.name = rdi.layerName()
            try:
                rdi2.tableResult = rdi.tableResult
                self.coll.add(rdi2, True)
            except:
                self.makeWaitLabel(rdi)
            
    
    def actuChoiceKey(self):
        elt = self.statChoice.currentData()
        while self.statChoiceKey.count():
            self.statChoiceKey.removeItem(0)
        if elt!=None:
            args = (elt.get('tablename'), elt.get('schemaname'))
            fields = self.dock.psql.getColumnsTable(*args)
            pkey = self.dock.psql.getPrimaryKey(*args)
            for f in fields:
                self.statChoiceKey.addItem(f, f)
            if pkey:
                self.statChoiceKey.setCurrentText(pkey)
            self.statChoiceKey.show()
            self.statChoiceKey.label.show()
        else:
            self.statChoiceKey.addItem("id", "id")
            self.statChoiceKey.hide()
            self.statChoiceKey.label.hide()
    
    def makeStatLayers(self):
        clearLayout(self.layoutEnjeu)
        empty = True
        for enjeu in self.dock.enjeu.getEltsSelected():
            empty = False
            if enjeu.stat:
                enjeu.stat.draw()
            else:
                enjeu.stat = rdiStatLayer(enjeu, self.layoutEnjeu)
                enjeu.stat.heightChanged.connect(self.adaptStatLayers)
        if empty:
            l = QtWidgets.QLabel("\n   Aucun enjeu sélectionné")
            font = QtGui.QFont()
            font.setItalic(True)
            l.setFont(font)
            self.layoutEnjeu.addWidget(l)
        self.adaptStatLayers()

    def adaptStatLayers(self):
        h = 20
        for enjeu in self.dock.enjeu.getEltsSelected():
            h += enjeu.stat.getHeight()
        h = max(h, self.scrollEnjeu.mini)
        self.scrollEnjeu.setMaximumHeight(h)
        self.launchAvailable()
            
    def makeButton(self):
        ico = QtGui.QIcon(os.path.join(self.dock.imageFolder, "execute.png"))
        b = QtWidgets.QPushButton(ico, " Calculer")
        b.setFixedSize(100, 30)
        self.layout.addWidget(b)
        b.clicked.connect(self.launchStat)
        self.validButton = b
    
    def makeWaitLabel(self, rdi):
        l = QtWidgets.QLabel(rdi.layerName())
        self.coll.layerBloc.addWidget(l)
        rdi.waitLabel = l
        rdi.timer = rdiTimer(rdi)
        rdi.timer.setLabel(l, deb=l.text())
        rdi.timer.setMaxPts(10)
        rdi.timer.start(200)
        
    def addRdi(self, elt):
        rdi = rdiLayer(elt)
        rdi.isStat = True
        rdi.key = self.statChoiceKey.currentData()
        self.rdiList.append(rdi)
        self.count += 1
        rdi.name = f"Stat{self.count}_{rdi.layerName()}"
        self.makeWaitLabel(rdi)
        # l = QtWidgets.QLabel(rdi.layerName())
        # self.coll.layerBloc.addWidget(l)
        # rdi.waitLabel = l
        # rdi.timer = rdiTimer(rdi)
        # rdi.timer.setLabel(l, deb=l.text())
        # rdi.timer.setMaxPts(10)
        # rdi.timer.start(200)
        return rdi
    
    def getEnjeuSelected(self):
        lst = []
        for e in self.dock.enjeu.getEltsSelected():
            if e.stat.checked:
                lst.append(e)
        return lst
    
    def launchAvailable(self):
        lst = self.getEnjeuSelected()
        if len(lst)>0:
            self.validButton.setEnabled(True)
        else:
            self.validButton.setEnabled(False)
    
    def launchStat(self):
        elt = self.statChoice.currentData()
        aleaList = self.dock.alea.getEltsSelected()
        enjeuList = self.getEnjeuSelected()
        if elt!=None:
            rdi = self.addRdi(elt)
            self.dock.psql.getExtractEnjeu(enjeu=rdi, aleaList=aleaList, callback=self.generateLayer)    
        else:
            elt = {'name':"Global"}
            rdi = self.addRdi(elt)
            if len(aleaList)>0:
                self.psql.getFusionAlea(rdi=rdi, aleaList=aleaList, callback=self.generateLayer)
            else:
                self.psql.getFusionEnjeu(rdi=rdi, enjeuList=enjeuList, callback=self.generateLayer)
    
    def generateLayer(self, table, rdi):
        if table!=rdi.tableName():
            args = (table, None)
        else:
            args = (rdi.tableName(), rdi.schemaName())
        full = self.psql.full(*args)
        geom = self.psql.getGeometryField(*args)
        geomST = quote(geom) 
        field = [rdi.key]
        if rdi.key!=geom:
            if self.psql.isGeomPolygon(*args) and self.psql.context['correction']:
                geomST = f"st_buffer({geomST}, 0)"
            field.append(f"st_union({geomST}) as {quote(geom)}")
        fields = self.psql.getNonGeomFields(*args)
        fields = map(lambda x: f"array_agg(distinct {quote(x)}) as {quote(x)}", [f for f in fields if f!=rdi.key])
        field.extend(fields)
        field_string = ", ".join(field)
        sql = f"SELECT {field_string} FROM {full} GROUP BY {rdi.key}"
        tmp = makeUniqueIdent()
        requete = f"stat_{tmp}"
        q = tmpQuery(self.psql, sql)
        q.setOrigin(*args)
        q.addBD(enjeu="", alea=table, requete=requete)
        description = f"tt"
        task = mGlobalTask(description)
        task.addSubTask(q)
        task.setCallback(self.generateStat, rdi, q.table, tmp)
        self.dock.psql.manager.add(task)
  
    def generateStat(self, rdi, table, tmp):
        description = f"Statistiques_{tmp}"
        task = mGlobalTask(description)
        q = statQuery(self)
        q.setEnv(table, rdi.key)
        q.description = f""
        task.addSubTask(q)
        task.setCallback(self.affStat, rdi, table)
        self.dock.psql.manager.add(task)
    
    def affStat(self, rdi, table):
        rdi.timer.stop()
        self.coll.layerBloc.removeWidget(rdi.waitLabel)
        rdi.waitLabel.setParent(None)
        del rdi.waitLabel
        self.coll.add(rdi, True)
        rdi.tableResult = table
        rdi.cb.setChecked(True)

        
                
    def initData(self):
        self.count = 0
        self.rdiList = []
        self.operation = [
                {
                    'code': 'max',
                    'label': 'Maximum',
                    'type': 'all',
                },
                {
                    'code': 'min',
                    'label': 'Minimum',
                    'type': 'all',
                },
                {
                    'code': 'sum',
                    'label': 'Somme',
                    'type': 'number',
                },
                {
                    'code': 'avg',
                    'label': 'Moyenne',
                    'type': 'number',
                },
            ]
    

class rdiStatLayer(QObject):

    dataChanged = pyqtSignal()
    heightChanged = pyqtSignal()
    
    def __init__(self, rdi, layout, dock=None):
        QObject.__init__(self)
        self.rdi = rdi
        if dock!=None:
            self.dock = dock
            self.noCB = True
        else:
            self.dock = rdi.coll.dock
            self.noCB = False
        self.layout = layout
        self.makeCount()
        self.getMemorize()
        self.draw()
        
    def makeCount(self):
        self.checked = True
        self.args = (self.rdi.tableName(), self.rdi.schemaName())
        self.fieldsData = self.dock.psql.getColumnsTableAll(*self.args)
        self.defStat = self.dock.stat.operation[0]['code']
        self.imageFolder = self.dock.imageFolder
        geomtype = self.rdi.topAttr('type')
        if geomtype:
            self.isPoly = geomtype=='polygon'
            self.isLine = geomtype=='line'
        else:
            self.isPoly = self.dock.psql.isGeomPolygon(*self.args)
            self.isLine = self.dock.psql.isGeomLinestring(*self.args)
    
    def json(self, txt):
        try:
            js = json.loads(txt)
        except:
            js = {}
        return js  
        
    def getMemorize(self):
        self.stats = {}
        self.count = 0
        js = self.json(self.rdi.getValueFromMeta('stats'))
        for k,v in js.items():
            self.count += 1
            id = f"stat_{self.count}"
            e = {'field':v['field'], 'stat':v['stat'], 'id':id}
            self.stats[id] = e
        
    def addStat(self):
        self.count += 1
        id = f"stat_{self.count}"
        e = {'field':None, 'stat':None, 'id':id}
        self.stats[id] = e
        self.drawStat(e)
        self.dataChanged.emit()
        self.heightChanged.emit()
        
    def delStat(self, butt):
        clearLayout(butt.hbox)
        del butt.hbox
        del self.stats[butt.id]
        self.dataChanged.emit()
        self.heightChanged.emit()
        
    def draw(self):
        hbox = QtWidgets.QHBoxLayout()
        self.layout.addLayout(hbox)
        if not self.noCB:
            self.cb = QtWidgets.QCheckBox(self.rdi.layerName())
            self.cb.setChecked(True)
            hbox.addWidget(self.cb)
            self.cb.stateChanged.connect(self.activate)
        self.fieldsWidget = QtWidgets.QWidget()
        lay = QtWidgets.QVBoxLayout(self.fieldsWidget)
        lay.setSpacing(0)
        lay.setContentsMargins(20,0,10,10)
        self.layout.addWidget(self.fieldsWidget)

        hbox = QtWidgets.QHBoxLayout()
        lay.addLayout(hbox)
        txt = "Auto: nb d'entités"
        if self.isPoly:
            txt += ", aire totale"
        if self.isLine:
            txt += ", longueur totale"
        l = QtWidgets.QLabel(txt)
        l.setWordWrap(True)
        font = QtGui.QFont()
        font.setItalic(True)
        l.setFont(font)
        hbox.addWidget(l)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "add.png"))
        b = QtWidgets.QPushButton(ico, "")
        b.setMaximumWidth(30)
        hbox.addWidget(b)
        b.clicked.connect(self.addStat)
        self.addButt = b
        for k,v in self.stats.items():
            self.drawStat(v)
            
        if not self.noCB and not self.checked:
            self.cb.setChecked(False)
            
    def drawStat(self, e):
        hbox = QtWidgets.QHBoxLayout()
        self.fieldsWidget.layout().addLayout(hbox)
        statFound = None
        combo = QtWidgets.QComboBox()
        combo.addItem("", "")
        for o in self.dock.stat.operation:
            combo.addItem(o['label'], o)
            if o['code']==e['stat']:
                statFound = o['label']
        hbox.addWidget(combo)
        combo.currentIndexChanged.connect(partial(self.setStatValue, e, combo))
        comboStat = combo
        comboStat.e = e
        combo = QtWidgets.QComboBox()
        hbox.addWidget(combo)
        combo.currentIndexChanged.connect(partial(self.setFieldValue, e, combo))
        comboStat.currentIndexChanged.connect(partial(self.adapt, comboStat, combo))
        if statFound!=None:
            comboStat.setCurrentText(statFound)
        else:
            self.adapt(comboStat, combo)

        ico = QtGui.QIcon(os.path.join(self.imageFolder, "delete.png"))
        b = QtWidgets.QPushButton(ico, "")
        b.setMaximumWidth(30)
        hbox.addWidget(b)
        b.hbox = hbox
        b.clicked.connect(partial(self.delStat, b))
        b.id = e['id']
      
    def activate(self):
        self.checked = self.cb.isChecked()
        if self.checked:
            self.addButt.show()
            self.fieldsWidget.show()
        else:
            self.addButt.hide()
            self.fieldsWidget.hide()
        self.heightChanged.emit()
        
    def setFieldValue(self, e, combo):
        e['field'] = combo.currentText()
        e['dataField'] = combo.currentData()
        self.dataChanged.emit()
        
    def setStatValue(self, e, combo):
        o = combo.currentData()
        if o!="":
            e['stat'] = o['code']
        else:
            e['stat'] = ""
        self.dataChanged.emit()
        
    def adapt(self, comboStat, comboField):
        o = comboStat.currentData()
        comboField.blockSignals(True)
        while comboField.count():
            comboField.removeItem(0)
        comboField.addItem("", "")
        comboField.blockSignals(False)
        if o!="":
            for fd in self.fieldsData:
                f = fd.get('column_name')
                if o['type']!='number' or fd.get('numeric_precision')!=None:
                    comboField.addItem(f, fd)
            if comboStat.e['field']!=None:
                comboField.setCurrentText(comboStat.e['field'])
 
    def setMemorize(self):
        js = {}
        for k,v in self.stats.items():
            js[k] = {'field':v['field'], 'stat':v['stat']}
        if js!={}:    
            return f"'{json.dumps(js)}'"
        else:
            return 'NULL'

    def getHeight(self):
        nbl = 1
        if self.checked:
            nbl += len(self.stats.keys()) + 1
        return nbl*25

    



class statQuery(abstractQuery):
    def __init__(self, stat):
        abstractQuery.__init__(self, stat.psql)
        self.stat = stat
        self.psql = stat.psql
        
    def setEnv(self, table, key):
        self.table = table
        self.key = key
    
    def makeColumnName(self, name):
        l = [" ", "_"]
        name = "".join([c for c in name if c.isalnum() or c in l])
        name = name.replace(" ","_")
        self.name = name
    
    def addStat(self, stat, field, type):
        functionList = ['length', 'area']
        self.c += 1
        statColU = f"{self.statCol}{self.c}_{self.name}_{stat}"
        if field!=None:
            if stat not in functionList:
                statColU += f"_{field}"
            col = quote(field)
        else:
            col = "*"
        func = f"{self.fullE}.{col}"
        if stat in functionList:
            if stat=='area':
                func = f"st_buffer({func},0)"
            func = f"st_{stat}(st_union({func}))"
        else:
            func = f"{stat}({func})"   
        self.get.append(f"{func} AS {statColU}")
        self.la.append(f"ADD COLUMN {statColU} {type}")
        self.lf.append(f"{statColU}")

    def makeCreate(self):
        lj = []
        get = []
        args = (self.table, None)
        full = self.psql.full(*args)
        geom = self.psql.getGeometryField(*args)
        field = self.psql.getColumnsTable(*args)
        
        self.statCol = 'stat'
        statColTotal = 'TOTAL'
        while self.statCol in field:
            self.statCol += '_'
        
        sql = ""
        self.c = 0
        sql += f" ALTER TABLE {full} ADD COLUMN {self.statCol} varchar;"
        sql += f" INSERT INTO {full} ({self.statCol}) VALUES ('{statColTotal}');"    
        
        for enjeu in self.stat.dock.enjeu.getEltsSelected():
            if enjeu.stat.checked:
                lj = []
                self.get = []
                self.lf = []
                self.la = []
                self.get.append(f"{full}.{quote(self.key)}")
                self.makeColumnName(enjeu.layerName())
                args = (enjeu.tableName(),enjeu.schemaName())
                if enjeu.tableWaiting!=enjeu.tableName():
                    args = (enjeu.tableWaiting, None)
                fullE = self.psql.full(*args)
                self.fullE = fullE
                geomE = self.psql.getGeometryField(*args)
                lj.append(f"{fullE} ON ST_Intersects({full}.{quote(geom)}, {fullE}.{quote(geomE)})")

                self.addStat('count', None, 'integer')
                if enjeu.stat.isPoly:
                    self.addStat('area', geomE, 'real')
                if enjeu.stat.isLine:
                    self.addStat('length', geomE, 'real')
                for k,v in enjeu.stat.stats.items():
                    if v['stat']!=None and v['field']!=None:
                        
                        type = 'character varying'
                        if (v['dataField'].get('numeric_precision')!=None):
                            type = 'real'
                        self.addStat(v['stat'], v['field'], type)
                
                joins = ", ".join(self.la)
                sql += f" ALTER TABLE {full} {joins};"
                
                andPart = ""
                try:
                    part = enjeu.part
                    if self.psql.hasColumn(part, *args):
                        andPart = f"AND {fullE}.{part}=1"
                except:
                    # self.error.append(sys.exc_info())
                    pass
                
                gets = ", ".join(self.get)
                lj.insert(0, f"SELECT {gets} from {full}")
                joins = " LEFT JOIN ".join(lj)
                sqls = f" {joins} WHERE {full}.{self.statCol} IS NULL {andPart} GROUP BY ROLLUP({full}.{quote(self.key)})" 
                fields = ", ".join(map(lambda x: f"{x}=stat.{x}", self.lf))
                sql += f" UPDATE {full} SET {fields} FROM ({sqls}) AS stat"
                sql += f" WHERE {full}.{quote(self.key)}=stat.{quote(self.key)} OR (stat.{quote(self.key)} IS NULL AND {full}.{self.statCol}='{statColTotal}') ;"
                
                
        return sql
    
    def prepare(self):
        # self.error.append(self.makeCreate())
        pass





