# -*- coding: utf-8 -*-

import os, sys
from functools import partial

from qgis.gui import *
from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic, QtXml
from PyQt5.QtCore import *
from qgis.utils import iface
from qgis import processing

from .rdi_layer import rdiLayer
from .rdi_meta import rdiMeta

class rdiAtlas(QObject):

    def __init__(self, parent):
        QObject.__init__(self)
        self.dock = parent
        self.working = False
        self.label = None
        self.meta = rdiMeta()
        self.folder = self.meta.readIni("autoGenFolder")
        self.initData()
        QgsProject.instance().layoutManager().layoutAdded.connect(self.actuPrinter)
       
    def make(self, layout):
        wl = QtWidgets.QWidget()
        layout.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        hbox.setContentsMargins(10,10,0,0)
        l = QtWidgets.QLabel("Dans ")
        l.setFixedSize(35,15)
        hbox.addWidget(l)
        ico = QtGui.QIcon(os.path.join(self.dock.imageFolder, "folder.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setToolTip("Choisir le dossier cible")
        butt.setMaximumWidth(30)
        hbox.addWidget(butt)
        l = QtWidgets.QLabel("")
        l.setMaximumWidth(250)
        hbox.addWidget(l)
        self.label = l
        butt.clicked.connect(self.setFolder)
        
        wl = QtWidgets.QWidget()
        layout.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        hbox.setContentsMargins(30,0,0,0)
        cb = QtWidgets.QCheckBox(f"{self.dock.param.get('alea')} sélectionnés uniquement")
        cb.setChecked(True)
        hbox.addWidget(cb)
        self.aleaCB = cb
        wl = QtWidgets.QWidget()
        layout.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        hbox.setContentsMargins(30,0,0,0)
        cb = QtWidgets.QCheckBox(f"{self.dock.param.get('enjeu')} sélectionnés uniquement")
        cb.setChecked(True)
        hbox.addWidget(cb)
        self.enjeuCB = cb
        
        wl = QtWidgets.QWidget()
        layout.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        hbox.setContentsMargins(10,10,0,5)
        l = QtWidgets.QLabel("Mode de création")
        l.setFixedSize(85,15)
        hbox.addWidget(l)
        hbox.setAlignment(l, Qt.AlignLeft)
        combo = QtWidgets.QComboBox()
        hbox.addWidget(combo)
        hbox.setAlignment(combo, Qt.AlignLeft)
        for k, v in self.options.items():
            combo.addItem(v, k)
        hbox.addWidget(combo)
        combo.currentIndexChanged.connect(self.actuMode)
        self.mode = combo
        l = QtWidgets.QLabel(" ")
        hbox.addWidget(l)
        
        
        # GPKG
        wg = QtWidgets.QWidget()
        layout.addWidget(wg)
        form = QtWidgets.QFormLayout(wg)
        form.setContentsMargins(30,10,10,10)
        self.gpkgOptions = wg
        wg.hide()
        l = QtWidgets.QLabel("Regroupement")
        combo = QtWidgets.QComboBox()
        form.addRow(l,combo)
        for k, v in self.group.items():
            combo.addItem(v, k)
        self.regroup = combo
        cb = QtWidgets.QCheckBox(f"Intégrer le style")
        self.style = cb
        form.addRow("", cb)
        
        # PDF
        wg = QtWidgets.QWidget()
        layout.addWidget(wg)
        form = QtWidgets.QFormLayout(wg)
        form.setContentsMargins(30,10,10,10)
        self.pdfOptions = wg
        wg.hide()
        l = QtWidgets.QLabel("Mise en page")
        combo = QtWidgets.QComboBox()
        form.addRow(l,combo)
        self.printer = combo
        self.actuPrinter()
        l = QtWidgets.QLabel("Atlas")
        combo = QtWidgets.QComboBox()
        combo.addItem("Aucun", None)
        ico = QtGui.QIcon(os.path.join(self.dock.imageFolder, 'stats.png'))
        for elt in self.dock.psql.getTableAdmin('stats', True):
            rdi = rdiLayer(elt)
            table = rdi.tableName()
            if table:
                name = rdi.layerName()
                combo.addItem(ico, name, elt)
        self.atlasChoice = combo
        form.addRow(l, combo)
        cb = QtWidgets.QCheckBox(f"Séparer les {self.dock.param.get('enjeu')}")
        self.split = cb
        form.addRow("", cb)
        

        wl = QtWidgets.QWidget()
        layout.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout(wl)
        ico = QtGui.QIcon(os.path.join(self.dock.imageFolder, "execute.png"))
        butt = QtWidgets.QPushButton(ico, " Générer")
        butt.setToolTip("Lancer la génération de cartes")
        butt.setMaximumWidth(100)
        hbox.addWidget(butt)
        butt.clicked.connect(self.launch)
        self.butt = butt
        
        
        wg = QtWidgets.QWidget()
        layout.addWidget(wg)
        vbox = QtWidgets.QVBoxLayout(wg)
        self.logs = wg
        wg.hide()
        l = QtWidgets.QLabel("Génération en cours\nVeuillez patienter\nNe pas interagir avec Qgis")
        vbox.addWidget(l)
        l = QtWidgets.QLabel("")
        vbox.addWidget(l)
        self.progress = l

        self.actuMode()
        self.setReady()
    
    def actuMode(self):
        mode = self.mode.currentData()
        if mode=='gpkg':
            self.pdfOptions.hide()
            self.gpkgOptions.show()
        if mode=='pdf':
            self.pdfOptions.show()
            self.gpkgOptions.hide()
    
    def actuPrinter(self):
        while self.printer.count():
            self.printer.removeItem(0)
        self.printer.addItem("Auto", None)
        list = sorted([cView.name() for cView in QgsProject.instance().layoutManager().printLayouts()], key=str.lower)
        for s in list:
            self.printer.addItem(s, s)
        
    def setReady(self):
        bool = (self.folder is not None and self.folder!="" and not self.working)
        try:
            self.butt.setEnabled(bool)
            name = self.folder
            if len(name)>40:
                name = name[:10] +"[...]"+ name[-25:]
            self.label.setText(name)
            self.label.setToolTip(self.folder)
            self.meta.writeIni("autoGenFolder", self.folder)
        except:
            pass
        
    def setFolder(self):
        dialog = QtGui.QFileDialog()
        dialog.setFileMode(QtGui.QFileDialog.Directory)
        dialog.setOption(QtGui.QFileDialog.ShowDirsOnly, True)
        target = dialog.getExistingDirectory(None, 'Choisir un emplacement')
        self.folder = target
        self.setReady()
    
    def initProgress(self):
        self.tot = self.nb
        if self.tot>0:
            self.part = 100/self.tot
        else:
            self.part = 100
        self.setProgress(False)
    
    def setProgress(self, inc=True):
        if inc:
            self.nb -= 1
            if self.nb<0:
                self.nb = 0
        p = int(self.part*(self.tot-self.nb))
        self.progress.setText(f"Avancement : {p}%")

    def startWorking(self):
        self.working = True
        self.butt.setEnabled(False)
        self.logs.show()

    def stopWorking(self):
        self.working = False
        self.butt.setEnabled(True)
        self.logs.hide()
        self.land()
    
    def needClear(self):
        mode = self.mode.currentData()
        if mode=='gpkg' and self.style.isChecked():
            return True
        if mode=='pdf':
            return True
 
    def aleaParallax(self):
        mode = self.mode.currentData()
        if self.mode.currentData()=='gpkg':
            return True
        if self.mode.currentData()=='pdf':
            return False
    
    def enjeuParallax(self):
        mode = self.mode.currentData()
        if self.mode.currentData()=='gpkg':
            return True
        if self.mode.currentData()=='pdf':
            if not self.split.isChecked():
                return True
            return False
            
    def confirm(self):
        qm = QtGui.QMessageBox
        rep = qm.question(None, "", f"Vous allez lancer la génération sur un grand nombre de fichiers : {self.nb}\nVoulez-vous continuer ?", qm.Yes | qm.No)
        if rep==qm.Yes:
            return True
        else:
            self.stopWorking()
    
    def launch(self):
        self.startWorking()
        self.aleaSelected = self.dock.alea.getEltsSelected()
        self.enjeuSelected = self.dock.enjeu.getEltsSelected()
        self.aleaMemo = self.dock.alea.getElts()
        if self.aleaCB.isChecked():
            self.aleaMemo = self.dock.alea.getEltsSelected()
        self.enjeuMemo = self.dock.enjeu.getElts()
        if self.enjeuCB.isChecked():
            self.enjeuMemo = self.dock.enjeu.getEltsSelected()
        self.nb = len(self.aleaMemo)*len(self.enjeuMemo)
        # if self.mode.currentData()=='gpkg':
            # self.nb += len(self.enjeuMemo)
        if not self.confirm():
            return
        self.initProgress()
        if self.needClear():
            for rdi in self.enjeuSelected:
                rdi.cb.setChecked(False)
                rdi.layer = None
            for rdi in self.aleaSelected:
                rdi.cb.setChecked(False)
                rdi.layer = None
        self.aleaData = {'list':self.aleaMemo.copy(), 'wait':len(self.aleaMemo), 'done':0, 'target':None}
        self.enjeuData = {}
        self.getAleas()
        
    def land(self):
        if self.needClear():
            for rdi in self.aleaSelected:
                rdi.cb.setChecked(True)
            for rdi in self.enjeuSelected:
                rdi.cb.setChecked(True)
    
    def getAleas(self):
        if self.aleaData['target'] is not None and self.needClear():
            self.remove(self.aleaData['target'])
        if len(self.aleaData['list'])==0:
            self.stopWorking()
            return
        if self.aleaParallax():
            list = self.aleaData['list'].copy()
            self.aleaData['list'] = []
            for alea in list:
                self.enjeuData[alea] = {'list':self.enjeuMemo.copy(), 'wait':len(self.enjeuMemo), 'done':0, 'target':None}
                self.getAlea(alea)
            return
        self.aleaData['target'] = self.aleaData['list'].pop()
        self.aleaData['target'].layer = None
        self.enjeuData[self.aleaData['target']] = {'list':self.enjeuMemo.copy(), 'wait':len(self.enjeuMemo), 'done':0, 'target':None}
        self.getAlea(self.aleaData['target'])
        
    def getAlea(self, alea):
        self.aleaData['done'] += 1
        if self.mode.currentData()=='pdf':
            self.dock.psql.getExtractAlea(alea=alea)
            elt = self.atlasChoice.currentData()
            if elt is not None:
                atlas = rdiLayer(elt)
                self.dock.psql.getExtractEnjeu(enjeu=atlas, aleaList=[alea], callback=self.setAtlas, args=(alea,))
            else:
                self.rect = alea.vlayer.extent()
                self.getEnjeux(alea)
        if self.mode.currentData()=='gpkg':
            vlayer, geomType = self.dock.psql.getLayer(alea.tableName(), alea.layerName(), alea.schemaName())
            if self.regroup.currentData()=="all":
                file = self.getFile(ext="gpkg")
            else:            
                file = self.getFile(alea=alea, ext="gpkg")
            self.saveGPKG(alea, vlayer, file, alea.layerName()+";")
            self.getEnjeux(alea)
    
    def setAtlas(self, table, atlas, alea):
        self.atlasTable = table
        self.atlasRdi = atlas
        self.getEnjeux(alea)

    def getEnjeux(self, alea):
        if self.enjeuData[alea]['target'] is not None and self.needClear():
            self.remove(self.enjeuData[alea]['target'])
        if len(self.enjeuData[alea]['list'])==0:
            if not self.aleaParallax():    
                self.getAleas()
            else:
                if self.aleaData['done']>=self.aleaData['wait']:
                    self.getAleas()
            return
        if self.enjeuParallax():
            list = self.enjeuData[alea]['list'].copy()
            self.enjeuData[alea]['list'] = []
            for enjeu in list:
                self.getEnjeu(alea, enjeu)
            return
        self.enjeuData[alea]['target'] = self.enjeuData[alea]['list'].pop()
        self.enjeuData[alea]['target'].layer = None
        self.getEnjeu(alea, self.enjeuData[alea]['target'])
    
    def getEnjeu(self, alea, enjeu):
        self.enjeuData[alea]['done'] += 1
        self.dock.psql.getExtractEnjeu(enjeu=enjeu, aleaList=[alea], callback=self.setExtract, args=(alea, enjeu))

    def endEnjeu(self, alea):
        if not self.enjeuParallax():    
            self.getEnjeux(alea)
        else:
            if self.enjeuData[alea]['done']>=self.enjeuData[alea]['wait']:
                self.getEnjeux(alea)

    def setExtract(self, table, rdi, alea, enjeu):
        self.setProgress()
        vlayer, geomType = self.dock.psql.getLayer(table, rdi.layerName())
        if self.mode.currentData()=='gpkg':
            name = enjeu.layerName()
            if self.regroup.currentData()=='one':
                file = self.getFile(alea=alea, enjeu=enjeu, ext="gpkg")
                self.saveGPKG(rdi, vlayer, file)
            else:
                file = self.getFile(alea=alea, ext="gpkg")
                if self.regroup.currentData()=='all':
                    name = alea.layerName() + ":" + name
                    file = self.getFile(ext="gpkg")
                self.saveGPKG(rdi, vlayer, file, name)
            self.endEnjeu(alea)
            
        if self.mode.currentData()=='pdf':
            if self.enjeuParallax():
                if self.enjeuData[alea]['done']>=self.enjeuData[alea]['wait']:
                    file = self.getFile(alea=alea, ext="pdf")
                    rdi.affLayer(table, callback=self.savePDF, args=(file,))
                    self.endEnjeu(alea)
                else:
                    rdi.affLayer(table)
            else:
                file = self.getFile(alea=alea, enjeu=enjeu, ext="pdf")
                rdi.affLayer(table, callback=self.savePDF, args=(file,))
                self.endEnjeu(alea)                

    def saveGPKG(self,rdi, vlayer, file, name=None):
        rdi.affVLayer(vlayer)
        if name is not None:
            vlayer.setName(name)
        alg = processing.run("native:package", {
            'LAYERS': [vlayer],
            'OVARWRITE': True,
            'SAVE_STYLES': self.style.isChecked(),
            'OUTPUT': file
        })       

    def savePDF(self, file):
        project = QgsProject.instance()
        manager = project.layoutManager()
        
        layoutName = self.printer.currentData()
        if layoutName is None:
            layout = QgsPrintLayout(project)
            layout.initializeDefaults()
            map = QgsLayoutItemMap(layout)
            map.setRect(20, 20, 20, 20)
            map.setExtent(iface.mapCanvas().extent())
            map.attemptMove(QgsLayoutPoint(40, 25, QgsUnitTypes.LayoutMillimeters))
            map.attemptResize(QgsLayoutSize(250, 180, QgsUnitTypes.LayoutMillimeters))
            map.refresh()
            layout.addLayoutItem(map)
            legend = QgsLayoutItemLegend(layout)
            legend.setTitle("Légende")
            layout.addLayoutItem(legend)
        else:
            layout = manager.layoutByName(layoutName)
            map = layout.referenceMap()
            map.refresh()
            legend = None
            for l in layout.items(): 
                if isinstance(l, QgsLayoutItemLegend):
                    legend = l
        
        if legend is not None:
            legend.setAutoUpdateModel(False)
            legend.refresh()
            legend.setAutoUpdateModel(True)
        
        exporter = QgsLayoutExporter(layout)
        if self.atlasChoice.currentData() is not None:
            map.setAtlasDriven(True)
            map.setAtlasScalingMode(QgsLayoutItemMap.Auto)
            vlayer, geomType = self.dock.psql.getLayer(self.atlasTable, self.atlasRdi.layerName())
            atlas = layout.atlas()
            atlas.setCoverageLayer(vlayer)
            atlas.setEnabled(True)
            exporter.exportToPdf(atlas, file, QgsLayoutExporter.PdfExportSettings())
        else:
            map.zoomToExtent(self.rect)
            exporter.exportToPdf(file, QgsLayoutExporter.PdfExportSettings())

    def getFile(self, alea=None, enjeu=None, ext=""):
        name = ""
        if ext!="" and ext[:1]!=".":
            ext = "."+str(ext)
        if alea is None and enjeu is None:
            name = "rdi"
        if alea is not None:
            name = alea.layerName()
        if enjeu is not None:
            if name != "":
                name += "_"
            name += enjeu.layerName()
        return os.path.join(self.folder, self.getName(name)+str(ext))
    
    def getName(self, name):
        name = name.replace(" ","_")
        name = "".join(x for x in name if x.isalnum() or x=='_')
        return name

    def remove(self, rdi):
        rdi.layer = self.dock.dyn.removeLayer(rdi)


    def initData(self):
        self.group = {
            'one': "Aucun",
            'alea': "Par aléa",
            'all': "Tout en un",
        }
        self.options = {
            'gpkg': "GeoPackage",
            'pdf': "Cartes PDF"
        }
        



   