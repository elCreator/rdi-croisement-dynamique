# -*- coding: utf-8 -*-

import os, sys, configparser, shutil, zipfile
from functools import partial

from qgis.gui import *
from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic, QtNetwork, QtXml, QtDesigner
from PyQt5.QtCore import *

class rdiMeta(QObject):

    def __init__(self):
        QObject.__init__(self)
        self.metadata = os.path.join(os.path.dirname(__file__), "metadata.txt")
        self.meta = None
        self.plugin = self.getPlugin()
        
    def getPlugin(self):
        path = os.path.dirname(__file__).replace("\\", "/")
        tt = path.split("/")
        doss = tt[len(tt)-1]
        return doss

    def getMetadata(self):
        if self.meta==None:
            self.meta = configparser.ConfigParser(allow_no_value=True)
            self.meta.optionxform = str
            if not os.path.exists(self.metadata):
                return
            self.meta.read(self.metadata)
            
    def getVersion(self):
        self.getMetadata()
        return self.meta.get('general', 'version')
        
    def getName(self):
        self.getMetadata()
        return self.meta.get('general', 'name')
        
    def readIni(self, key):
        s = QSettings() 
        s.beginGroup(self.plugin)
        value = s.value(key)
        s.endGroup()
        return value

    def writeIni(self, key, value):
        s = QSettings() 
        s.beginGroup(self.plugin)
        s.setValue(key, value)
        s.endGroup()






