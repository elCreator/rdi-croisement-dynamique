# -*- coding: utf-8 -*-

import os

from qgis.gui import *
from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtPrintSupport import QPrinter


FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'base.ui'))


class rdiNotice(QtWidgets.QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, parent=None):
        super(rdiNotice, self).__init__()
        self.setupUi(self)
        layout = QtWidgets.QVBoxLayout()
        layout.setAlignment(Qt.AlignTop)
        self.dockWidgetContents.setLayout(layout)
        self.setWindowTitle("RDI - Manuel d'utilisation")
        self.layout = layout
        self.parent = parent
        self.root = os.path.join(os.path.dirname(__file__), "help")
        self.imageFolder = os.path.join(os.path.dirname(__file__), "images")
        self.ressources = os.path.join(self.root, "ressources")
        self.affHTML()
        self.affNavButton()

    def affHTML(self):
        self.file = os.path.join(self.root, "notice.htm")
        self.url = QUrl.fromLocalFile(self.file)
        self.view = QWebView(self)
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        self.view.setFixedHeight(screen.height()-80)
        self.view.load(self.url)
        self.view.settings().setUserStyleSheetUrl(QUrl.fromLocalFile(os.path.join(self.ressources, "notice.css")))
        self.view.settings().setUserStyleSheetUrl(QUrl.fromLocalFile(os.path.join(self.ressources, "nav.css")))
        self.layout.addWidget(self.view)
    
    def affNavButton(self):
        hbox = QtWidgets.QHBoxLayout()
        l = QtWidgets.QLabel(" ")
        hbox.addWidget(l)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "browser.png"))
        butt = QtWidgets.QPushButton(ico, "   Afficher dans un navigateur")
        butt.setMaximumWidth(200)
        butt.clicked.connect(self.affNav)
        self.layout.addLayout(hbox)
        hbox.addWidget(butt)
        l = QtWidgets.QLabel(" ")
        hbox.addWidget(l)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "print.png"))
        butt = QtWidgets.QPushButton(ico, "   Imprimer en PDF")
        butt.setMaximumWidth(200)
        butt.clicked.connect(self.affPdf)
        self.layout.addLayout(hbox)
        hbox.addWidget(butt)
        l = QtWidgets.QLabel(" ")
        hbox.addWidget(l)
    
    def actuPage(self, tag=None):
        if tag and tag!="":
            self.tag = tag
            self.url.setFragment(tag)
            self.view.load(self.url)
    
    def actuLabel(self):
        self.label.setText(self.url.toString())
    
    def affNav(self):
        QtGui.QDesktopServices.openUrl(self.url)
        
    def affPdf(self):
        self.view.page().mainFrame().evaluateJavaScript("flat();")
        saveLocation = os.path.join(self.root, "RDI_notice.pdf");
        printer = QPrinter(QPrinter.HighResolution)
        printer.setOutputFileName(saveLocation)
        self.view.print(printer)
        QtGui.QDesktopServices.openUrl(QUrl.fromLocalFile(saveLocation))
        self.view.page().mainFrame().evaluateJavaScript("unflat();")
    
        
    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()
