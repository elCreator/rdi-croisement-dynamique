# -*- coding: utf-8 -*-

from qgis.core import *
from qgis.gui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import *
from PyQt5 import QtWidgets, QtGui
from qgis.utils import iface

from functools import partial

from .rdi_param import rdiParam
from .rdi_async import mGlobalTask, manageTasks, abstractQuery
from .rdi_tools import makeUniqueIdent, quote, rdiTimer, flashMsg, compress


class rdiPostgis(QObject):
    
    connected = pyqtSignal()
    connecting = pyqtSignal()
    
    def __init__(self, callback=None, label=None, password=None):
        QObject.__init__(self)
        self.actuParams()
        self.PGdb = self.param.get('db')
        self.PGschema = self.param.get('schema', 'CONN')
        if self.PGschema=="":
            self.PGschema = "public"
        self.PGhost = self.param.get('host')
        self.PGport = self.param.get('port')
        self.PGuser = self.param.get('user')
        self.PGpassword = self.param.get('password')
        if password:
            self.PGpassword = password
        logs = self.param.get('logs')
        self.manager = manageTasks(logs)
        self.checkLoginPassword(callback, label)
        
    def actuParams(self):
        self.param = rdiParam()
        self.initContext()
    
    def checkLoginPassword(self, callback=None, label=None):
        if self.PGpassword=="" and not self.param.get('savePassword'):
            self.connDialog = connDialog(self)
            self.connDialog.callback = callback
            self.connDialog.label = label
            self.connDialog.accepted.connect(self.setLoginPassword)
            self.connDialog.rejected.connect(self.setLoginPassword)
        else:
            self.switchConnect(callback, label)
    
    def setLoginPassword(self):
        self.PGuser = self.connDialog.login()
        self.PGpassword = self.connDialog.password()
        self.switchConnect(self.connDialog.callback, self.connDialog.label)
        
    def switchConnect(self, callback, label):
        if callback:
            self.asyncConnect(callback, label)
        else:
            self.connect()

    def connect(self):
        self.error = None
        try:
            self.db = self.newConn()
            if not self.db.open():
                self.error = "Database Error: %s" % self.db.lastError().text()
        except:
            self.error = "Aucun paramétrage de la connexion Postgis"
        self.connected.emit()
        
    def asyncConnect(self, callback, label):
        m = mGlobalTask("Tentative de connexion")
        m.task.setCallin(self.connect)
        m.setCallback(callback)
        if label:
            self.startTimerConn(m, label)
        self.manager.add(m)
        
    def startTimerConn(self, m, label):
        timer = rdiTimer(self.manager)
        timer.timeout.connect(self.connecting.emit)
        timer.setLabel(label, deb=label.text()+"Tentative de connexion en cours")
        timer.setMaxPts(10)
        timer.start(200)
        m.setCallback(self.endTimerConn, timer)
        
    def endTimerConn(self, timer):
        try:
            timer.stop()
            del timer
        except:
            pass
        
    def newConn(self, name=None, init=True):
        if not init:
            return QSqlDatabase.cloneDatabase(self.db, name)
        
        if name:
            new = QSqlDatabase.addDatabase("QPSQL", name)
        else:
            new = QSqlDatabase.addDatabase("QPSQL")
        new.setHostName(self.PGhost)
        new.setPort(int(self.PGport))
        new.setDatabaseName(self.PGdb)
        new.setUserName(self.PGuser)
        new.setPassword(self.PGpassword) 
        return new
    
    def setConn(self, db):
        db.setHostName(self.PGhost)
        db.setPort(int(self.PGport))
        db.setDatabaseName(self.PGdb)
        db.setUserName(self.PGuser)
        db.setPassword(self.PGpassword) 
        return db
        
    def delConn(self, name):
        QSqlDatabase.removeDatabase(name)
        

    def initContext(self):
        self.tmpPrefixe = self.param.get('tmpPrefixe')
        self.context = {}
        self.context['buffer'] = max(min(self.param.get('buffer'), self.param.get('bufferMax')), self.param.get('bufferMin'))
        self.context['simplify'] = min(self.param.get('simplify'), self.param.get('simplifyMax'))
        self.context['cutting'] = self.param.get('cutting')
        self.context['correction'] = self.param.get('correction')
        self.context['cluster'] = self.param.get('cluster')
        self.context['clusterField'] = self.param.get('clusterField')
        self.context['clusterPrecision'] = self.param.get('clusterPrecision')
    
    def getTable(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select * from " + self.full(table, schema)
        query = self.sqlSend(sql)
        return self.makeDictResult(query)
    
    def getTableCount(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select count(*) from " + self.full(table, schema)
        query = self.sqlSend(sql)
        return self.makeDictResult(query)
        
    def getSchemas(self):
        sql = f"select nspname from pg_catalog.pg_namespace " + \
            f" where nspname<>'information_schema' and nspname !~ E'^pg_'" + \
            f" order by nspname"
        return self.getList(sql)
        
    def getTables(self, schema, geomOnly=True):
        if geomOnly:
            sql = f"select c.tablename from pg_catalog.pg_tables c, information_schema.columns i " + \
                f" where c.schemaname='{schema}' " + \
                f" and i.table_name = c.tablename " + \
                f" and i.table_schema=c.schemaname " + \
                f" and i.udt_name='geometry' " + \
                f" and c.tablename not like '_tmp_%'" + \
                f" order by c.tablename "
        else:
            sql = f"select tablename from pg_catalog.pg_tables " + \
                f" where schemaname='{schema}'" + \
                f" and tablename not like '_tmp_%'" + \
                f" order by tablename"
        return self.getList(sql)

    def getList(self, sql):
        query = self.sqlSend(sql)
        list = []
        while query and query.next():
            list.append(query.value(0))
        return list  
      
    def isTable(self, table, schema=None):
        bool = False
        schema = self.getSchema(schema)
        sql = f"select * from pg_catalog.pg_tables where tablename='{table}' and schemaname='{schema}'"
        query = self.sqlSend(sql)
        if query and query.next():
            bool = True
        return bool     
        
    def getColumnsTableAll(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select * from information_schema.columns where table_name='{table}' and table_schema='{schema}'"
        query = self.sqlSend(sql)
        return self.makeDictResult(query) 
        
    def getColumnsTable(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select column_name from information_schema.columns where table_name='{table}' and table_schema='{schema}'"
        return self.getList(sql)
     
    def hasColumn(self, col, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select column_name from information_schema.columns where table_name='{table}' and table_schema='{schema}' and column_name='{col}'"
        query = self.sqlSend(sql)
        if query and query.next():
            return True
    
    def sqlSend(self, sql, flash=False):
        try:
            query = self.db.exec_(sql)
        except:
            if flash:
                flashMsg(f"Aucune connexion à la base de données")
            return
        if query.lastError().type()!=QSqlError.NoError:
            print(sql, query.lastError().text())
            if flash:
                flashMsg(f"{query.lastError().text()}", category='error')
        return query
        
    def sqlInsert(self, sql, flash=False):
        query = self.sqlSend(sql, flash)
        if query and query.next():
            return query.value(0)

    def makeDictResult(self, query):
        list = []
        if not query:
            return list
        rec = query.record()    
        while (query.next()):
            row = {}
            for i in range(rec.count()):
                row[rec.fieldName(i)] = query.value(i)
            list.append(row)
        return list  
        
    def getTableAdmin(self, code=None, compact=False):
        sql = f"select * from {self.admin('tableAdmin')}"
        if code:
            sql += f" where code='{code}'"
        sql += f" order by (case when tablename is null or tablename='' then 0 else 1 end), "
        if self.param.get('cbCompact') or compact:    
            sql += f" (case when parent is null then 0 else 1 end),"
        sql += f" \"order\", (case when name is null or name='' then tablename else name end), id"
        query = self.sqlSend(sql)
        return self.makeDictResult(query) 
        
    def getTableAdminElt(self, id):
        sql = sql = f"select * from {self.admin('tableAdmin')} where id={id}"
        query = self.sqlSend(sql)
        return self.makeDictResult(query) 
        
    def getTableAdminBlocs(self, mixed=False):
        if mixed:
            field = f"bloc"
        else:
            field = f"concat(code,'_',bloc)"
        sql = sql = f"select {field} from {self.admin('tableAdmin')} where bloc is not NULL and bloc<>'' group by 1"
        return self.getList(sql)
        
    def full(self, table, schema=None):
        schema = self.getSchema(schema)
        s = quote(table)
        if schema and schema!="":
            s = quote(schema) + "." + s
        return s
    
    def getGeomType(self, table, schema=None, geom=None):
        if not table:
            return []
        if not geom:
            geom = self.getGeometryField(table, schema)
        geomType = []
        sql = f"select geometryType({quote(geom)}) from {self.full(table, schema)} group by geometryType({quote(geom)})"
        query = self.sqlSend(sql)
        while query and query.next():
            geomType.append(query.value(0))
        if len(geomType)==0:
            sql = f"SELECT type FROM geometry_columns " + \
                f" WHERE f_table_schema = '{schema}' " + \
                f" AND f_table_name = '{table}' " + \
                f" and f_geometry_column = '{geom}';"
            query = self.sqlSend(sql)
            while query and query.next():
                geomType.append(query.value(0))
        return geomType    
        
    def getGeomTypeUnique(self, table, schema=None, geom=None, geomType=None):
        if not geomType:
            geomType = self.getGeomType(table, schema, geom)
        if len(geomType)==1:
            return geomType[0]
        
    def isGeomPoint(self, table, schema=None, geom=None, geomType=None):
        if not geomType:
            geomType = self.getGeomType(table, schema, geom)
        bool = False
        for t in geomType:
            if t and t.lower().find('point')>=0:
                bool = True
        return bool
    
    def isGeomPolygon(self, table, schema=None, geom=None, geomType=None):
        if not geomType:
            geomType = self.getGeomType(table, schema, geom)
        bool = False
        for t in geomType:
            if t and t.lower().find('polygon')>=0:
                bool = True
        return bool
        
    def isGeomLinestring(self, table, schema=None, geom=None, geomType=None):
        if not geomType:
            geomType = self.getGeomType(table, schema, geom)
        bool = False
        for t in geomType:
            if t and t.lower().find('linestring')>=0:
                bool = True
        return bool
    
    def isMultiGeom(self, table, schema=None, geom=None, geomType=None):
        if not geomType:
            geomType = self.getGeomType(table, schema, geom)
        bool = False
        for t in geomType:
            if t and t.lower().find('multi')>=0:
                bool = True
        return bool
    
    def needDump(self, table, schema=None, geom=None):
        bool = self.isGeomPoint(table, schema, geom)
        if bool:
            bool = self.isMultiGeom(table, schema, geom)
        return bool
    
    def getLayer(self, tableName, layerName, schema=None, cond=''):
        schema = self.getSchema(schema)
        if not self.isTable(tableName, schema):
            print('no table', tableName, schema)
            return None, None
        uri = QgsDataSourceUri()
        uri.setConnection(self.PGhost, str(self.PGport), self.PGdb, self.PGuser, self.PGpassword)
        geom = self.getGeometryField(tableName, schema)
        pkey = self.getPrimaryKey(tableName, schema)
        geomType = self.getGeomType(tableName, schema, geom)
        uri.setDataSource(schema, tableName, geom, cond, pkey)
        vlayer = QgsVectorLayer(uri.uri(), layerName, "postgres")
        if vlayer.isValid():
            return vlayer, geomType
        else:
            print('invalid', tableName, schema, geom, pkey)
            return None, None

    
    
    def getSchema(self, schema=None):
        if not schema or schema=="":
            schema = self.PGschema
        return schema
    
    def getPrimaryKey(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select " + \
			f"		a.attname as column_name " + \
			f"	from " + \
			f"		pg_class t, " + \
			f"		pg_class i, " + \
			f"		pg_index ix, " + \
			f"		pg_attribute a, " + \
			f"		pg_catalog.pg_tables c " + \
			f"	where " + \
			f"		t.oid = ix.indrelid " + \
			f"		and i.oid = ix.indexrelid " + \
			f"		and a.attrelid = t.oid " + \
			f"		and a.attnum = ANY(ix.indkey) " + \
			f"		and t.relkind = 'r' " + \
			f"		and ix.indisprimary = true " + \
			f"		and t.relname = '{table}' " + \
			f"		and c.tablename = t.relname " + \
			f"		and c.schemaname = '{schema}' " + \
			f"	order by " + \
			f"		t.relname, " + \
			f"		i.relname;"
        query = self.sqlSend(sql)
        if query and query.next():
            return str(query.value(0))
     
    def getSequence(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select " + \
            f"		s.sequence_name " + \
            f"	from " + \
            f"		information_schema.columns c, " + \
            f"		information_schema.sequences s " + \
            f"	where " + \
            f"		c.table_catalog = '{self.PGdb}' " + \
            f"		and c.table_name = '{table}' " + \
            f"		and c.table_schema = '{schema}' " + \
            f"		and s.sequence_catalog = table_catalog " + \
            f"		and s.sequence_schema = table_schema " + \
            f"		and c.column_default like '%'||sequence_name||'%' ;"
        query = self.sqlSend(sql)
        if query and query.next():
            return str(query.value(0))
        
    def hasIndex(self, col, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select " + \
            f"		t.relname as table_name, " + \
            f"		i.relname as index_name, " + \
            f"		a.attname as column_name " + \
            f"  from " + \
            f"      pg_class t, " + \
            f"      pg_class i, " + \
            f"      pg_index ix, " + \
            f"      pg_attribute a, " + \
            f"      pg_catalog.pg_tables c " + \
            f"  where " + \
            f"      t.oid = ix.indrelid " + \
            f"      and i.oid = ix.indexrelid " + \
            f"      and a.attrelid = t.oid " + \
            f"      and a.attnum = ANY(ix.indkey) " + \
            f"      and t.relkind = 'r' " + \
            f"      and c.tablename = t.relname " + \
            f"      and c.tablename = '{table}' " + \
            f"      and c.schemaname = '{schema}' " + \
            f"      and a.attname = '{col}' " + \
            f"  order by " + \
            f"      t.relname, " + \
            f"      i.relname; "
        query = self.sqlSend(sql)
        if query and query.next():
            return True
            
    def makeSpatialIndex(self, table, schema=None, geom=None):
        schema = self.getSchema(schema)
        if not geom:
            geom = self.getGeometryField(table, schema)
        col = f"{table}_geom"
        sql = f"DROP INDEX IF EXISTS {quote(col)} CASCADE; CREATE INDEX {quote(col)} ON {self.full(table, schema)} USING gist({quote(geom)});"
        self.db.exec_(sql)

    def isValidGeos(self, table, schema=None, geom=None):
        schema = self.getSchema(schema)
        if not geom:
            geom = self.getGeometryField(table, schema)
        sql = f"SELECT st_isValid({quote(geom)}) FROM {self.full(table, schema)} WHERE st_isValid({quote(geom)})='f'"
        query = self.sqlSend(sql)
        if query and query.next():
            return False
        else:
            return True
            
    def makeValidGeos(self, table, schema=None, geom=None, type=None):
        schema = self.getSchema(schema)
        if not geom:
            geom = self.getGeometryField(table, schema)
        geomST = f"st_makeValid({quote(geom)})"
        if not type:
            geomType = self.psql.getGeomType(table, schema, geom)
            if self.psql.isGeomPoint(table, schema, geomType=geomType):
                type = "point"
            if self.psql.isGeomLinestring(table, schema, geomType=geomType):
                type = "line"
            if self.psql.isGeomPolygon(table, schema, geomType=geomType):
                type = "polygon"
        if type:
            ctype = {'point':1, 'line':2, 'polygon':3}
            ntype = ctype[type]
            geomST = f"st_collectionExtract({geomST}, {ntype})"
        sql = f"UPDATE {self.full(table, schema)} SET {quote(geom)}={geomST})"
        query = self.sqlSend(sql)    

    def getGeometryField(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select column_name from information_schema.columns " + \
            f"	where table_name = '{table}' " + \
            f"	and udt_name='geometry' " + \
            f"	and table_schema='{schema}' "
        query = self.sqlSend(sql)
        if query and query.next():
            return str(query.value(0))
        
    def getNonGeomFields(self, table, schema=None):
        schema = self.getSchema(schema)
        sql = f"select column_name from information_schema.columns " + \
            f"	where table_name = '{table}' " + \
            f"	and udt_name<>'geometry' " + \
            f"	and table_schema='{schema}' "
        query = self.sqlSend(sql)
        list = []
        while query and query.next():
            list.append(query.value(0))
        return list
    
    def getSrid(self, table, schema=None, geom=None):
        if not geom:
            geom = self.getGeometryField(table, schema)
        sql = f"SELECT st_srid({quote(geom)}) FROM {self.full(table, schema)} limit 1"
        query = self.sqlSend(sql)
        if query and query.next():
            return str(query.value(0))
        
    def proj(self, table, srid, schema=None, geom=None):
        if not geom:
            geom = self.getGeometryField(table, schema)
        st_geom = self.full(table, schema) + "." + quote(geom)
        srid_ori = self.getSrid(table, schema)
        if srid!=srid_ori:
            st_geom = f"st_transform({st_geom}, {srid})"
        return st_geom


    def admin(self, code):
        if code=='tableTmp':
            return self.full(self.param.get(code), self.param.get('schema', 'CONN'))
        else:
            return self.full(self.param.get(code), self.param.get('schema', 'ADMIN'))
 
    
    def checkColumnName(self, table, schema, name):
        fields = self.getColumnsTable(table, schema)
        trouve = False
        for f in fields:
            if f==name:
                trouve = True
                break
        return trouve
        
    def getNewColumnName(self, table, schema, name):
        while self.checkColumnName(table, schema, name):
            name += "_"
        return name
    

    def getHull(self, rdi):
        requete = f"hull"
        rdiAttr = rdi.getAttr() 
        rdiArgs = (rdiAttr['table'], rdiAttr['schema'])
        geom = self.getGeometryField(*rdiArgs)
        geomST = f"{quote(geom)}"
        if self.isGeomPolygon(*rdiArgs) and self.context['correction']:
            geomST = f"st_buffer(st_collectionExtract(st_makeValid({geomST}), 3), 0)"
            requete += "_corr"
        rdiTotal = self.full(*rdiArgs)
        sqlExtract = f"SELECT ST_ConvexHull(ST_Union({geomST})) as geom FROM {rdiTotal}"
        q = tmpQuery(self, sqlExtract)
        q.addBD(enjeu=rdiTotal, alea=rdiTotal, requete=requete)
        return q
 
    def getSimplify(self, rdi):
        rdiAttr = rdi.getAttr() 
        rdiArgs = (rdiAttr['table'], rdiAttr['schema'])
        fields = ", ".join(map(quote, self.getNonGeomFields(*rdiArgs)))
        geom = self.getGeometryField(*rdiArgs)
        rdiTotal = self.full(*rdiArgs)
        sqlExtract = f"SELECT ST_MakeValid(ST_SimplifyPreserveTopology({quote(geom)}, {self.context['simplify']})) as geom, {fields} FROM {rdiTotal}"
        requete = f"simplify{self.context['simplify']}"
        q = tmpQuery(self, sqlExtract)
        q.addBD(enjeu=rdiTotal, alea=rdiTotal, requete=requete)
        return q
        
    def getTampon(self, rdi):
        rdiAttr = rdi.getAttr() 
        rdiArgs = (rdiAttr['table'], rdiAttr['schema'])
        rdiTotal = self.full(*rdiArgs)
        sqlExtract, requete = self.getTamponSQL(rdi, True)
        q = tmpQuery(self, sqlExtract)
        q.addBD(enjeu=rdiTotal, alea=rdiTotal, requete=f"buffer{requete}")
        return q

    def getTamponSQL(self, rdi, buff=False):
        rdiAttr = rdi.getAttr() 
        rdiArgs = (rdiAttr['table'], rdiAttr['schema'])
        geomST = f"{quote(self.getGeometryField(*rdiArgs))}"
        requete = ""
        buffer = self.context['buffer'] or 0
        if self.context['buffer'] and self.context['buffer']!=0 or buff:
            requete += f"{self.context['buffer']}"
        if self.isGeomPolygon(*rdiArgs) and self.context['correction']:
            geomST = f"st_buffer(st_collectionExtract(st_makeValid({geomST}), 3), 0)"
            requete += "_corr"
        if self.context['simplify']>0:
            geomST = f"ST_MakeValid(ST_SimplifyPreserveTopology({geomST}, {self.context['simplify']}))"
            requete += f"_simpl{self.context['simplify']}"
        geomST = f"ST_UNION({geomST})"
        if self.context['buffer'] and self.context['buffer']!=0 or buff:
            requete += f"_buff{self.context['buffer']}"
            geomST = f"ST_Buffer({geomST}, {buffer})"

        if rdiAttr['context']:
            if 'field' in rdiAttr['context']:
                where = f" WHERE {rdiAttr['context']['field']} NOT IN ({rdiAttr['context']['values']})"
                requete += f":{rdiAttr['context']['field']}:{rdiAttr['context']['values']}"
            if 'condition' in rdiAttr['context']:
                where = f" WHERE {rdiAttr['context']['condition']}"
                if 'excl' in rdiAttr['context']:
                    requete += f":{rdiAttr['context']['excl']}"
                else:
                    requete += f":{compress(rdiAttr['context']['condition'])}"
        else:
            where = ""    
        
        sql = f"SELECT {geomST} as geom FROM {self.full(*rdiArgs)} {where}"
        return sql, requete  

    def getSingle(self, rdi):
        rdiAttr = rdi.getAttr()
        rdiArgs = (rdiAttr['table'], rdiAttr['schema'])
        fields = ", ".join(map(quote, self.getNonGeomFields(*rdiArgs)))
        geom = self.getGeometryField(*rdiArgs)
        rdiTotal = self.full(*rdiArgs)
        sqlExtract = f"SELECT (ST_DUMP({quote(geom)})).geom as geom, {fields} FROM {rdiTotal}"
        q = tmpQuery(self, sqlExtract)
        # q.setOrigin(*rdiArgs, method='dump')
        # q.setPkey(self.getPrimaryKey(*rdiArgs))
        q.addBD(enjeu=rdiTotal, alea=rdiTotal, requete="dump")
        return q
        
    def getSumAttr(self, rdi):
        rdiAttr = rdi.getAttr()
        rdiArgs = (rdiAttr['table'], rdiAttr['schema'])
        fields = ", ".join(map(quote, self.getNonGeomFields(*rdiArgs)))
        geom = self.getGeometryField(*rdiArgs)
        rdiTotal = self.full(*rdiArgs)
        all = f"{rdiTotal}.*"
        requete = ""
        multiple = ""
        if self.needDump(*rdiArgs):
            all = f"(ST_DUMP({quote(geom)})).geom as geom, {fields}"
            requete += "_dump"
            method = 'dump'
        if self.context['clusterField'] and rdiAttr['field']!="":
            field = rdiAttr['field']
            precision = self.context['clusterPrecision']
            part = self.getNewColumnName(*rdiArgs, "part")
            rdi.part = part
            all += f", {part}"
            multiple =  f" CROSS JOIN generate_series(1, round({rdiTotal}.{quote(field)}*{precision})::integer, 1) {part}"  
            requete += f"_mult{precision}_{field}"
        sqlExtract = f"SELECT {all} FROM {rdiTotal} {multiple}"
        q = tmpQuery(self, sqlExtract)
        q.addBD(enjeu=rdiTotal, alea=rdiTotal, requete=requete)
        return q
  
    def getExtractEnjeuU(self, alea, enjeu):
        aleaAttr = alea.getAttr() 
        aleaArgs = (aleaAttr['table'], aleaAttr['schema'])    
        selectBuffer, requete = self.getTamponSQL(alea) 
        q = self.getTampon(alea)
        if q.dispo:
            selectBuffer = f"SELECT geom FROM {self.full(q.table)}"
        return self.getExtractEnjeuUsql(enjeu, selectBuffer, requete, aleaAttr['table'], aleaAttr['schema'])
            
    def getExtractEnjeuUsql(self, enjeu, selectBuffer, requete, table, schema):
        enjeuAttr = enjeu.getAttr()
        enjeuArgs = (enjeuAttr['table'], enjeuAttr['schema'])
        enjeuTotal = self.full(*enjeuArgs)
        aleaTotal = self.full(table, schema)
        requete = f"intersect{requete}"
        geomEnjeu = self.getGeometryField(*enjeuArgs)    
        all = f"{enjeuTotal}.*"
        multiple = ""
        srid = self.getSrid(table, schema)
        pkey = self.getPrimaryKey(*enjeuArgs)
        enjeuProj = f"{self.proj(enjeuAttr['table'], srid, enjeuAttr['schema'], geomEnjeu)}"
        if self.isGeomPolygon(*enjeuArgs) and self.context['correction']:
            enjeuProj = f"st_buffer(st_collectionExtract(st_makeValid({enjeuProj}), 3), 0)"
            requete += "_corrE"
        method = None
        if enjeu.isStat==None and enjeu.cluster and self.needDump(*enjeuArgs):
            list = self.getNonGeomFields(*enjeuArgs)
            fields = ", ".join(map(lambda x: f"{enjeuTotal}.{quote(x)}", list))
            all = f"(ST_DUMP({enjeuTotal}.{quote(geomEnjeu)})).geom as geom, {fields}"
            geomEnjeu = "geom"
            requete += "_dump"
            method = 'dump'
        
        if self.context['clusterField'] and enjeuAttr['field']!="" and enjeu.isStat==None and enjeu.cluster and self.isGeomPoint(*enjeuArgs):
            field = enjeuAttr['field']
            precision = self.context['clusterPrecision']
            part = self.getNewColumnName(*enjeuArgs, "part")
            enjeu.part = part
            all += f", {part}"
            multiple =  f" CROSS JOIN generate_series(1, round({enjeuTotal}.{quote(field)}*{precision})::integer, 1) {part}"  
            requete += f"_mult{precision}_{field}"
       
        if enjeu.isStat==None and self.context['cutting'] and not self.isGeomPoint(*enjeuArgs):
            list = self.getNonGeomFields(*enjeuArgs)
            fields = ", ".join(map(lambda x: f"{enjeuTotal}.{quote(x)}", list))
            if enjeu.inverted:
                all = f"ST_Multi({enjeuProj}) as geom, {fields}"
            else:
                all = f"ST_Multi(ST_Intersection({enjeuProj}, buffer.geom)) as geom, {fields}"
            geomEnjeu = "geom"
            requete += "_cut"
            method = 'cut'
        
        n = ""
        if enjeu.isStat==None and enjeu.inverted:
            n = "NOT"
            requete += "_not"
        
        sqlExtract = f"WITH buffer AS ({selectBuffer})" +\
			f" SELECT {all} FROM {enjeuTotal} , buffer {multiple}" +\
			f" WHERE {n} ST_Intersects(buffer.geom, {enjeuProj})"
             
        if enjeu.isStat==None and self.context['cutting'] and not self.isGeomPoint(*enjeuArgs) and enjeu.inverted:
            typeGeomInt = 1
            if self.isGeomPolygon(*enjeuArgs):
                typeGeomInt = 3
            if self.isGeomLinestring(*enjeuArgs):
                typeGeomInt = 2
            all = f"ST_Multi(ST_CollectionExtract(ST_Difference({enjeuProj}, buffer.geom), {typeGeomInt})) as geom, {fields}"
            sqlExtract += f" UNION SELECT {all} FROM {enjeuTotal} , buffer " +\
                        f" WHERE ST_Intersects(buffer.geom, {self.proj(enjeuAttr['table'], srid, enjeuAttr['schema'], geomEnjeu)})"
        q = tmpQuery(self, sqlExtract)
        # q.setPkey(pkey)
        q.setOrigin(*enjeuArgs, method=method)
        q.addBD(enjeu=enjeuTotal, alea=aleaTotal, requete=requete)
        return q
                    
    def getAssembEnjeu(self, enjeu, tList):
        enjeuAttr = enjeu.getAttr()
        enjeuArgs = (enjeuAttr['table'], enjeuAttr['schema'])
        if len(tList)>1:
            method = None
            join = "union"
            if enjeu.cluster and self.needDump(*enjeuArgs):
                method = 'dump'
            if enjeu.inverted:
                join = "intersect" 
            sqlExtract = f" {join} ".join(map(lambda x: f"SELECT * FROM {self.full(x)}", tList))
            q = tmpQuery(self, sqlExtract)
            q.setOrigin(*enjeuArgs, method=method)
            # q.setPkey(self.getPrimaryKey(*enjeuArgs))
            q.addBD(enjeu=self.full(*enjeuArgs), alea="+".join(tList), requete=join)
        elif len(tList)>0:
            q = tmpQuery(self, '')
            q.table = tList[0]
            q.dispo = True
        else:
            if enjeu.cluster:
                q = self.getSumAttr(rdi=enjeu)
            else:
                q = tmpQuery(self, '')
                q.table = enjeuAttr['table']
                q.dispo = True
        return q
        
    def getExtractEnjeu(self, enjeu, aleaList, callback=None, args=(), kwargs={}):
        enjeuAttr = enjeu.getAttr()
        enjeuArgs = (enjeuAttr['table'], enjeuAttr['schema'])
        if enjeu.cluster and self.isGeomPoint(*enjeuArgs):
            enjeu.needBack = True
        else:
            enjeu.needBack = False
        nb = len(aleaList)
        s = "s" if nb>1 else ""
        description = f"Extraction enjeu {quote(enjeuAttr['name'])} sur {nb} emprise{s}"
        task = mGlobalTask(description)
        if enjeu.isStat==None and self.context['cutting'] and not self.isGeomPoint(*enjeuArgs) and len(aleaList)>1:
            self.getFusionAlea(enjeu, aleaList, self.getExtractFusion, task, callback, args, kwargs)
        else:
            self.getFusionAlea(None, aleaList)
            tList = [] 
            for alea in aleaList:
                aleaAttr = alea.getAttr() 
                q = self.getExtractEnjeuU(alea=alea, enjeu=enjeu)
                if not q.dispo:
                    q.description = f"{quote(aleaAttr['name'])}/{quote(enjeuAttr['name'])}"
                    task.addSubTask(q)
                tList.append(q.table)
            q = self.getAssembEnjeu(enjeu=enjeu, tList=tList)
            q.description = f"Assemblage {len(tList)}/{quote(enjeuAttr['name'])}"
            self.endExtractEnjeu(enjeu, task, q, callback, args, kwargs)
    
    def getExtractFusion(self, table, enjeu, task, callback, args, kwargs):
        selectBuffer = f"SELECT geom FROM {self.full(table)}"
        q = self.getExtractEnjeuUsql(enjeu, selectBuffer, "", table, None)
        q.description = f"Extraction sur alea fusion"
        self.endExtractEnjeu(enjeu, task, q, callback, args, kwargs)

    def endExtractEnjeu(self, enjeu, task, q, callback=None, args=(), kwargs={}):
        if callback!=None:
            task.setCallback(callback, q.table, enjeu, *args, **kwargs)
        else:
            task.setCallback(enjeu.affLayer, q.table)
        if not q.dispo:
            task.addSubTask(q, final=True)
        if task.count()==0 or callback!=None:
            enjeu.tableWaiting = q.table
            self.manager.add(task)
        else:
            self.getWaiting(enjeu, self.launchCallback, task, q.table)

    
    
    def launchCallback(self, rdi, task, table):
        rdi.tableWaiting = table
        self.manager.add(task)


    def getEmptyTask(self, rdi):
        rdiAttr = rdi.getAttr()
        rdiArgs = (rdiAttr['table'], rdiAttr['schema'])
        fields = ", ".join(map(quote, self.getNonGeomFields(*rdiArgs)))
        geom = self.getGeometryField(*rdiArgs)
        task = mGlobalTask("Extraction enjeu vide "+quote(rdiAttr['name']))
        rdiTotal = self.full(*rdiArgs)
        sqlExtract = "select * from "+rdiTotal+" limit 0"
        requete = "empty"
        method = None
        if rdi.cluster and self.needDump(*rdiArgs):
            sqlExtract = f"SELECT (ST_DUMP({quote(geom)})).geom as geom, {fields} FROM {rdiTotal} limit 0"
            requete += "_dump"
            method = 'dump'
        q = tmpQuery(self, sqlExtract)
        q.setOrigin(*rdiArgs, method=method)
        # q.setPkey(self.getPrimaryKey(*rdiArgs))
        q.addBD(enjeu=rdiTotal, alea=rdiTotal, requete=requete)
        rdi.tableWaiting = q.table
        if not q.dispo:
            q.description = "empty " + quote(rdiAttr['name'])
            task.addSubTask(q, final=True)
        return task, q.table
        
        
    def getError(self, rdi):
        task, table = self.getEmptyTask(rdi)
        task.setCallback(rdi.affLayer, table, error=True)
        self.manager.add(task)         
    
    def getWaiting(self, rdi, *args, **kwargs):
        task, table = self.getEmptyTask(rdi)
        task.setCallback(rdi.affLayerWaiting, table, *args, **kwargs)
        self.manager.add(task)         

    def getExtractAlea(self, alea):
        aleaAttr = alea.getAttr()
        if self.context['simplify']>0:
            description = f"Simplification alea {quote(aleaAttr['name'])}"
            task = mGlobalTask(description)
            q = self.getSimplify(rdi=alea)
            alea.tableWaiting = q.table
            task.setCallback(alea.affLayer, q.table)
            if not q.dispo:
                q.description = f"Simplification/{quote(aleaAttr['name'])}"
                task.addSubTask(q, final=True)
            self.manager.add(task)
        else:
            alea.tableWaiting = aleaAttr['table']
            alea.affLayer()    
        alea.tamponWaiting = None
        alea.affTampon()
        if self.context['buffer']!=0:
            description = f"Extraction tampon {self.context['buffer']}m alea {quote(aleaAttr['name'])}"
            task = mGlobalTask(description)
            q = self.getTampon(rdi=alea)
            alea.tamponWaiting = q.table
            task.setCallback(alea.affTampon, q.table)
            if not q.dispo:
                q.description = f"Tampon/{quote(aleaAttr['name'])}"
                task.addSubTask(q, final=True)
            self.manager.add(task)

    
    def getFusion(self, rdi, list, mode, callback, args):
        description = f"Extraction emprise totale"
        task = mGlobalTask(description)
        tList = []
        for l in list:
            lAttr = l.getAttr() 
            q = None
            if mode=='alea':
                q = self.getTampon(rdi=l)
            if mode=='enjeu':
                q = self.getHull(rdi=l)
            if q!=None and not q.dispo:
                q.description = f"{quote(lAttr['name'])}"
                task.addSubTask(q)
            tList.append(q.table)
        sqlUnion = " UNION ".join(map(lambda x: f"SELECT * FROM {self.full(x)}", tList))
        if sqlUnion!="":
            sqlExtract = f"SELECT 1 as id, ST_UNION(geom) as geom FROM ({sqlUnion}) as foo"
            q = tmpQuery(self, sqlExtract)
            q.addBD(enjeu="", alea="+".join(tList), requete="fusion")  
            if not q.dispo:    
                task.addSubTask(q, final=True)
            if callback!=None:
                task.setCallback(callback, q.table, rdi, *args)
            self.manager.add(task)    
    
    def getFusionAlea(self, rdi, aleaList, callback=None, *args):
        self.getFusion(rdi, aleaList, 'alea', callback, args)
        
    def getFusionEnjeu(self, rdi, enjeuList, callback=None, *args):
        self.getFusion(rdi, enjeuList, 'enjeu', callback, args)

    def deleteTable(self, table, schema=None):
        sql = f"DROP TABLE IF EXISTS {self.full(table, schema)}"
        query = self.sqlSend(sql)
    

    def nbTmp(self, callback, *args, **kwargs):
        c = 0
        schema = self.getSchema()
        sql = f"SELECT count(*) from {self.admin('tableTmp')}"
        query = self.sqlSend(sql)
        if query and query.next():
            c = query.value(0)
        else:
            c = 0
        s = "s" if c>1 else ""
        txt = f"{c} requête{s} présente{s}"
        if callback:
            callback(txt, *args, **kwargs)
    
    def purge(self, callback, *args, **kwargs):
        c = 0
        schema = self.getSchema()
        sql = f"SELECT tablename FROM pg_catalog.pg_tables" + \
            f" WHERE schemaname='{schema}'" + \
            f" AND tablename LIKE ('{self.param.get('tmpPrefixe')}%')"
        query = self.sqlSend(sql)
        while query and query.next():
            table = query.value(0)
            sql = f"DROP TABLE IF EXISTS {self.full(table)}"
            self.db.exec_(sql)
            c += 1
        sql = f"TRUNCATE {self.admin('tableTmp')} RESTART IDENTITY"
        query = self.sqlSend(sql)
        sql = f"TRUNCATE {self.admin('tableTmp')}"
        query = self.sqlSend(sql)
        s = "s" if c>1 else ""
        txt = f"{c} table{s} purgée{s}"
        if callback:
            callback(txt, *args, **kwargs)

    





class tmpQuery(abstractQuery):
    def __init__(self, psql, sqlExec):
        abstractQuery.__init__(self, psql)
        self.queryTimeout = self.psql.param.get('queryTimeout')
        self.stockTimeout = self.psql.param.get('stockTimeout')
        self.sqlExec = sqlExec
        self.adminTable = self.psql.admin('tableTmp')
        self.tmpPrefixe = self.psql.param.get('tmpPrefixe')
      
    def delete(self, table=None):
        self.psql.deleteTable(table)
        sql = f"DELETE FROM {self.adminTable} WHERE tablename='{table}'"
        query = self.psql.db.exec_(sql)
        
    def checkBD(self, enjeu, alea, requete):
        sql = f"SELECT tablename, dispo, " + \
            f" (DATE_PART('hour',localtimestamp-cree)*3600+DATE_PART('minute',localtimestamp-cree)*60+DATE_PART('second',localtimestamp-cree))" + \
            f" as delai FROM {self.adminTable}" + \
            f" WHERE alea='{alea}' AND enjeu='{enjeu}' AND requete='{requete}'" +\
            f" ORDER BY (case when dispo is null then 1 else 0 end), dispo desc, cree desc"
        query = self.psql.db.exec_(sql)
        while query.next():
            table = query.value(0)
            dispo = query.value(1)
            delai = query.value(2)
            if self.dispo:
                self.delete(table)
            else:
                if dispo and self.isTable(table):
                    if delai>self.stockTimeout:
                        self.delete(table)
                    else:
                        self.dispo = True
                        self.table = table
                else:
                    try:
                        m = self.psql.manager.waiting[self.full(table)]
                    except:
                        m = None
                    if delai>self.queryTimeout:
                        self.delete(table)
                        if m:
                            m.cancel()
                    else:
                        self.table = table
                        self.dependency = m
        
    def addBD(self, enjeu, alea, requete):
        self.checkBD(enjeu, alea, requete)
        self.tmp = self.tmpPrefixe + makeUniqueIdent()
        self.sqlPrepare = f" INSERT INTO {self.adminTable} (tablename, alea, enjeu, cree, requete, sql)" + \
            f" values ('{self.tmp}', '{alea}', '{enjeu}', localtimestamp, '{requete}', '{self.sqlExec}');"
        if not self.table:
            
            self.table = self.tmp

    def makeCreate(self):
        sql = f" CREATE TABLE {self.full()} AS ({self.sqlExec});"
        if self.pkey:
            col = f"{self.table}_pkey"
            sql += f" ALTER TABLE {self.full()} ADD CONSTRAINT {quote(col)} PRIMARY KEY ({quote(self.pkey)});"
        return sql
    
    def prepare(self):
        self.psql.db.exec_(self.sqlPrepare)
        
    def endCreate(self):
        sql = f" UPDATE {self.adminTable} SET dispo=timeofday()::timestamp WHERE tablename='{self.table}';"
        return sql
        
    def kill(self):
        sql = f" DELETE FROM {self.adminTable} WHERE tablename='{self.table}';"
        query = self.psql.db.exec_(sql)




class connDialog(QgsDialog):
    
    axepted = pyqtSignal(str, str)
    
    def __init__(self, psql):
        buttons = QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel
        QgsDialog.__init__(self, iface.mainWindow(),
                            fl=Qt.WindowFlags(),
                            buttons=buttons,
                            orientation=Qt.Horizontal)
        self.setWindowTitle("Connexion à Postgres") 
        self.setWindowModality(Qt.ApplicationModal)
        self.psql = psql
        self.drawTitle()
        self.drawForm()
        self.show()
        
    def drawTitle(self):
        hbox = QtWidgets.QHBoxLayout(self)
        hbox.setSpacing(5)
        hbox.setContentsMargins(0,5,0,5)
        self.layout().addLayout(hbox)
        l = QtWidgets.QLabel(f"Identifiants de connexion pour ", self)
        hbox.addWidget(l)
        l = QtWidgets.QLabel(f"{self.psql.PGdb}", self)
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        hbox.addWidget(l)
        l = QtWidgets.QLabel(f" sur ", self)
        hbox.addWidget(l)
        l = QtWidgets.QLabel(f"{self.psql.PGhost}", self)
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        hbox.addWidget(l)
        
    def drawForm(self):
        form = QtWidgets.QFormLayout(self)
        form.setSpacing(5)
        self.layout().addLayout(form)
        wl = QtWidgets.QLabel("Login", self)
        wf = QtWidgets.QLineEdit(self)
        self.loginEdit = wf
        wf.setText(self.psql.PGuser)
        form.addRow(wl, wf)
        wl = QtWidgets.QLabel("Password", self)
        wf = QgsPasswordLineEdit(self)
        self.passwordEdit = wf
        form.addRow(wl, wf)
        
        l = QtWidgets.QLabel("", self)
        self.layout().addWidget(l)
        
    def login(self):
        return self.loginEdit.text()
        
    def password(self):
        return self.passwordEdit.text()
   