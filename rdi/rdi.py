# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication, Qt, QUrl
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QDesktopWidget

from .rdi_dockwidget import rdiDockWidget
from .rdi_configure import rdiConfigure
from .rdi_notice import rdiNotice
from .rdi_meta import rdiMeta

import os.path

class RDI:

    def __init__(self, iface):
        
        self.iface = iface
        self.plugin_dir = os.path.dirname(__file__)
        self.imageFolder = os.path.join(self.plugin_dir,'images')

        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'rdi_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        self.meta = rdiMeta()
        self.pluginName = self.meta.getName()
        
        self.actions = []
        self.menu = self.tr(f"&{self.pluginName}")
        self.toolbar = None
        self.psql = None

        self.pluginIsActive = False
        self.dockwidget = None
        self.configureIsActive = False
        self.noticeIsActive = False
        

    def tr(self, message):
        return QCoreApplication.translate('rdi', message)
    
    def makeToolBar(self):
        self.toolbar = self.iface.addToolBar(u'RDI')
        self.toolbar.setObjectName(u'RDI')
    
    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        add_to_maintoolbar=False,
        status_tip=None,
        whats_this=None,
        parent=None):
        
        if icon_path:
            icon = QIcon(icon_path)
            action = QAction(icon, text, parent)
        else:
            action = QAction(text, parent)
        if callback:
            action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            if not self.toolbar:
                self.makeToolBar()
            self.toolbar.addAction(action)
            
        if add_to_maintoolbar:    
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToMenu(self.menu, action)

        self.actions.append(action)

        return action


    def initGui(self):

        icon_path = ':/plugins/rdi/images/analyze.png'
        icon_path = os.path.join(self.imageFolder,'analyze.png')
        self.add_action(
            icon_path,
            text=self.tr(u'Analyser'),
            callback=self.run,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)   
        icon_path = os.path.join(self.imageFolder,'config.png')
        self.add_action(
            icon_path,
            text=self.tr(u'Configurer'),
            callback=self.configure,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)
        icon_path = os.path.join(self.imageFolder,'help.png')
        self.add_action(
            icon_path,
            text=self.tr(u'A propos'),
            callback=self.notice,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)
        icon_path = os.path.join(self.imageFolder,'analyze.png')
        self.add_action(
            icon_path,
            text=self.tr(self.menu),
            callback=self.run,
            parent=self.iface.mainWindow(),
            add_to_menu=False,
            add_to_toolbar=False,
            add_to_maintoolbar=True)   

    def onClosePlugin(self): 
        self.dockwidget.closingPlugin.disconnect(self.onClosePlugin)
        self.pluginIsActive = False


    def unload(self):
        for action in self.actions:
            try:
                self.iface.removePluginMenu(self.menu, action)
            except:
                pass
            try:
                self.iface.removeToolBarIcon(action)
            except:
                pass
        try:
            del self.toolbar
        except:
            pass
        if self.pluginIsActive and self.dockwidget:
            self.dockwidget.closeAllLayer()
            self.dockwidget.close()
        if self.configureIsActive and self.dockconfigure:
            self.dockconfigure.close()

    def run(self, reload=False):
        if not self.pluginIsActive:
            self.pluginIsActive = True
            if self.dockwidget == None:
                self.dockwidget = rdiDockWidget(self)
            elif reload:
                self.dockwidget.rebuilt()    
            self.dockwidget.closingPlugin.connect(self.onClosePlugin)
            self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dockwidget)
            self.dockwidget.show()
        elif reload:
            self.dockwidget.rebuilt()
    
    
    
    def onCloseConfigure(self): 
        self.dockconfigure.closingPlugin.disconnect(self.onCloseConfigure)
        self.configureIsActive = False
        
    def configure(self):
        if not self.configureIsActive:
            self.dockconfigure = rdiConfigure(self)
            screen = QDesktopWidget().screenGeometry()
            self.dockconfigure.setGeometry(screen.width()/2-200, screen.height()/2-200, 0, 0)
            self.dockconfigure.closingPlugin.connect(self.onCloseConfigure)
            self.dockconfigure.show()
            self.configureIsActive = True
        
        
        
    
    def onCloseNotice(self): 
        self.docknotice.closingPlugin.disconnect(self.onCloseNotice)
        self.noticeIsActive = False
    
    def notice(self, tag=None):
        if not self.noticeIsActive:
            self.docknotice = rdiNotice(self)
            screen = QDesktopWidget().screenGeometry()
            self.docknotice.setGeometry(0, 0, screen.width(), screen.height()-40)
            self.docknotice.closingPlugin.connect(self.onCloseNotice)
            self.docknotice.show()
            self.noticeIsActive = True
        if tag!=None:
            self.docknotice.actuPage(tag)
        
        