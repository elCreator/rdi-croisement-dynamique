# -*- coding: utf-8 -*-
"""
/***************************************************************************
 RDI
                                 A QGIS plugin
 Croisement dynamique des enjeux avec les zones inondables
                             -------------------
        begin                : 2020-12-02
        copyright            : (C) 2020 by DDT67
        email                : ddt-inondation@bas-rhin.gouv.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    from .rdi import RDI
    return RDI(iface)


