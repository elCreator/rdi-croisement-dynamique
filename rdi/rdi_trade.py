# -*- coding: utf-8 -*-

import sys, os, json, processing
from functools import partial

from qgis.gui import *
from qgis.core import *
from qgis.utils import iface
from PyQt5 import QtGui, QtWidgets, uic, QtXml, QtSvg
from PyQt5.QtCore import *

from .rdi_param import rdiParam
from .rdi_tools import quote, makeUniqueIdent
from .rdi_layer import rdiLayer
from .rdi_async import mGlobalTask, manageTasks, abstractQuery






class rdiTrade():

    def __init__(self, parent):
        self.admin = parent
        self.dock = parent.dock
        self.psql = parent.dock.psql
        self.initData()
        pass
        
    def createTableFromFeatures(self, layer, table, schema, callback, dump):
        description = f"Insertion de {layer.name()}"
        task = mGlobalTask(description)
        if self.psql.param.get('manualInsertion'):
            q = tradeQuery(self.psql, self)
        else:
            q = qgsTradeQuery(self.psql, self)
        q.makeTable(table, schema, layer)
        q.description = f"{quote(schema)}.{quote(table)}"
        task.addSubTask(q)
        task.setCallback(callback, q.full())
        if dump:
            q = pointQuery(self.psql, self)
            q.targetTable(table, schema)
            q.description = f"Simplification géométrie"
            task.addSubTask(q, final=True)
        self.psql.manager.add(task)
        return table 
        
    def createTableFromFile(self, layer, table, schema, callback, dump):
        name = os.path.basename(layer)
        description = f"Insertion de {name}"
        task = mGlobalTask(description)
        q = gdalTradeQuery(self.psql, self)
        q.makeTable(table, schema, layer)
        q.description = f"{quote(schema)}.{quote(table)}"
        task.addSubTask(q)
        task.setCallback(callback, q.full())
        if dump:
            q = pointQuery(self.psql, self)
            q.targetTable(table, schema)
            q.description = f"Simplification géométrie"
            task.addSubTask(q, final=True)
        self.psql.manager.add(task)
        return table 
        
    def changeTableReference(self, table, schema, wTable, wSchema, callback):
        newTable = wTable.text().strip()
        newSchema = wSchema.currentText()
        description = f"Modification de la référence pour {quote(schema)}.{quote(table)}"
        task = mGlobalTask(description)
        q = referenceQuery(self.psql, self)
        q.targetTable(table, schema)
        q.newTable(newTable, newSchema)
        q.description = f"{quote(newSchema)}.{quote(newTable)}"
        task.addSubTask(q)
        task.setCallback(callback)
        self.psql.manager.add(task)
    
    def getTypeField(self, txt):
        for e in self.translator['pgType']:
            if txt.lower().find(e['qgis'])>=0:
                return e
        print(txt)
        return {'qgis':txt, 'pg':'character varying', 'quote':True}
        
    def getTypeGeom(self, type):
        for e in self.translator['geomType']:
            if type==e['qgis']:
                return e['pg'] 
        return 'geometry'
        
        
    def disjoinTable(self, table, schema, field):
        print(table, schema, field)
        
        description = f"Démultiplication de {table}"
        task = mGlobalTask(description)
        q = sumQuery(self.psql, self)
        q.targetTableField(table, schema, field)
        q.description = f"   sur {field}"
        task.addSubTask(q)
        self.psql.manager.add(task)
        
        pass
        
        
    def initData(self):
        self.translator = {}
        self.translator['pgType'] = [
            {'qgis':'int', 'pg':'integer'},
            {'qgis':'float', 'pg':'real'},
            {'qgis':'real', 'pg':'real'},
            {'qgis':'numeric', 'pg':'real'},
            {'qgis':'varchar', 'pg':'character varying', 'quote':True},
            {'qgis':'string', 'pg':'character varying', 'quote':True},
        ]
        self.translator['geomType'] = [
            {'qgis':0, 'pg':'MULTIPOINT'},
            {'qgis':1, 'pg':'MULTILINESTRING'},
            {'qgis':2, 'pg':'MULTIPOLYGON'},
        ]
        
        
   

class tradeQuery(abstractQuery):
    def __init__(self, psql, trade):
        abstractQuery.__init__(self, psql)
        self.trade = trade
        
    def makeTable(self, table, schema, layer):
        self.table = table
        self.schema = schema
        self.layer = layer

    def makeCreate(self):
        pkey = 'id'
        geom = 'geom'
        self.srid = self.layer.crs().postgisSrid()
        geomType = self.trade.getTypeGeom(self.layer.geometryType())
        fields = []
        list = [f"{quote(pkey)} serial", f"{quote(geom)} geometry"]
        for i,f in enumerate(self.layer.fields().toList()):
            name = f.name()
            if name==geom or name==pkey:
                continue
            type = self.trade.getTypeField(f.typeName())
            field = {'name':name, 'index':i}
            if 'quote' in type:
                field['quote'] = True
            fields.append(field)
            list.append(f"{quote(name)} {type['pg']}")
        pgFields = ", ".join(list)
        sql = f" DROP TABLE IF EXISTS {self.full()} CASCADE; CREATE TABLE {self.full()} ({pgFields});"        
        vslist = []
        list = [f"{quote(geom)}"]
        for f in fields:
            list.append(f"{quote(f['name'])}")
        pgFields = ", ".join(list)
        for feat in self.layer.getFeatures():
            if feat.geometry().isNull():
                continue
            vlist = [f"st_force2d(st_makeValid(st_geomFromText('{feat.geometry().asWkt()}')))"]
            for f in fields:
                ind = feat.fieldNameIndex(f['name'])
                val = feat.attribute(f['index'])
                if val!=None:
                    if 'quote' in f:
                        val = str(val).replace("'","''")
                        val = f"'{val}'"
                else:
                    val = 'null'
                vlist.append(str(val))
            values = ", ".join(vlist)
            vslist.append(f"({values})")    
        if len(vslist)>0:
            sql += f" INSERT INTO {self.full()} ({pgFields}) VALUES " + ", ".join(vslist) + f";"
        # sql += f" SELECT ST_SetSrid({geom}, {srid}) FROM {self.full()};"

        return sql
    
    def prepare(self):
        pass
 
class qgsTradeQuery(abstractQuery):
    def __init__(self, psql, trade):
        abstractQuery.__init__(self, psql)
        self.trade = trade
        
    def makeTable(self, table, schema, layer):
        self.table = table
        self.schema = schema
        self.layer = layer

    def makeCreate(self):

        uri = QgsDataSourceUri()
        uri.setConnection(self.psql.PGhost, str(self.psql.PGport), self.psql.PGdb, self.psql.PGuser, self.psql.PGpassword)
        uri.setDataSource(self.schema, self.table, "geom", "", "id")
        outCrs=QgsCoordinateReferenceSystem('EPSG:2154')
           
             
        QgsVectorLayerExporter.exportLayer(self.layer, uri.uri(False), 'postgres', outCrs)           
        
        sql = ""

        return sql
    
    def prepare(self):
        pass


class gdalTradeQuery(abstractQuery):
    def __init__(self, psql, trade):
        abstractQuery.__init__(self, psql)
        self.trade = trade
        
    def makeTable(self, table, schema, layer):
        self.table = table
        self.schema = schema
        self.layer = layer

    def makeCreate(self):

        PARAMETERS ={   
            'HOST' : self.psql.PGhost, 
            'PORT' : str(self.psql.PGport), 
            'DBNAME' : self.psql.PGdb, 
            'SCHEMA' : self.schema,
            'USER' : self.psql.PGuser,
            'PASSWORD' : self.psql.PGpassword,
            'INPUT' : self.layer,                       
            'TABLE' : self.table, 
            'SHAPE_ENCODING' : 'UTF-8',                   
            'INDEX' : False,                         
            'T_SRS' : QgsCoordinateReferenceSystem('EPSG:2154'),
            'SKIPFAILURES' : True,  
            'LAUNDER' : 'NO',
        }
            
        result=processing.run("gdal:importvectorintopostgisdatabasenewconnection", PARAMETERS)     
        sql = ""

        return sql
    
    def prepare(self):
        pass
        
        
        
        
        
class pointQuery(abstractQuery):
    def __init__(self, psql, trade):
        abstractQuery.__init__(self, psql)
        self.trade = trade
        self.fields = None
        
    def targetTable(self, table, schema):
        self.table = table
        self.schema = schema
        args = (self.table, self.schema)
        if self.psql.isGeomPoint(*args):
            self.tmp = self.psql.full(f"{self.table}_tmp", self.schema)
            self.fields = ", ".join(map(quote, self.psql.getNonGeomFields(*args)))
            self.geom = self.psql.getGeometryField(*args)

    def makeCreate(self):
        if self.fields:
            sqlExtract = f"SELECT (ST_DUMP({quote(self.geom)})).geom as geom, {self.fields} FROM {self.full()}"
            sql = f" DROP TABLE IF EXISTS {self.tmp} CASCADE;  CREATE TABLE {self.tmp} AS ({sqlExtract});"
            sql += f" DROP TABLE IF EXISTS {self.full()} CASCADE; CREATE TABLE {self.full()} AS (SELECT * FROM {self.tmp});"
            sql += f" DROP TABLE IF EXISTS {self.tmp} CASCADE;"
            return sql
    
    def prepare(self):
        # self.error.append(self.makeCreate())
        pass
    


# class sumQuery(abstractQuery):
    # def __init__(self, psql, trade):
        # abstractQuery.__init__(self, psql)
        # self.trade = trade
        
    # def targetTableField(self, table, schema, field):
        # self.table = table
        # self.schema = schema
        # self.field = field
        # self.new = self.psql.full(f"{self.table}_sum", self.schema)
        # self.fields = ",".join(self.psql.getColumnsTable(self.table, self.schema))

    # def makeCreate(self):  
        # precision = 10
        # sql = f" DROP TABLE IF EXISTS {self.new} CASCADE;" + \
            # f" CREATE TABLE {self.new} AS (SELECT * FROM {self.full()} t" + \
            # f" CROSS JOIN generate_series(0, (t.{quote(self.field)}+1)::integer, {(1/precision)}) part" + \
            # f" WHERE part<=round(t.{quote(self.field)}*{precision})/{precision}-{(1/precision)});"        
        # return sql     
    
    # def prepare(self):
        # self.error.append(self.makeCreate())
        # pass







class referenceQuery(abstractQuery):
    def __init__(self, psql, trade):
        abstractQuery.__init__(self, psql)
        self.trade = trade

    def targetTable(self, table, schema):
        self.table = table
        self.schema = schema
        
    def newTable(self, table, schema):
        self.newTable = table
        self.newSchema = schema
        
    def makeCreate(self):
        sql = ""
        if self.schema!=self.newSchema:
            sql += f" ALTER TABLE {self.full()} SET SCHEMA {self.newSchema};"
        if self.table!=self.newTable:
            sql += f" ALTER TABLE {self.full(self.table, self.newSchema)} RENAME TO {self.full(self.newTable, self.newSchema)};"
        if sql!="":
            sql += f" UPDATE {self.trade.admin.fullAdmin} SET tablename='{self.newTable}', schemaname='{self.newSchema}' WHERE tablename='{self.table}' AND schemaname='{self.schema}'"
        return sql
    
    def prepare(self):
        # self.error.append(self.makeCreate())
        pass










