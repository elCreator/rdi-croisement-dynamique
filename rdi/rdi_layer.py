# -*- coding: utf-8 -*-

import os, json, time, sys
from functools import partial

from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtCore import *

from .rdi_tools import rdiTimer, flashMsg, quote, clickLabel


class rdiLayer(QObject):
    
    clicked = pyqtSignal()
    changed = pyqtSignal()
    
    def __init__(self, meta=None, code=None):
        QObject.__init__(self)
        self.vlayer = None
        self.layer = None
        self.style = None
        self.stat = None
        self.cb = None
        self.combo = None
        self.code = None
        self.checked = None
        self.cluster = None
        self.needBack = None
        self.isStat = None
        self.inverted = False
        self.loaded = False
        self.widget = None
        self.showed = True
        self.icoPaint = None
        self.params = {}
        self.tampon = rdiTampon(self)
        self.light = rdiLight(self)
        self.search = []
        self.name = "no name"
        self.setMeta(meta)
     
    def setMeta(self, meta):
        self.meta = meta
        self.parent()
        self.id()
        self.setName()
        self.findKeySearch()
        self.setTimer()
        self.context = None
        self.tableWaiting = self.tableName()
        
    def cbClicked(self):
        pass
        
    def blockSignals(self, bool):
        if self.cb!=None:
            self.cb.blockSignals(bool)
        if self.combo!=None:
            self.combo.blockSignals(bool)
    
    def recup(self, layer):
        self.blockSignals(True)    
        self.cb.setChecked(True)
        self.checked = True
        dock = self.coll.dock
        self.cluster = dock.param.get('cluster')
        if self.combo:
            for i in range(self.combo.count()):
                if self.combo.itemData(i).id()==layer.rdiId:
                    self.combo.setCurrentIndex(i)
                    break
        self.layer = layer.rdi.layer
        self.tampon = layer.rdi.tampon
        self.light = layer.rdi.light
        self.layerId = layer.rdi.layerId
        self.style = layer.rdi.style
        self.inverted = layer.rdi.inverted
        self.field.memo = layer.rdi.field.memo
        self.style.rdi = self
        if layer.rdi.icoPaint:
            self.manualPainting()
        
        layer.rdi = self
        self.invertAff()
        
        self.blockSignals(False)    
    
    def getValueFromMeta(self, key, fallback=None):
        val = fallback
        try:
            v = self.meta[key]
            if v:
                val = v
        except:
            val = fallback
        return val
    
    def setName(self):
        name = str(self.getValueFromMeta('tablename', 'error'))
        name = str(self.getValueFromMeta('name', name))
        self.name = name
    
    def deleteLayer(self):
        self.coll.remove(self)
    
    def newLine(self, layout):
        self.bloc = layout
        wl = QtWidgets.QWidget()
        layout.addWidget(wl)
        hbox = QtWidgets.QHBoxLayout()
        hbox.setSpacing(0)
        hbox.setContentsMargins(0,0,0,0)
        wl.setLayout(hbox)
        self.layout = hbox
        self.widget = wl
    
    def addCb(self, layout):
        self.newLine(layout)
        l = rdiLayerLabel(os.path.join(self.coll.dock.imageFolder, "invert.png"))
        self.layout.addWidget(l)
        self.icoInverted = l
        self.invertAff()
        name = self.name
        if self.parent()!=None:
            name = f"{self.coll.find(self.parent()).name}  ({name})"
        self.cb = rdiLayerCb(name)
        self.layout.addWidget(self.cb)
        self.cb.stateChanged.connect(self.setLayer)
        self.cb.rightClick.connect(self.invert)
        l.leftClick.connect(self.cb.click)
        l.rightClick.connect(self.invert)
        self.icoPaint = None
        self.field = rdiLayerField(self)
        if self.coll.matchCode('stats'):
            l = rdiLayerLabel(os.path.join(self.coll.dock.imageFolder, "delete.png"))
            l.setToolTip("Supprimer cette statistique de la liste")
            self.layout.addWidget(l)
            l.leftClick.connect(self.deleteLayer)
        if hasattr(self, 'coll') and not self.coll.dock.param.get('cbCompact') and self.parent()!=None:
            self.addKeySearch(self.coll.find(self.parent()).name.lower())
        
    def invert(self):
        if self.coll.dock.param.get('inversion') and self.coll.matchCode('enjeu'):
            self.inverted = not self.inverted
            self.invertAff()
            self.setLayer()
    
    def invertAff(self):
        if self.inverted:
            self.icoInverted.show()
        else:
            self.icoInverted.hide()
    
    def addCombo(self, rdi):
        if not self.combo:
            self.combo = QtWidgets.QComboBox()
            self.layout.addWidget(self.combo)
            self.comboList = []
            self.combo.currentIndexChanged.connect(self.setLayer)    
            self.combo.currentIndexChanged.connect(self.changed.emit)  
        self.combo.blockSignals(True)
        self.comboList.append(rdi)    
        self.combo.addItem(rdi.name, rdi)
        self.combo.blockSignals(False)
        for key in rdi.search:
            self.addKeySearch(key)
            
    def addField(self):
        try:
            self.field.setNumeric()
            self.field.makeCombo(self.layout)
        except:
            pass
    
    def resetRenderer(self):
        if self.style:
            self.style.reset()
    
    def setLayer(self):
        self.tableWaiting = self.tableName()
        self.tamponWaiting = None
        self.error = False
        dock = self.coll.dock
        self.cluster = dock.param.get('cluster')
        self.checked = self.cb.isChecked()
        self.clicked.emit()
        if self.cb.isChecked():
            if self.coll.matchCode('enjeu'):
                dock.actuLayersEnjeu(self) 
            if self.coll.matchCode('alea'):
                dock.actuLayersAlea(self)
                dock.actuLayersEnjeu()
            if self.coll.matchCode('stats'):
                self.tableWaiting = self.tableResult
                self.affLayer(self.tableWaiting)
        else:
            self.loaded = False
            self.coll.dock.legend.remove(self)
            self.tableWaiting = None
            self.layer = dock.dyn.removeLayer(self)
            if self.coll.matchCode('alea'):
                dock.actuLayersEnjeu()
        
    
    def affLayerWaiting(self, table=None, callback=None, *args, **kwargs):
        self.affLayer(table, True)
        if callback:
            callback(self, *args, **kwargs)
    
    def affLayer(self, table=None, wait=None, error=None, callback=None, args=(), kwargs={}):
        self.wait = wait
        self.stopWaiting()
        attr = self.getAttr()
        dock = self.coll.dock
        schema = None
        if not table:
            table = attr['table']
        if table==attr['table']:
            schema = attr['schema'] 
        if table and self.tableWaiting==table:
            cond = ''
            if table==attr['table']:
                if attr['context'] and 'condition' in attr['context']:
                    cond = attr['context']['condition']
            vlayer, geomType = dock.psql.getLayer(table, attr['name'], schema, cond)
            if not vlayer:
                msg = ""
                if self.coll.matchCode('enjeu'):
                    if not wait and not error:
                        dock.psql.getError(self)
                    msg = f"L'extraction {quote(attr['name'])} n'a pas fonctionné"
                    nb = len(dock.alea.getEltsSelected())
                    if nb>0:
                        s = "s" if nb>1 else ""
                        art = f"les {nb}" if nb>1 else "la"
                        msg += f" pour {art} zone{s} sélectionée{s}"
                    
                if self.coll.matchCode('alea'):
                    self.layer = dock.dyn.removeLayer(self)
                    self.cb.setChecked(False)
                    msg = f"La zone {quote(attr['name'])} n'a pas une géométrie correcte"
                if msg!="":
                    flashMsg(msg, category='warning')  
            else:
                self.geomType = geomType
                self.affVLayer(vlayer)
                # self.vlayer = vlayer
                # if not self.layer:
                    # self.layer = dock.dyn.addLayer(self)
                # else:
                    # self.freeze = True
                    # self.layer = dock.dyn.majLayer(self)
                    # self.freeze = False
                # if self.layer:
                if self.layer:
                    # self.layerId = self.layer.layerId()
                    # self.vlayer.rdi = self
                    # self.vlayer.rdiId = self.actif()
                    # self.vlayer.destroyed.connect(self.timer.clear)
                    # dock.dyn.makeStyle(self)
                    # self.vlayer.styleChanged.connect(self.manualPainting)
                    if wait:
                        self.startWaiting()
                    else:
                        if dock.param.get('entityCount') and self.coll.matchCode('enjeu'):
                            res = dock.psql.getTableCount(table, schema)[0]
                            if 'count' in res:
                                self.layer.setName(f"{attr['name']} ({res['count']})")
                    if error:
                        name = f"[Echec] {self.baseName}"
                        self.layer.setName(name)        
                if self.needBack and dock.param.get('clusterBack'):
                    vlayer, geomType = dock.psql.getLayer(table, attr['name'], schema, cond)
                    self.affLight(vlayer, geomType)
        if callback is not None:
            if 'getLayer' in kwargs:
                del kwargs['getLayer']
                callback(self.layer.layer(), *args, **kwargs)
            else:
                callback(*args, **kwargs)
                               
    def affVLayer(self, vlayer):
        dock = self.coll.dock
        self.vlayer = vlayer
        if not self.layer:
            self.layer = dock.dyn.addLayer(self)
        else:
            self.freeze = True
            self.layer = dock.dyn.majLayer(self)
            self.freeze = False
        if self.layer:
            self.layerId = self.layer.layerId()
            self.vlayer.rdi = self
            self.vlayer.rdiId = self.actif()
            self.vlayer.destroyed.connect(self.timer.clear)
            dock.dyn.makeStyle(self)
            self.vlayer.styleChanged.connect(self.manualPainting)

                
    def manualPainting(self):
        if not self.icoPaint:
            l = rdiLayerLabel(os.path.join(self.coll.dock.imageFolder, "paint.png"), (Qt.AlignRight | Qt.AlignVCenter))
            l.setToolTip("Supprimer le style défini manuellement")
            self.layout.addWidget(l)
            l.leftClick.connect(self.cancelManualpainting)
            self.icoPaint = l
        if self.style:
            self.style.saveStyle()
            
    def cancelManualpainting(self):
        self.resetRenderer()
        self.coll.dock.dyn.makeStyle(self)
        if self.icoPaint:
            w = self.icoPaint
            self.layout.removeWidget(w)
            w.setParent(None)
            del w
            self.icoPaint = None
        
    def actuVisibleElement(self):     
        lst = []
        for feat in self.vlayer.getFeatures():
            pkey = 'id'
            ind = feat.fieldNameIndex(pkey)
            val = feat.attribute(ind)
            lst.append(val)
        print(lst)

    def setContextFromLegend(self, item):
        if self.coll.dock.filterMode=="Légende":
            lst = []
            l = []
            for i,elt in enumerate(item.elts):
                filter = self.style.filterList[i]
                if not elt.isChecked() and filter!="":
                    lst.append(filter)
                    l.append(str(i))
            if len(lst)>0:
                # self.context = {'condition':" and ".join(map(lambda x: f"NOT ({x})", lst))}       
                self.context = {'condition':" and ".join(map(lambda x: f"NOT ({x})", lst)), 'excl':"legend:"+",".join(l)}       
            else:
                self.context = None
            
    def setContextFromList(self, col, lst):
        if self.coll.dock.filterMode=="Sélection" or self.coll.dock.filterMode=="Visible":
            if len(lst)>0: 
                liste = ",".join(map(lambda x: f"'{x}'", lst))
                self.context = {'condition':f"{col} IN ({liste})", 'excl':f"sel:{col}:"+",".join(map(str,lst))}       
            else:
                self.context = None
    
    def affTampon(self, table=None):
        attr = self.getAttr()
        dock = self.coll.dock
        if table:
            if table==self.tamponWaiting:
                vlayer, geomType = dock.psql.getLayer(table, f"Tampon: {attr['name']}")
                if not vlayer:
                    msg = f"Le tampon pour {quote(attr['name'])} n'a pas fonctionné"
                    flashMsg(msg, category='warning')  
                    self.tampon.layer = dock.dyn.removeTampon(self)    
                else:
                    self.tampon.vlayer = vlayer
                    self.tampon.geomType = geomType
                    self.tampon.layer = dock.dyn.addTampon(self)
                    self.tampon.layerId = self.tampon.layer.layerId()
                    if self.tampon.layer:
                        dock.dyn.makeTampon(self)
        else:
            dock.dyn.removeTampon(self)
    
    def affLight(self, vlayer, geomType):
        dock = self.coll.dock
        self.light.vlayer = vlayer
        self.light.geomType = geomType
        self.light.layer = dock.dyn.addLight(self)
        self.light.layerId = self.light.layer.layerId()
        if self.light.layer:
            dock.dyn.makeLight(self)
    
    def startWaiting(self):
        self.baseName = self.layer.name()
        if self.timer:
            self.timer.start(500)
        self.affWaiting()
        
    def stopWaiting(self):
        try:
            self.timer.stop()
        except:
            pass
        
    def affWaiting(self):
        try:
            name = f"[Extraction{self.timer.pts()}] {self.baseName}"
            self.layer.setName(name)
        except:
            self.stopWaiting()
            pass
    
    def topAttr(self, item):
        attr = self.getValueFromMeta(item)
        if self.combo:
            attr = self.combo.currentData().getValueFromMeta(item)
        return attr  
    
    def tableName(self): 
        return self.topAttr('tablename')
            
    def schemaName(self): 
        return self.topAttr('schemaname')
        
    def layerName(self):
        layerName = self.name
        if self.combo:
            layerName = f"{self.name} ({self.combo.currentText()})"
        if hasattr(self, 'coll') and not self.coll.dock.param.get('cbCompact') and self.parent()!=None:
            layerName = f"{self.coll.find(self.parent()).name}  ({self.name})"
        return layerName
        
    def bloc(self):
        bloc = self.getValueFromMeta('bloc')
    
    def styleContent(self):
        style = self.getValueFromMeta('style')
        if self.combo:
            style = self.combo.currentData().getValueFromMeta('style', style)
        return style
        
    def jsStyle(self):
        style = self.styleContent()
        try:
            jsStyle = json.loads(style)
        except:
            if style!=None and style[:9].lower()=="<!doctype":
                jsStyle = {"xml":"1"}
            else:
                jsStyle = {}
        return jsStyle
        
    def getAttr(self):
        try:
            field = self.field.get()
        except:
            field = None
        dict = {
            'table': self.tableName(),
            'schema': self.schemaName(),
            'context': self.context,
            'name': self.layerName(),
            'field': field,
                }
        return dict
        
    def findKeySearch(self):
        for k, v in self.meta.items():
            if k in ['tablename', 'name', 'tags', 'bloc']:
                self.addKeySearch(str(v).lower())
                
    def addKeySearch(self, key):
        self.search.append(key)
        
    def keyMatch(self, key):
        bool = False
        needle = key.lower()
        for haystack in self.search:
            if haystack.find(needle)>=0:
                bool = True
        return bool
        
    def parent(self):
        try:
            p = self.propParent
        except:
            self.propParent = self.getValueFromMeta('parent')
        return self.propParent
        
    def id(self):
        try:
            p = self.propId
        except:
            self.propId = self.getValueFromMeta('id')
        return self.propId
        
    def actif(self):
        return self.topAttr('id')
    
    def setTimer(self):
        self.timer = rdiTimer(self, self.affWaiting)
      
    def getIdentFeature(self):
        pkey = self.topAttr('pkey')
        if pkey==None:
            pkey = self.coll.dock.psql.getPrimaryKey(self.tableName(), self.schemaName())   
        if pkey==None:
            list = self.coll.dock.psql.getNonGeomFields(self.tableName(), self.schemaName())
            if 'id' in list:
                pkey = 'id'
            else:
                pkey = list[0]
        return pkey
        
 
 
 
class rdiTampon():
    def __init__(self, rdi):
        self.rdi = rdi
        self.layer = None
        self.layerId = None
        self.vlayer = None
        self.geomType = None
        self.style = None
        self.params = {}

    def jsStyle(self):
        return {"tampon":"1"}
    
    def layerName(self):
        return self.rdi.layerName()
        
    def tableName(self):
        return self.rdi.tableName()
        
class rdiLight():
    def __init__(self, rdi):
        self.rdi = rdi
        self.coll = None
        self.layer = None
        self.layerId = None
        self.vlayer = None
        self.geomType = None
        self.style = None
        self.cluster = False
        self.params = {'opacity':0.2}

    def jsStyle(self):
        return self.rdi.jsStyle()
    
    def styleContent(self):
        return self.rdi.styleContent()
    
    def layerName(self):
        return self.rdi.layerName()
        
    def tableName(self):
        return self.rdi.tableName()
 
 

class rdiLayerCb(QtWidgets.QCheckBox):
    rightClick = pyqtSignal()
    def __init__(self, txt):
        QtWidgets.QCheckBox.__init__(self, txt)
    
    def mousePressEvent(self, event):
        if event.button()==Qt.RightButton:
            self.rightClick.emit()
        else:
            super().mousePressEvent(event)
            
class rdiLayerLabel(QtWidgets.QLabel):
    rightClick = pyqtSignal()
    leftClick = pyqtSignal()
    def __init__(self, path, align=None):
        QtWidgets.QLabel.__init__(self)
        ico = QtGui.QIcon(path)
        self.setMaximumWidth(20)
        self.setPixmap(ico.pixmap(15,15))
        if align:
            self.setAlignment(align)
    
    def mousePressEvent(self, event):
        if event.button()==Qt.RightButton:
            self.rightClick.emit()
        else:
            self.leftClick.emit()

class rdiLayerField(QObject):
    def __init__(self, rdi, psql=None):
        QObject.__init__(self)
        self.rdi = rdi
        self.memo = rdi.topAttr('cluster')
        self.numeric = False
        self.labels = []
        self.on = True
        try:
            self.psql = self.rdi.coll.dock.psql
            self.inCollection = True
        except:
            self.psql = psql
            self.inCollection = False
    
    def makeContainer(self, layout):
        self.container = QtWidgets.QWidget()
        if self.rdi.icoPaint:
            layout.insertWidget(layout.indexOf(self.rdi.icoPaint), self.container)
        else:
            layout.addWidget(self.container)
        lay = QtWidgets.QHBoxLayout(self.container)
        lay.setSpacing(2)
        lay.setContentsMargins(0,0,0,0)
        self.makeLabel("Ʃ(", 10)

    def makeLabel(self, txt, size):
        l = QtWidgets.QLabel(txt)
        self.labels.append(l)
        l.setMaximumWidth(size)
        self.container.layout().addWidget(l)
    
    def makeCombo(self, layout):
        self.makeContainer(layout)
        self.combo = QtWidgets.QComboBox()
        self.combo.setToolTip(f"Choix du champ à sommer pour la couche \"{self.rdi.layerName()}\"\nComptera seulement les entités si laissé vide")
        self.container.layout().addWidget(self.combo)
        self.labels.append(self.combo)
        self.makeLabel(")", 5)
        self.majCombo()
        if self.inCollection:
            self.rdi.changed.connect(self.majCombo)
            self.combo.currentIndexChanged.connect(self.change)
            
            l = QtWidgets.QLabel("")
            self.container.layout().addWidget(l)
            l.hide()
            self.label = l
            self.icoON = QtGui.QIcon(os.path.join(self.rdi.coll.dock.imageFolder,'close.png'))
            self.icoOFF = QtGui.QIcon(os.path.join(self.rdi.coll.dock.imageFolder,'sum.png'))
            butt = QtWidgets.QPushButton(self.icoON, '')
            butt.setToolTip(f"Compter seulement les entités")
            butt.setMaximumWidth(20)
            self.container.layout().addWidget(butt)
            butt.clicked.connect(self.toggle)
            self.toggleButton = butt
            if not self.memo or self.memo=="":
                self.toggle(block=True)
    
    def toggle(self, block=False):
        self.on = not self.on
        if self.on:
            self.label.hide()
            for l in self.labels:
                l.show()
            self.toggleButton.setIcon(self.icoON)
            self.toggleButton.setToolTip(f"Compter seulement les entités")
        else:
            self.label.show()
            for l in self.labels:
                l.hide()
            self.toggleButton.setIcon(self.icoOFF)
            self.toggleButton.setToolTip(f"Faire une somme sur attribut")
        if not block:
            self.change()
    
    def setNumeric(self):
        self.numeric = True
        try:
            self.majCombo()
        except:
            pass
            
    def isPoint(self):
        geomtype = self.rdi.topAttr('type')
        if geomtype:
            isPoint = (geomtype=='point')
        else:
            isPoint = self.psql.isGeomPoint(self.rdi.tableName(), self.rdi.schemaName())
        return isPoint
    
    def majCombo(self):
        if self.isPoint():
            self.container.show()
        else:
            self.container.hide()
        while self.combo.count():
            self.combo.removeItem(0)
        self.combo.addItem("", "")
        fields = self.psql.getColumnsTableAll(self.rdi.tableName(), self.rdi.schemaName())
        for e in fields:
            if not self.numeric or e.get('numeric_precision')!=None:
                f = e.get('column_name')
                self.combo.addItem(f, f)
        if self.memo!="":
            self.combo.setCurrentText(self.memo)
        
    def get(self):
        try:
            if self.on:
                return self.combo.currentText()
            else:
                return ""
        except:
            return ""
            
    def change(self):
        self.memo = self.get()
        self.rdi.setLayer()
 
 
 
 
class rdiLayersCollection(QObject):
    
    changed = pyqtSignal()
    
    def __init__(self, dock, code=None):
        QObject.__init__(self)
        self.dock = dock
        self.code = code
        self.dict = {}
        self.list = []
        self.blocs = {}

    def title(self):
        try:
            return self.dock.param.get(self.code)
        except:
            return "Titre "+str(self.code)

    def setFilter(self, filter):
        self.filter = filter
    
    def setActu(self, actu):
        self.actu = actu

    def construct(self, layout):
        wg = QtWidgets.QWidget()
        layout.addWidget(wg)
        lg = QtWidgets.QVBoxLayout()
        lg.setSpacing(0)
        lg.setContentsMargins(2,0,2,0)
        wg.setLayout(lg)
        self.makeEntete(lg)
        self.makeList(lg)
    
    def makeEntete(self, lg):
        w = QtWidgets.QWidget()
        lg.addWidget(w)
        hbox = QtWidgets.QHBoxLayout()
        hbox.setContentsMargins(5,8,5,2)
        w.setLayout(hbox)
         
        l = QtWidgets.QLabel()
        img = os.path.join(self.dock.imageFolder,f"{self.code}.png")
        ico = QtGui.QIcon(img)
        l.setPixmap(ico.pixmap(15,15))
        l.setMaximumWidth(20)
        hbox.addWidget(l)
        
        l = QtWidgets.QLabel(self.title())
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        hbox.addWidget(l)
        
        filter = QtWidgets.QLineEdit()
        filter.setClearButtonEnabled(True)
        filter.setPlaceholderText("Mots-clefs du filtre ici")
        self.filter = filter
        filter.textChanged.connect(self.affFromFilter)
        hbox.addWidget(filter)
        l = QtWidgets.QLabel()
        hbox.addWidget(l)
        src = os.path.join(self.dock.imageFolder,'filterOn.png')
        pixmap = QtGui.QPixmap(src)
        pixmap = pixmap.scaledToHeight(20)
        l.setPixmap(pixmap)
        
        if self.actu:
            ico = QtGui.QIcon(os.path.join(self.dock.imageFolder,'refresh.png'))
            butt = QtWidgets.QPushButton(ico, '')
            butt.setToolTip("Actualiser")
            butt.setMaximumWidth(20)
            hbox.addWidget(butt)
            butt.clicked.connect(self.actu)
    
    def makeList(self, lg):
        sc = QtWidgets.QScrollArea()
        sc.setWidgetResizable(True)
        lg.addWidget(sc)
        w = QtWidgets.QWidget()
        gbox = QtWidgets.QVBoxLayout()
        gbox.setSpacing(0)
        gbox.setAlignment(Qt.AlignTop)
        w.setLayout(gbox)
        sc.setWidget(w)
        self.layerBloc = gbox
        if self.dock.param.get('blocDrop'):
            self.bloc = rdiLayersBloc(self)

    def add(self, rdi, force=False):
        self.dict[rdi.id()] = rdi
        self.list.append(rdi)
        rdi.coll = self
        rdi.clicked.connect(self.changed.emit)
        if rdi.tableName()!=None or force:
            if self.dock.param.get('blocDrop'):
                layout = self.bloc.layout
                bloc = rdi.getValueFromMeta('bloc')
                if bloc and bloc!="":
                    if bloc not in self.blocs:
                        self.blocs[bloc] = {'list':[], 'bloc':rdiLayersBloc(self, bloc)}
                    layout = self.blocs[bloc]['bloc'].layout
                    self.blocs[bloc]['list'].append(rdi)
            else:
                layout = self.layerBloc
            rdi.addCb(layout)
        
    def remove(self, rdi):
        try:
            rdi.cb.setChecked(False)
        except:
            pass
        rdi.bloc.removeWidget(rdi.widget)
        rdi.widget.setParent(None)
    
    def find(self, id):
        try:
            return self.dict[id]
        except:
            return None
    
    def affFromFilter(self):
        for rdi in self.list:
            if rdi.widget!=None:
                if rdi.keyMatch(self.filter.text()):
                    rdi.widget.show()
                    rdi.showed = True
                else:
                    rdi.widget.hide()
                    rdi.showed = False
        for c,b in self.blocs.items():
            has = False
            for rdi in b['list']:
                has = has or rdi.showed
            if has:
                b['bloc'].widgetTitle.show()
            else:
                b['bloc'].widgetTitle.hide()
            
    def matchCode(self, code):
        return (self.code==code)

    def getEltsSelected(self):
        list = []
        for rdi in self.list:
            if rdi.checked:
                list.append(rdi)
        return list
        
    def getElts(self):
        return self.list
        
    def addLayerField(self):
        for rdi in self.list:
            rdi.addField()
     
     
     
     
class rdiLayersBloc(QObject):
    changed = pyqtSignal()
    def __init__(self, coll, title=None):
        QObject.__init__(self)
        self.coll = coll
        self.makeTitle(title)
        self.makeBloc()
        
    def makeTitle(self, title):
        self.title = title
        self.code = f"{self.coll.code}_{title}"
        if title:
            w = rdiLayersBlocTitle(self.coll, title)
            if self.code in self.coll.dock.blocState:
                w.changeState()
            self.coll.layerBloc.addWidget(w)
            w.clicked.connect(self.clickAndDrop)
            self.widgetTitle = w
    
    def makeBloc(self):
        w = QtWidgets.QWidget()
        gbox = QtWidgets.QVBoxLayout(w)
        gbox.setSpacing(0)
        gbox.setContentsMargins(0,0,0,0)
        gbox.setAlignment(Qt.AlignTop)
        self.coll.layerBloc.addWidget(w)
        self.layout = gbox
        self.widget = w
        if self.title and self.code not in self.coll.dock.blocState:
            self.widget.hide()
            
    def clickAndDrop(self, state):
        if state=='plus':
            self.widget.hide()
            del self.coll.dock.blocState[self.code]
        if state=='minus':
            self.widget.show()
            self.coll.dock.blocState[self.code] = True
    
    

class rdiLayersBlocTitle(QtWidgets.QWidget):
    clicked = pyqtSignal(str)
    def __init__(self, coll, title):
        QtWidgets.QHBoxLayout.__init__(self)
        self.path = {'doss':coll.dock.imageFolder}
        self.layout = QtWidgets.QHBoxLayout(self)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0,0,0,0)
        self.initState()
        self.makeCross()
        self.makeLabel(title)
    
    def makeCross(self):
        l = QtWidgets.QLabel()
        l.setMaximumWidth(20)
        self.layout.addWidget(l)
        self.cross = l
        self.affectImg()
        
    def affectImg(self):
        ico = QtGui.QIcon(os.path.join(self.path['doss'], self.path[self.state]))
        self.cross.setPixmap(ico.pixmap(15,15))
    
    def makeLabel(self, txt):
        l = QtWidgets.QLabel(txt)
        self.layout.addWidget(l)
    
    def initState(self):
        self.state = 'plus'
        self.cd = {
            'plus': 'minus',
            'minus': 'plus',
        }
        self.path['plus'] = 'plus.png'
        self.path['minus'] = 'minus.png'
        
    def changeState(self):
        self.state = self.cd[self.state]
        self.affectImg()
    
    def mousePressEvent(self, event):
        self.changeState()
        self.clicked.emit(self.state)







        