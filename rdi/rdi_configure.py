# -*- coding: utf-8 -*-

import os, logging, json
from functools import partial

from qgis.gui import *
from qgis.core import *
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtCore import *

from .rdi_postgis import rdiPostgis
from .rdi_param import rdiParam
from .rdi_tools import quote, flashMsg, setTimeout, rdiTimer, clearLayout

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'base.ui'))


class rdiConfigure(QtWidgets.QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, parent=None):
        self.initdata()
        super(rdiConfigure, self).__init__()
        self.setupUi(self)
        layout = QtWidgets.QVBoxLayout()
        layout.setAlignment(Qt.AlignTop)
        self.dockWidgetContents.setLayout(layout)
        self.setWindowTitle("RDI - Configuration")
        self.layout = layout
        self.parent = parent
        self.connexions = "PostgreSQL/connections/"
        self.imageFolder = os.path.join(os.path.dirname(__file__),'images')
        self.param = rdiParam()
        self.makeConnParameter()
        self.timeout = False
        self.timer = rdiTimer(self, self.actuTimer)
        

    def getConnexionsList(self):
        s = QSettings() 
        s.beginGroup(self.connexions)
        list = s.childGroups()
        s.endGroup()
        return list    
        
    def setInfoFromCombo(self, combo):
        conn = combo.currentText()
        if conn and conn!="":
            s = QSettings()
            s.beginGroup(self.connexions+conn)
            currentKeys = s.childKeys()
            for item in self.translator['pg']:
                if 'qgis' in item and 'label' in item:
                    self.dform[item['label']].setText(s.value(item['qgis'],""))
                        
    def validAndConnect(self):
        password = None
        for item in self.translator['pg']:
            if 'label' in item:
                self.param.changeValue('CONN', item['ini'], self.dform[item['label']].text())
            if 'check' in item:
                self.param.changeValue('CONN', item['ini'], self.dform[item['check']].isChecked())
                if item['ini']=='savePassword' and not self.dform[item['check']].isChecked():
                    password = self.param.get('password')
                    self.parent.passwordTmp = password
                    self.param.changeValue('CONN', 'password', "")
        self.connInfos.setText("")
        self.connButton.setEnabled(False)
        self.psql = rdiPostgis(self.resultConnect, self.connInfos, password)
        dock = self.parent.dockwidget
        if dock!=None:
            dock.brokeWidget()
            
    def resultConnect(self):
        if self.psql.error:
            self.connButton.setEnabled(True)
            self.connInfos.setText(self.psql.error)
        else:
            self.makeTableAdmin()
            
    def toggleTest(self, test=True):
        self.clearWidget(self.response)
        
        self.launchButt.setEnabled(False)
        self.fixButt.setEnabled(False)
        
        self.lineTest.show()
        self.timer.stop()
        self.timer.setCountdown(3)
        self.timer.start(500)
        if self.timeout:
            self.timeout.clear()
            del self.timeout
        self.timeout = setTimeout(3000, self.testAdmin)
    
    def actuTimer(self):
        self.lineTest.label.setText(self.timer.remain())
    
    def tryAdmin(self):
        perfect = []
        almost = []
        for schemaC in self.psql.getSchemas():
            for schemaA in self.psql.getSchemas():
                if (len(perfect)>0):
                    return perfect
                absent, invalid, make = self.checkRdiTables(schemaC, schemaA)
                if len(absent)==0 and len(invalid)==0:
                    perfect.append((schemaC, schemaA))
                elif len(absent)==0:
                    almost.append((schemaC, schemaA))
        perfect.extend(almost)
        return perfect
    
    def testAdmin(self):
        self.timer.stop()
        if self.timeout:
            self.timeout.clear()
            self.timeout = None
        
        self.lineTest.hide()
        self.clearWidget(self.response)
        
        self.param.set('schema', self.schemaC.currentText(), 'CONN')
        self.param.set('schema', self.schemaA.currentText(), 'ADMIN')
        for item in self.translator['admin']:
            self.param.set(item['ini'], self.dform[item['label']].text())
            
        l = QtWidgets.QLabel("Recherche de l'existence des tables...")
        self.response.addWidget(l)
        QtWidgets.QApplication.processEvents()
        
        # self.param = rdiParam()
        # self.psql = rdiPostgis()
        self.psql.actuParams()
        isValid = False
        absent, invalid, make = self.checkRdiTables(self.param.get('schema', 'CONN'), self.param.get('schema', 'ADMIN'))
        self.makeList = make
        if len(absent)==0 and len(invalid)==0:
            string = "Les tables spécifiées existent et sont conformes"
            # ico = QtGui.QIcon(os.path.join(self.imageFolder, "analyze.png"))
            # butt = QtWidgets.QPushButton(ico, " Lancer l'application")
            # butt.setFixedSize(140, 30)
            # butt.clicked.connect(partial(self.parent.run, True))
            # butt.clicked.connect(self.close)
            # self.response.addWidget(butt)
            # self.response.setAlignment(butt, Qt.AlignCenter)
            isValid = True
            self.launchButt.setEnabled(True)
        else:
            string = "La configuration détectée ne correspond pas aux besoins du plugin:"
            if len(absent)>0:
                s = "s" if len(absent)>1 else ""
                string += f"\n → Table{s} à créer : " + ", ".join(absent)
            if len(invalid)>0:
                s = "s" if len(invalid)>1 else ""
                string += f"\n → Table{s} existante{s} à refaire : " + ", ".join(invalid)
                string += f"\n     (Attention, le changement de structure peut conduire à la perte de certaines données)"
            # ico = QtGui.QIcon(os.path.join(self.imageFolder, "execute.png"))
            # butt = QtWidgets.QPushButton(ico, '  Générer')
            # butt.clicked.connect(self.generateLack)
            # butt.setFixedSize(130, 25)
            # self.response.addWidget(butt)
            # self.response.setAlignment(butt, Qt.AlignCenter)
            self.fixButt.setEnabled(True)
        l.setText(string)
        return isValid
        
    def checkRdiTables(self, schemaC, schemaA):
        absent = []
        invalid = []
        make = []
        for item in self.translator['admin']:
            if 'default' in item:
                schema = schemaC
            else:
                schema = schemaA
            table = self.param.get(item['ini'])  
            if self.psql.isTable(table, schema):
                cols = self.psql.getColumnsTable(table, schema)
                bool = True
                for k,v in self.translator['relschem'][item['ini']].items():
                    b = False
                    for c in cols:
                        b = b or (c==k)
                    bool = bool and b
                if not bool:
                    invalid.append(table)
                    make.append(item)
            else:
                absent.append(table)
                make.append(item) 
        return absent, invalid, make
    
    def generateLack(self):
        self.fixButt.setEnabled(False)
        for item in self.makeList:
            if 'default' in item:
                schema = self.param.get('schema', 'CONN')
            else:
                schema = self.param.get('schema', 'ADMIN')
            table = self.param.get(item['ini'])
            full = self.psql.full(table, schema)
            flist = []
            pkey = None
            for k,v in self.translator['relschem'][item['ini']].items():
                flist.append(f"{quote(k)} {v}")
                if not pkey and v=='serial':
                    pkey = k
            if pkey:
                col = f"{table}_pkey"
                flist.append(f"constraint {quote(col)} primary key ({quote(pkey)})")
            fields = ", ".join(flist)
            memo = None
            if self.psql.isTable(table, schema):
                memo = self.psql.getTable(table, schema)
            sql = f"drop table if exists {full} cascade; create table {full} ({fields});"
            self.psql.sqlSend(sql, flash=True)
            if memo:
                self.restoreData(item, memo)
        self.testAdmin()
     
    def restoreData(self, item, memo):
        if 'default' in item:
            schema = None
        else:
            schema = self.param.get('schema', 'ADMIN')
        table = self.param.get(item['ini'])
        full = self.psql.full(table, schema)
        pkey = self.psql.getPrimaryKey(table, schema)
        cols = self.psql.getColumnsTable(table, schema)
        fields = ", ".join(map(quote, cols))    
        maxval = 0
        vslist = []
        for elt in memo:
            vlist = []
            for k in cols:
                val = elt.get(k, 'null')
                if k==pkey:
                    maxval = max(maxval, val)
                type = self.translator['relschem'][item['ini']][k]
                if type in ('character varying', 'timestamp without time zone') and str(val).lower()!='null':
                    val = str(val).replace("'", "''")
                    val = f"'{val}'"
                vlist.append(str(val))
            values = ", ".join(vlist)
            vslist.append(f"({values})")    
        if len(vslist)>0:
            sql = f"insert into {full} ({fields}) values " + ", ".join(vslist) + ";"
            self.psql.sqlSend(sql)
            seq = self.psql.getSequence(table, schema)
            if seq:
                sql = f"select setval('{self.psql.full(seq, schema)}',{maxval});"
                self.psql.sqlSend(sql)
            else:
                flashMsg(f"Erreur initialisation sequence pour {table}\n", category='error')
    
    
    
    def clearWidget(self, layout):
        for i in reversed(range(layout.count())): 
            w = layout.itemAt(i).widget()
            layout.removeWidget(w)
            w.setParent(None)
    
    def makeConnParameter(self):
        layout = self.layout
        clearLayout(layout)
        l = QtWidgets.QLabel("Paramètres de connexion à Postgis :" + \
                            "                                     ")
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        layout.addWidget(l)
        
        w = QtWidgets.QWidget()
        layout.addWidget(w)
        hbox = QtWidgets.QHBoxLayout(w)
        hbox.setSpacing(0)
        hbox.setContentsMargins(20,0,0,0)
        # w.setLayout(hbox)
        l = QtWidgets.QLabel("Charger la config depuis")
        hbox.addWidget(l)
        combo = QtWidgets.QComboBox()
        hbox.addWidget(combo)
        combo.currentIndexChanged.connect(partial(self.setInfoFromCombo, combo))
        combo.addItem("", {})
        for conn in self.getConnexionsList():
            combo.addItem(conn, {})
            
        w = QtWidgets.QWidget()
        layout.addWidget(w)
        form = QtWidgets.QFormLayout()
        w.setLayout(form)
        self.dform = {}
        for item in self.translator['pg']:
            if 'label' in item:
                wl = QtWidgets.QLabel(item['label'])
                wf = QtWidgets.QLineEdit()
                if ('ini' in item) and (item['ini'] in self.param.getSection('CONN')):
                    if item['ini']=='password':
                        wf = QgsPasswordLineEdit()
                    val = str(self.param.get(item['ini']))
                    wf.setText(val)        
                form.addRow(wl, wf)
                self.dform[item['label']] = wf
            if 'check' in item:
                wf = QtWidgets.QCheckBox(item['check'])
                if ('ini' in item):
                    wf.setChecked(self.param.get(item['ini']))      
                form.addRow(QtWidgets.QLabel(), wf)
                self.dform[item['check']] = wf
        
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "plug.png"))
        butt = QtWidgets.QPushButton(ico, " Se connecter")
        butt.setFixedSize(130, 30)
        layout.addWidget(butt)
        layout.setAlignment(butt, Qt.AlignCenter)
        butt.clicked.connect(self.validAndConnect)
        self.connButton = butt
        self.connInfos = QtWidgets.QLabel()
        layout.addWidget(self.connInfos)
    
    
    def makeCombo(self, list, val):
        combo = QtWidgets.QComboBox()
        combo.addItem("","")
        for s in list:
            combo.addItem(s, s)
        try:
            combo.setCurrentText(val)
        except:
            pass
        return combo
        
    def makeIndent(self, text, indent):
        crlf = "\n"
        deb = ""
        for i in range(indent):
            deb += " "
        return crlf.join(map(lambda x: f"{deb}{x}",text.split(crlf)))
    
    def makeComment(self, text, layout, indent=0):
        l = QtWidgets.QLabel(self.makeIndent(text, indent))
        font = QtGui.QFont()
        font.setItalic(True)
        l.setFont(font)
        layout.addWidget(l)
        return l
        
    def makeTitle(self, text, layout, indent=0):
        l = QtWidgets.QLabel()
        layout.addWidget(l)
        l = QtWidgets.QLabel(self.makeIndent(text, indent))
        font = QtGui.QFont()
        font.setBold(True)
        l.setFont(font)
        layout.addWidget(l)
        return l
        
    def makeForm(self, layout):
        w = QtWidgets.QWidget()
        layout.addWidget(w)
        form = QtWidgets.QFormLayout(w)
        form.setContentsMargins(0,0,0,0)
        return form
    
    def makeTableAdmin(self):
        layout = self.layout
        self.clearWidget(layout)
        hbox = QtWidgets.QHBoxLayout()
        layout.addLayout(hbox)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "back.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setMaximumWidth(30)
        hbox.addWidget(butt)
        butt.clicked.connect(self.makeConnParameter)
        l = self.makeComment("Connexion valide", hbox)
        hbox.setAlignment(l, Qt.AlignCenter)
         
        form = self.makeForm(layout)
        wl = QtWidgets.QLabel("Schéma éxecution")
        wf = self.makeCombo(self.psql.getSchemas(), self.param.get('schema', 'CONN'))
        wf.currentIndexChanged.connect(self.toggleTest)
        form.addRow(wl, wf)
        self.schemaC = wf
        self.makeComment("doit être accessible en écriture à tous les utilisateurs", layout, indent=7)
        
        form = self.makeForm(layout)
        self.dform = {}
        for item in self.translator['admin']:
            label = item['label']
            wl = QtWidgets.QLabel(label)
            wf = QtWidgets.QLineEdit(self.param.get(item['ini']))
            wf.textChanged.connect(self.toggleTest)
            form.addRow(wl, wf)
            self.dform[label] = wf
        
        l = QtWidgets.QLabel()
        layout.addWidget(l)
        txt = "La table d'aministration peut être séparée dans un schéma avec accès restreint" + \
            "\n   → dans ce cas, seuls les utilisateurs ayant un droit d'écriture" + \
            "\n      pourront modifier la configuration des couches du plugin"
        self.makeComment(txt, layout, indent=7)
        form = self.makeForm(layout)
        wl = QtWidgets.QLabel("Schéma administration")
        wf = self.makeCombo(self.psql.getSchemas(), self.param.get('schema', 'ADMIN'))
        wf.currentIndexChanged.connect(self.toggleTest)
        form.addRow(wl, wf)
        self.schemaA = wf
        l = QtWidgets.QLabel()
        layout.addWidget(l)

        
        self.lineTest = QtWidgets.QWidget()
        layout.addWidget(self.lineTest)
        hbox = QtWidgets.QHBoxLayout(self.lineTest)
        l = QtWidgets.QLabel("Lancer à nouveau le test de compatibilité")
        hbox.addWidget(l)
        hbox.setAlignment(l, Qt.AlignRight)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "database-look.png"))
        butt = QtWidgets.QPushButton(ico, "")
        butt.setMaximumWidth(30)
        hbox.addWidget(butt)
        butt.clicked.connect(self.testAdmin)
        l = QtWidgets.QLabel("")
        hbox.addWidget(l)
        l.setMaximumWidth(30)
        self.lineTest.label = l

        w = QtWidgets.QWidget()
        layout.addWidget(w)
        self.response = QtWidgets.QVBoxLayout(w)
        
        hbox = QtWidgets.QHBoxLayout()
        layout.addLayout(hbox)
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "analyze.png"))
        butt = QtWidgets.QPushButton(ico, " Lancer l'application")
        butt.setFixedSize(140, 30)
        butt.clicked.connect(partial(self.parent.run, True))
        butt.clicked.connect(self.close)
        butt.setEnabled(False)
        hbox.addWidget(butt)
        hbox.setAlignment(butt, Qt.AlignCenter)
        self.launchButt = butt
        ico = QtGui.QIcon(os.path.join(self.imageFolder, "execute.png"))
        butt = QtWidgets.QPushButton(ico, '  Générer')
        butt.clicked.connect(self.generateLack)
        butt.setFixedSize(120, 30)
        butt.setEnabled(False)
        hbox.addWidget(butt)
        hbox.setAlignment(butt, Qt.AlignCenter)
        self.fixButt = butt
        
        if not self.testAdmin():
            perfect = self.tryAdmin()
            if len(perfect)>0:
                try:
                    self.schemaC.setCurrentText(perfect[0][0])
                    self.schemaA.setCurrentText(perfect[0][1])
                except:
                    pass
            self.testAdmin()
    
    
    
    
    def initdata(self):
        self.translator = {}
        
        self.translator['pg'] = [
                {
                    'qgis':'host',
                    'label':'Hôte',
                    'ini':'host',
                },
                {
                    'qgis':'port',
                    'label':'Port',
                    'ini':'port',
                },
                {
                    'qgis':'username',
                    'label':'Utilisateur',
                    'ini':'user'},
                {
                    'qgis':'password',
                    'label':'Mot de passe',
                    'ini':'password',
                },
                {
                    'qgis':None,
                    'check': 'Sauvegarder',
                    'ini':'savePassword',
                },
                {
                    'qgis':'database',
                    'label':'Base',
                    'ini':'db',
                },
            ]
        
        self.translator['admin'] = [
                {
                    'label':'Exécution',
                    'ini':'tableTmp',
                    'default':1,
                },
                {
                    'label':'Administration',
                    'ini':'tableAdmin',
                },
            ]
        
        self.translator['relschem'] = {
            'tableAdmin': {
                    'id': 'serial',
                    'code': 'character varying', 
                    'tablename': 'character varying', 
                    'schemaname': 'character varying',
                    'geom': 'character varying',
                    'type': 'character varying',
                    'index': 'boolean',
                    'valid': 'boolean',
                    'name': 'character varying',
                    'parent': 'integer',
                    'bloc' : 'character varying',
                    'style': 'character varying',
                    'order': 'integer',
                    'tags': 'character varying',
                    'stats': 'character varying',
                    'cluster': 'character varying',
                },
            'tableTmp': {
                    'id':'serial', 
                    'tablename':'character varying', 
                    'alea':'character varying', 
                    'enjeu':'character varying',
                    'requete':'character varying',
                    'cree':'timestamp without time zone',
                    'dispo':'timestamp without time zone',
                    'sql':'character varying',
                },
            }
        

        
    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()
